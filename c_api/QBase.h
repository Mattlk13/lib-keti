/** @file c_api/QBase.h

@brief C API quantum base classes declaration

@copyright This file is part of the LibKet library (C API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller

@ingroup c_api
*/

#pragma once
#ifndef C_API_QBASE_H
#define C_API_QBASE_H

#ifdef __cplusplus
extern "C" {
#endif
  
enum qError
  {
   // clang-format off
   qSuccess                =   0,
   qErrorInvalidValue      =   1,
   qErrorNotYetImplemented =  31,
   qErrorNotReady          = 300
   // clang-format on
  };
typedef enum qError qError_t;

#ifdef __cplusplus
}
#endif

#endif // C_API_QBASE_H
