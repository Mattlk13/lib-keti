/** @file c_api/QStream.cpp

@brief C API quantum stream class implementation

@copyright This file is part of the LibKet library (C API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller

@ingroup c_api
*/

#include <stdlib.h>

#include <QStream.h>

#include <QBase.hpp>

/**
   @brief Quantum stream class specialization for the Python
   execution unit
   
   @ingroup c_api
*/
struct qPythonStream {
  void *obj;
};

/**
   @brief Quantum stream class specialization for the C++
   execution unit
   
   @ingroup c_api
*/
struct qCppStream {
  void *obj;
};

