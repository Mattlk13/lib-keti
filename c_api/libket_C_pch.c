/** @file c_api/libket_C_pch.c

    @brief C API main pre-compiled header file

    @copyright This file is part of the LibKet library (C API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */
#include <LibKet.h>
int main()
{
  printf("LibKet precompiled C headers\n");
  return 0;
}
