.. _LibKet Index:

.. image:: _static/LibKet_logo_color.png
    :width: 350
    :alt: LibKet

Welcome to LibKet
=================

LibKet (pronounced lib-ket) is a lightweight `expression
template <https://en.wikipedia.org/wiki/Expression_templates>`_ library
that allows you to develop quantum algorithms as backend-agnostic
generic expressions and execute them on different quantum simulator
and hardware backends without changing your code.

Supported quantum hardware backends

- `IBM Quantum Experience <https://quantum-computing.ibm.com>`_
- `QuTech Quantum Inspire <https://quantuminspire.com>`_
- `Rigetti Aspen <https://qcs.rigetti.com>`_

Supported quantum simulator backends

- `Atos Quantum Learning Machine <https://atos.net/en/solutions/quantum-learning-machine>`_
- `Google Cirq <https://quantumai.google/cirq>`_
- `IBM Qiskit <https://qiskit.org>`_
- `QE-Lab OpenQL <https://openql.readthedocs.io>`_
- `QuEST - Quantum Exact Simulation Toolkit <https://quest.qtechtheory.org>`_
- `QuTech QX simulator <https://qutech.nl/qx-quantum-computer-simulator/>`_
- `Rigetti PyQuil <http://docs.rigetti.com/>`_ 

All you need to get started is a C++14 (or better) compiler and,
optionally, `Python 3.x <https://www.python.org>`_ to execute quantum
algorithms directly from within LibKet.

.. _LibKet User Documentation:

User Documentation
------------------

.. toctree::
   :maxdepth: 1

   sections/installation_guide
   sections/basics
   sections/advanced
   sections/library
   sections/tutorials
   sections/api_documentation
   sections/release_notes
   sections/faq

.. Hiding - Indices and tables
   :ref:`genindex`
   :ref:`modindex`
   :ref:`search`
