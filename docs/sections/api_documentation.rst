.. _LibKet API Documentation:
   
API documentation
=================

C++ API:
--------
For the full API documentation, visit: `C++ API <https://libket.gitlab.io/LibKet/>`_

C API:
------

Python API:
-----------

.. .. _LibKet API Filters:

.. Filters
.. -------
..
   .. doxygengroup:: filters
      :members:

.. .. _LibKet API Gates:

.. Gates
.. -----
..
   .. doxygengroup:: gates
      :outline:

.. .. _LibKet API Circuits:

.. Circuits
.. --------
..
   .. doxygengroup:: circuits
      :content-only:

.. .. _LibKet API Devices:

.. Devices
.. -------
..
   .. doxygengroup:: devices
   
