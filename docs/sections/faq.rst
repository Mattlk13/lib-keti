.. _LibKet FAQ:
   
Frequently Asked Questions
==========================

#. **Why the name LibKet?**

   The acronym **LibKet** stands for Quantum Expression Template
   Library and is an allusion to the famous `bra-ket notation
   <https://en.wikipedia.org/wiki/Bra–ket_notation>`_ since the Dutch
   spelling of the word `Quantum` is `Kwantum`.
      
#. **Can you add support for the quantum device X?**

   Sure, as long as there exists either a Python package or a C/C++
   API that allows to communicate with the quantum device, we can add
   support for it. Just let us know which quantum device you would
   like to see supported in **LibKet** by creating a `feature request
   <https://gitlab.com/libket/LibKet/-/issues>`_.

#. **I think I found a bug in LibKet. How can I report it?**

   Please create an `bug report
   <https://gitlab.com/libket/LibKet/-/issues>`_ describing the bug,
   your specific setup (operating system and compiler versions) and
   the steps to reproduce the bug. We will take care it is fixed.

#. **I developed a bugfix/new feature. How can I contribute it?**

   Please `fork <https://gitlab.com/libket/LibKet/-/forks/new>`_ the
   **LibKet** repository, commit your changes there, and create a
   `merge request <https://gitlab.com/libket/LibKet/-/merge_requests/new>`_.

#. **I used LibKet for my research. How can I cite it?**

   Please use the folloginw BibTeX entry to cite **LibKet**

   .. code-block:: rst

      @InProceedings{10.1007/978-3-030-50433-5_35,
      author    = {M{\"o}ller, Matthias and Schalkers, Merel},
      editor    = {Krzhizhanovskaya, Valeria V. and Z{\'a}vodszky, G{\'a}bor and Lees, Michael H. and Dongarra, Jack J. and Sloot, Peter M. A. and Brissos, S{\'e}rgio and Teixeira, Jo{\~a}o},
      title     = {LibKet: A Cross-Platform Programming Framework for Quantum-Accelerated Scientific Computing},
      booktitle = {Computational Science -- ICCS 2020},
      year      = {2020},
      publisher = {Springer International Publishing},
      address   = {Cham},
      pages     = {451--464},
      isbn      = {978-3-030-50433-5},
      doi       = {10.1007/978-3-030-50433-5_35},
      url       = {https://doi.org/10.1007/978-3-030-50433-5_35}
      }

#. **I like LibKet. Can I contribute to its further development?**

   That's nice to hear. Sure, you are more than welcome to contribute
   to the developmet of **LibKet**. Please contact us by email
   (m.moller@tudelft.nl) or
   `slack <https://join.slack.com/t/libket/shared_invite/zt-mrlp17rr-vFyaF~pDgzBb3iRB_DO7jA>`_
   to discuss possible collaborations.
