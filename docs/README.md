# LibKet - The Quantum Expression Template Library

**LibKet** (pronounced lib-ket) is a lightweight [expression
template](https://en.wikipedia.org/wiki/Expression_templates) library
that allows you to develop quantum algorithms as backend-agnostic
generic expressions and execute them on different quantum simulator
and hardware backends without changing your code.

## Getting Started

**LibKet** is designed as header-only C++ library with minimal
external dependencies. All you need to get started is a C++14 compiler
and, optionally, Python 3.x to execute quantum algorithms directly
from within **LibKet**.

Have a look at the following code snipped to see how easy it is to
generate the quantum algorithm for an `n`-qubit [Quantum Fourier
Transform](https://en.wikipedia.org/wiki/Quantum_Fourier_transform)
(QFT) circuit and execute it on the [Quantum
Inspire](https://www.quantum-inspire.com/) simulator platform using
6-qubits:

```cpp
// Include main header file
#include <LibKet.hpp>

// Import namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

// Create generic quantum expression
auto expr = qft(init());

// Execute QFT<6> on the Quantum-Inspire simulator platform
try {
  QDevice<QDeviceType::qi_26_simulator, 6> qi; qi(expr);
  utils::json result = qi.eval();
                      
  QInfo << result << std::endl;

  QInfo << "job ID     : " << qi.get<QResultType::id>(result)               << std::endl;
  QInfo << "time stamp : " << qi.get<QResultType::timestamp>(result)        << std::endl;
  QInfo << "duration   : " << qi.get<QResultType::duration>(result).count() << std::endl;
  QInfo << "best       : " << qi.get<QResultType::best>(result)             << std::endl;
  QInfo << "histogram  : " << qi.get<QResultType::histogram>(result)        << std::endl;
                                         
} catch(const std::exception &e) {
  QWarn << e.what() << std::endl;
}
```

## Copyright

Copyright (c) 2018-2021 Matthias Möller (m.moller@tudelft.nl).

In Dutch, 'Quantum' is spelled 'Kwantum', which explains the spelling
**LibKet**. The name is an allusion to the famous [bra-ket
notation](https://en.wikipedia.org/wiki/Bra%E2%80%93ket_notation) that
is widely used for expressing quantum algorithms.

