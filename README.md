<img src="docs/_static/LibKet_logo_color.png"  width="350">

# LibKet - The Quantum Expression Template Library

[![License: MPL 2.0][license-badge]][license-link]
[![Documentation][rtd-badge]][rtd-link]
[![Documentation][dox-badge]][dox-link]
[![Pipeline][pipeline-badge]][pipeline-link]
[![Appveyor][appveyor-badge]][appveyor-link]
[![Coveralls][coveralls-badge]][coveralls-link]
[![Coverity][coverity-badge]][coverity-link]
[![Codacy][codacy-badge]][codacy-link]

**LibKet** (pronounced lib-ket) is a lightweight [expression
template](https://en.wikipedia.org/wiki/Expression_templates) library
that allows you to develop quantum algorithms as backend-agnostic
generic expressions and execute them on different quantum simulator
and hardware backends without changing your code.

**LibKet** is designed as header-only C++ library with minimal
external dependencies. All you need to get started is a C++14 compiler
and, optionally, Python 3.x to execute quantum algorithms directly
from within **LibKet**.

Have a look at the following code snipped to see how easy it is to
generate the quantum algorithm for an `n`-qubit [Quantum Fourier
Transform](https://en.wikipedia.org/wiki/Quantum_Fourier_transform)
(QFT) circuit and execute it on the [Quantum
Inspire](https://www.quantum-inspire.com/) simulator platform using
6-qubits:

```cpp
// Include main header file
#include <LibKet.hpp>

// Import namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

// Create generic quantum expression
auto expr = qft(init());

// Execute QFT<6> on the Quantum-Inspire simulator platform
try {
  QDevice<QDeviceType::qi_26_simulator, 6> qi; qi(expr);
  utils::json result = qi.eval();
                      
  QInfo << result << std::endl;

  QInfo << "job ID     : " << qi.get<QResultType::id>(result)               << std::endl;
  QInfo << "time stamp : " << qi.get<QResultType::timestamp>(result)        << std::endl;
  QInfo << "duration   : " << qi.get<QResultType::duration>(result).count() << std::endl;
  QInfo << "best       : " << qi.get<QResultType::best>(result)             << std::endl;
  QInfo << "histogram  : " << qi.get<QResultType::histogram>(result)        << std::endl;
                                         
} catch(const std::exception &e) {
  QWarn << e.what() << std::endl;
}
```

## Quickstart with interactive tutorial notebook

[![docker][docker-badge]][docker-link]
[![binder][binder-badge]][binder-link]

Without installing anything, you can quickly get started with quantum
programming in **LibKet** by exploring our interactive
[Jupyter](jupter) notebook tutorials and examples. To run them in a
preconfigured execution environment on [Binder](binder), click the
"launch binder" badge above or the link [here](binder-link)! To learn
more about the tutorials and how you can add your own, visit the
[mmoelle1/LibKet](libket-github) repository at GitHub. If you'd rather
set everything up locally, or are interested in contributing to
**LibKetpy, continue reading the
[documentation](https://libket.readthedocs.org/) and the [API
reference](https://libket.gitlab.io/LibKet/).

## Publications and talks

* [Mini-tutorial](https://github.com/mmoelle1/LibKet) at SIAM CSE21, March 1-5, 2021.
* [Paper][iccs2020-paper] and [presentation][iccs2020-slides] at ICCS 2020

## Citing LibKet

If you use **LibKet** in your research please cite it as follows

```bibtex
@InProceedings{10.1007/978-3-030-50433-5_35,
  author    = {M{\"o}ller, Matthias and Schalkers, Merel},
  editor    = {Krzhizhanovskaya, Valeria V. and Z{\'a}vodszky, G{\'a}bor and Lees, Michael H. and Dongarra, Jack J. and Sloot, Peter M. A. and Brissos, S{\'e}rgio and Teixeira, Jo{\~a}o},
  title     = {LibKet: A Cross-Platform Programming Framework for Quantum-Accelerated Scientific Computing},
  booktitle = {Computational Science -- ICCS 2020},
  year      = {2020},
  publisher = {Springer International Publishing},
  address   = {Cham},
  pages     = {451--464},
  isbn      = {978-3-030-50433-5},
  doi       = {10.1007/978-3-030-50433-5_35},
  url       = {https://doi.org/10.1007/978-3-030-50433-5_35}
}
```

## Joining the LibKet community

[![Slack][slack-badge]][slack-invite]

If you'd like to get involved with **LibKet** join the [LibKet Slack
Workpace](slack-invite) by clicking this [invite
link](slack-invite). This is the right place to ask general questions,
start discussions on **LibKet**, and hear about updates.

If you'd like to help improve **LibKet** please [report
bugs](https://gitlab.com/libket/LibKet/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
and [request
feature](https://gitlab.com/libket/LibKet/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
through the issue tracker system. Please select `Labels -> Feature
request` for the latter.

If you'd like to work on a bugfix or a new feature, please [fork this
repository](https://gitlab.com/libket/LibKet/-/forks/new) and create
a [merge
request](https://gitlab.com/libket/LibKet/-/merge_requests/new) once
your changes are ready. Thanks for contributing to **LibKet**.

## Copyright

Copyright (c) 2018-2021 Matthias Möller (m.moller@tudelft.nl).

In Dutch, 'Quantum' is spelled 'Kwantum', which explains the spelling
**LibKet**. The name is an allusion to the famous [bra-ket
notation](https://en.wikipedia.org/wiki/Bra%E2%80%93ket_notation) that
is widely used for expressing quantum algorithms.

[appveyor-badge]:  https://ci.appveyor.com/api/projects/status/ejax6hyqfnpd51j5?svg=true
[appveyor-link]:   https://ci.appveyor.com/project/mmoelle168354/libket
[binder-badge]:    https://mybinder.org/badge_logo.svg
[binder-link]:     https://mybinder.org/v2/gh/libket/LibKet/master?filepath=notebooks
[codacy-badge]:    https://api.codacy.com/project/badge/Grade/e8679cc335b34b1babc4bdb34a60c100
[codacy-link]:     https://www.codacy.com/manual/mmoelle1/LibKet?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=mmoelle1/LibKet&amp;utm_campaign=Badge_Grade
[coveralls-badge]: https://coveralls.io/repos/gitlab/mmoelle1/LibKet/badge.svg?branch=matthias_branch
[coveralls-link]:  https://coveralls.io/gitlab/libket/LibKet?branch=master
[coverity-badge]:  https://scan.coverity.com/projects/libket/badge.svg
[coverity-link]:   https://scan.coverity.com/projects/libket
[docker-badge]:    https://img.shields.io/docker/pulls/libket/libket
[docker-link]:     https://hub.docker.com/r/mmoelle1/libket
[dox-badge]:       https://img.shields.io/badge/docs-doxygen-blue.svg
[dox-link]:        https://libket.gitlab.io/LibKet/
[license-badge]:   https://img.shields.io/badge/License-MPL%202.0-brightgreen.svg
[license-link]:    https://opensource.org/licenses/MPL-2.0
[pipeline-badge]:  https://gitlab.com/libket/LibKet/badges/matthias_branch/pipeline.svg
[pipeline-link]:   https://gitlab.com/libket/LibKet/-/commits/matthias_branch
[rtd-badge]:       https://readthedocs.org/projects/libket/badge/?version=latest
[rtd-link]:        http://libket.readthedocs.io/en/latest/?badge=latest
[slack-badge]:     https://img.shields.io/badge/slack-LibKet-00a6d6
[slack-invite]:    https://join.slack.com/t/libket/shared_invite/zt-mrlp17rr-vFyaF~pDgzBb3iRB_DO7jA

[libket-github]:   https://github.com/mmoelle1/LibKet
[libket-gitlab]:   https://gitlab.com/libket/LibKet

[iccs2020-paper]:  https://link.springer.com/chapter/10.1007/978-3-030-50433-5_35
[iccs2020-slides]: http://ta.twi.tudelft.nl/nw/users/matthias/files/presentations/iccs2020.pdf

[binder]:          https://mybinder.org
[jupyter]:         https://jupyter.org
