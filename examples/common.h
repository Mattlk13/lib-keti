/** @file examples/common.h

    @brief Common macros for C++ tutorials
    
    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>

/**
   @brief Macro evaluates the given expression

   @param[in]    nqubits number of qubits
   @param[in]    expr    quantum expression
   @param[in]    backend quantum backend
   @param[in]    shots   number of shots
   @param[inout] result  result stored as JSON object
*/
#define EVAL_EXPR_PYTHON(nqubits, expr, backend, shots, result)         \
  {                                                                     \
    QInfo << "\n\n\n===== " #backend " =====\n\n\n";                    \
    QDevice<backend , nqubits> device; device(expr);                    \
    QDebug << device << std::endl;                                      \
    try {                                                               \
      utils::json res = device.eval(shots);                             \
      result[#backend] = res;                                           \
      QInfo << "job ID     : " << device.get<QResultType::id>(res) << std::endl; \
      QInfo << "time stamp : " << device.get<QResultType::timestamp>(res) << std::endl; \
      QInfo << "duration   : " << device.get<QResultType::duration>(res).count() << std::endl; \
      QInfo << "best       : " << device.get<QResultType::best>(res) << std::endl; \
    } catch(const std::exception &e) { QWarn << e.what() << std::endl; } \
  }

#ifdef LIBKET_WITH_AQASM
#define EVAL_EXPR_AQASM(nqubits, expr, backend, shots, result) EVAL_EXPR_PYTHON(nqubits, expr, backend, shots, result)
#else
#define EVAL_EXPR_AQASM(nqubits, expr, backend, shots, result) result[#backend] = "AQASM disabled"
#endif

#ifdef LIBKET_WITH_CIRQ
#define EVAL_EXPR_CIRQ(nqubits, expr, backend, shots, result) EVAL_EXPR_PYTHON(nqubits, expr, backend, shots, result)
#else
#define EVAL_EXPR_CIRQ(nqubits, expr, backend, shots, result) result[#backend] = "CIRQ disabled"
#endif

#ifdef LIBKET_WITH_CQASM
#define EVAL_EXPR_CQASM(nqubits, expr, backend, shots, result) EVAL_EXPR_PYTHON(nqubits, expr, backend, shots, result)
#else
#define EVAL_EXPR_CQASM(nqubits, expr, backend, shots, result) result[#backend] = "CQASM disabled"
#endif

#ifdef LIBKET_WITH_OPENQASM
#define EVAL_EXPR_OPENQASM(nqubits, expr, backend, shots, result) EVAL_EXPR_PYTHON(nqubits, expr, backend, shots, result)
#else
#define EVAL_EXPR_OPENQASM(nqubits, expr, backend, shots, result) result[#backend] = "OPENQASM disabled"
#endif

#ifdef LIBKET_WITH_QUIL
#define EVAL_EXPR_QUIL(nqubits, expr, backend, shots, result) EVAL_EXPR_PYTHON(nqubits, expr, backend, shots, result)
#else
#define EVAL_EXPR_QUIL(nqubits, expr, backend, shots, result) result[#backend] = "QUIL disabled"
#endif

/**
   @brief Macro evaluates the given expression on OpenQL
   
   @param[in]    nqubits number of qubits
   @param[in]    expr    quantum expression
   @param[in]    backend quantum backend
   @param[in]    shots   number of shots
   @param[inout] result  result stored as JSON object
*/
#ifdef LIBKET_WITH_OPENQL
#define EVAL_EXPR_OPENQL(nqubits, expr, backend, shots, result)         \
  {                                                                     \
    QInfo << "\n\n\n===== " #backend " =====\n\n\n";                    \
    QDevice<backend, nqubits> device; device(expr);                     \
    QDebug << device << std::endl;                                      \
    try {                                                               \
      device.compile("libket");                                         \
    } catch(const std::exception &e) { QWarn << e.what() << std::endl; } \
  }
#else
#define EVAL_EXPR_OPENQL(nqubits, expr, backend, shots, result)
#endif

/**
   @brief Macro evaluates the given expression on Qasm2Tex
   
   @param[in]    nqubits number of qubits
   @param[in]    expr    quantum expression
   @param[in]    backend quantum backend
   @param[in]    shots   number of shots
   @param[inout] result  result stored as JSON object
*/
#ifdef LIBKET_WITH_QASM
#define EVAL_EXPR_QASM(nqubits, expr, backend, shots, result)           \
  {                                                                     \
    QInfo << "\n\n\n===== " #backend " =====\n\n\n";                    \
    QDevice<backend, nqubits> device; device(expr);                     \
    QDebug << device << std::endl;                                      \
    try {                                                               \
      device.to_file("libket");                                         \
    } catch(const std::exception &e) { QWarn << e.what() << std::endl; } \
  }
#else
#define EVAL_EXPR_QASM(nqubits, expr, backend, shots, result)
#endif

/**
   @brief Macro evaluates the given expression on QuEST
   
   @param[in]    nqubits number of qubits
   @param[in]    expr    quantum expression
   @param[in]    backend quantum backend
   @param[in]    shots   number of shots
   @param[inout] result  result stored as JSON object
*/
#ifdef LIBKET_WITH_QUEST
#define EVAL_EXPR_QUEST(nqubits, expr, backend, shots, result)          \
  {                                                                     \
    QInfo << "\n\n\n===== " #backend " =====\n\n\n";                    \
    QDevice<backend, nqubits> device; device(expr);                     \
    QDebug << device << std::endl;                                      \
    try {                                                               \
      auto job = device.execute(1);                                     \
      utils::json res;                                                  \
      res["duration"] = job->duration().count();                        \
      result[#backend] = res;                                           \
      QInfo << "duration      : " << job->duration().count() << std::endl; \
      QInfo << "best          : " << device.best() << std::endl;        \
    } catch(const std::exception &e) { QWarn << e.what() << std::endl; } \
  }
#else
#define EVAL_EXPR_QUEST(nqubits, expr, backend, shots, result)
#endif

/**
   @brief Macro evaluates the given expression on QX
   
   @param[in]    nqubits number of qubits
   @param[in]    expr    quantum expression
   @param[in]    backend quantum backend
   @param[in]    shots   number of shots
   @param[inout] result  result stored as JSON object
*/
#ifdef LIBKET_WITH_QX
#define EVAL_EXPR_QX(nqubits, expr, backend, shots, result)             \
  {                                                                     \
    QInfo << "\n\n\n===== " #backend " =====\n\n\n";                    \
    QDevice<backend, nqubits> device; device(expr);                     \
    QDebug << device << std::endl;                                      \
    try {                                                               \
      auto job = device.execute(1);                                     \
      utils::json res;                                                  \
      res["duration"] = job->duration().count();                        \
      result[#backend] = res;                                           \
      QInfo << "duration      : " << job->duration().count() << std::endl; \
      device.reg().dump();                                              \
      device.reset();                                                   \
    } catch(const std::exception &e) { QWarn << e.what() << std::endl; } \
  }
#else
#define EVAL_EXPR_QX(nqubits, expr, backend, shots, result)
#endif

#define EVAL_EXPR_SIMPLE(nqubits, expr, shots, result)                  \
  EVAL_EXPR_AQASM(nqubits, expr, QDeviceType::atos_qlm_feynman_simulator, shots, result); \
  EVAL_EXPR_CIRQ(nqubits, expr, QDeviceType::cirq_simulator, shots, result); \
  EVAL_EXPR_CQASM(nqubits, expr, QDeviceType::qi_26_simulator, shots, result); \
  EVAL_EXPR_OPENQASM(nqubits, expr, QDeviceType::qiskit_qasm_simulator, shots, result); \
  EVAL_EXPR_OPENQASM(nqubits, expr, QDeviceType::ibmq_qasm_simulator, shots, result); \
  EVAL_EXPR_QUIL(nqubits, expr, QDeviceType::rigetti_9q_square_simulator, shots, result); \
  EVAL_EXPR_OPENQL(nqubits, expr, QDeviceType::openql_qx_compiler, shots, result); \
  EVAL_EXPR_QASM(nqubits, expr, QDeviceType::qasm2tex_visualizer, shots, result); \
  EVAL_EXPR_QUEST(nqubits, expr, QDeviceType::quest, shots, result);    \
  EVAL_EXPR_QX(nqubits, expr, QDeviceType::qx, shots, result);
