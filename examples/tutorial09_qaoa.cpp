/** @file examples/tutorial09_qaoa.cpp

    @brief C++ tutorial-09: Quantum Approximate Optimization Algorithm.
           This tutorial shows how to create a QAOA circuit for the
           maxcut problem on an arbitrary graph.

    @copyright This file is part of the LibKet library.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller, Huub Donkers
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

template<index_t start, index_t end, index_t step, index_t index>
struct maxcut_cost_function
{  
  template<typename  Expr, typename Graph, typename Gamma>
  inline constexpr auto operator()(Expr&& expr, Graph, Gamma) noexcept
  {
    auto cnot1 = cnot(sel<Graph::template from<index>()>(gototag<0>()),
                      sel<Graph::template to<index>()>(gototag<0>(expr))
                      );

    auto rot_z_gamma = rz(Gamma{}, sel<1>(cnot1));

    auto cnot2 = cnot(sel<Graph::template from<index>()>(gototag<0>()),
                      sel<Graph::template to<index>()>(gototag<0>(rot_z_gamma))
                      );

    return gototag<0>(cnot2);         
  }
};

int main(int argc, char *argv[])
{
  // Set number of qubits
  const size_t nqubits = 5;

  // Create compile-time graph
  constexpr const utils::graph<>
    ::edge<0,1>
    ::edge<0,4>
    ::edge<1,2>
    ::edge<1,4>
    ::edge<2,3> g;
  
  QInfo << g << std::endl;

  //Set circuits parameters
  auto beta = QConst(0.5);
  auto gamma = QConst(0.8);
  
  QInfo << "QAOA parameters: " << beta << ", " << gamma << std::endl;
  
  //Set initial state to superposition
  auto init_state = all(h(init()));

  //Cost unitaries
  auto cost = utils::constexpr_for<0, g.size()-1, 1, maxcut_cost_function>(tag<0>(init_state), g, gamma);

  //Mixer unitaries
  auto mixer = rx(beta, cost);  

  //Measure result
  auto qaoa_circuit = measure(mixer);

  // Create empty JSON object to receive results
  utils::json result;
  
  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, qaoa_circuit, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
