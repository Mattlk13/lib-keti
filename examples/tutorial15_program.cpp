/** @file examples/tutorial15_program.cpp

    @brief C++ tutorial-15: Illustration of the quantum program class

    This tutorial illustrates the usage of the quantum program class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{  
  // Set number of qubits
  const size_t nqubits = 9;
  
  // Create quantum program
  QProgram prog;

  prog.rx( 3.141, {0,1,2} );  
  prog.h( {0,1,2} );
  prog.h( 3 );
  prog.rx( 3.141, {3,4,5} );
  prog.cnot( {3,4,5}, {6,7,8} );
  prog.measure({0,1,2,3,4,5,6,7,8});

  // Create quantum expression as string
  std::string expr = prog.to_string();
  
  std::cout << expr << std::endl;
  
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
