/** @file examples/tutorial04_constexpr_for.cpp

    @brief C++ tutorial-04: Quantum expression with custom constexpr-for loop

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

template<index_t start, index_t end, index_t step, index_t index>
struct body
{  
  template<typename Expr, typename Graph>
  inline constexpr auto operator()(Expr&& expr, Graph) noexcept
  { 
    const auto edge0 = Graph::template from<index>();
    const auto edge1 = Graph::template to<index>();
    return cnot(sel<edge0>(), sel<edge1>(gototag<0>(expr)));
  }
};

int main(int argc, char *argv[])
{
  // Set number of qubits
  const size_t nqubits = 5;

   // Create compile-time ring graph
  constexpr const utils::graph<>
    ::edge<0,1>
    ::edge<1,2>
    ::edge<2,3>
    ::edge<3,4>
    ::edge<4,0> g;

  
  // Create simple quantum expression:
  // cnots for every connection in graph g
  auto expr = measure(all(
                          utils::constexpr_for<0, g.size()-1, 1,body>(tag<0>(init()), g)
                          )
                      );
  
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";

  return 0;
}
