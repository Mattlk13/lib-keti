/** @file python_api/PyQFilters.hpp

    @brief Python API quantum filter classes

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup py_filters Quantum filters

    @ingroup  py_api
*/

#pragma once
#ifndef PYTHON_API_QFILTERS_HPP
#define PYTHON_API_QFILTERS_HPP

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>

#include "PyQBase.hpp"

/**
   @brief Select filter class

   @ingroup py_filters
*/
class PyQFilterSelect : public PyQBase
{
public:
  /// @brief Default constructor
  PyQFilterSelect() = default;

  /// @brief Constructor
  PyQFilterSelect(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Constructor
  PyQFilterSelect(const std::vector<std::size_t>& sel)
    : sel(sel)
    , PyQBase()
  {}

  /// @brief Constructor
  PyQFilterSelect(const std::vector<std::size_t>& sel,
                  std::shared_ptr<PyQObject> obj)
    : sel(sel)
    , PyQBase(obj)
  {}

  /// @brief Prints the content of the current filter
  std::string print() const override
  {
    std::stringstream ss;
    for (auto it = sel.cbegin(); it != sel.cend(); it++)
      ss << std::to_string(*it) << (it + 1 == sel.cend() ? "" : ",");
    return "LibKet::filter::sel<" + ss.str() + ">(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current filter object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQFilterSelect> tmp =
      std::make_shared<PyQFilterSelect>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::vector<std::size_t> sel;
};

/**
   @brief Select-all filter class

   @ingroup py_filters
*/
class PyQFilterSelectAll : public PyQBase
{
public:
  /// @brief Default constructor
  PyQFilterSelectAll() = default;

  /// @brief Constructor
  PyQFilterSelectAll(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Prints the content of the current filter
  std::string print() const override
  {
    return "LibKet::filter::all(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current filter object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQFilterSelectAll> tmp =
      std::make_shared<PyQFilterSelectAll>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief Shift filter class

   @ingroup py_filters
*/
class PyQFilterShift : public PyQBase
{
public:
  /// @brief Default constructor
  PyQFilterShift() = default;

  /// @brief Constructor
  PyQFilterShift(std::shared_ptr<PyQObject> obj)
    : offset(0)
    , PyQBase(obj)
  {}

  /// @brief Constructor
  PyQFilterShift(long int offset)
    : offset(offset)
    , PyQBase()
  {}

  /// @brief Constructor
  PyQFilterShift(long int offset, std::shared_ptr<PyQObject> obj)
    : offset(offset)
    , PyQBase(obj)
  {}

  /// @brief Prints the content of the current filter
  std::string print() const override
  {
    return "LibKet::filter::shift<" + std::to_string(offset) + ">(" +
           _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current filter object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQFilterShift> tmp =
      std::make_shared<PyQFilterShift>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  long int offset;
};

/**
   @brief Tag filter class

   @ingroup py_filters
*/
class PyQFilterTag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQFilterTag() = default;

  /// @brief Constructor
  PyQFilterTag(std::shared_ptr<PyQObject> obj)
    : tag(0)
    , PyQBase(obj)
  {}

  /// @brief Constructor
  PyQFilterTag(std::size_t tag)
    : tag(tag)
    , PyQBase()
  {}

  /// @brief Constructor
  PyQFilterTag(std::size_t tag, std::shared_ptr<PyQObject> obj)
    : tag(tag)
    , PyQBase(obj)
  {}

  /// @brief Prints the content of the current filter
  std::string print() const override
  {
    return "LibKet::filter::tag<" + std::to_string(tag) + ">(" + _obj->print() +
           ")";
  }

  /// @brief Returns duplicate of the current filter object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQFilterTag> tmp = std::make_shared<PyQFilterTag>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::size_t tag;
};

/**
   @brief Goto-tag filter class

   @ingroup py_filters
*/
class PyQFilterGotoTag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQFilterGotoTag() = default;

  /// @brief Constructor
  PyQFilterGotoTag(std::shared_ptr<PyQObject> obj)
    : tag(0)
    , PyQBase(obj)
  {}

  /// @brief Constructor
  PyQFilterGotoTag(std::size_t tag)
    : tag(tag)
    , PyQBase()
  {}

  /// @brief Constructor
  PyQFilterGotoTag(std::size_t tag, std::shared_ptr<PyQObject> obj)
    : tag(tag)
    , PyQBase(obj)
  {}

  /// @brief Prints the content of the current filter
  std::string print() const override
  {
    return "LibKet::filter::gototag<" + std::to_string(tag) + ">(" +
           _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current filter object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQFilterGotoTag> tmp =
      std::make_shared<PyQFilterGotoTag>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::size_t tag;
};

/**
   @brief Range filter class

   @ingroup py_filters
*/
class PyQRange : public PyQBase
{
public:
  /// @brief Default constructor
  PyQRange() = default;

  /// @brief Constructor
  PyQRange(std::shared_ptr<PyQObject> obj)
    : begin(0)
    , end(0)
    , PyQBase(obj)
  {}

  /// @brief Constructor
  PyQRange(std::size_t begin, std::size_t end)
    : begin(begin)
    , end(end)
    , PyQBase()
  {}

  /// @brief Constructor
  PyQRange(std::size_t begin, std::size_t end, std::shared_ptr<PyQObject> obj)
    : begin(begin)
    , end(end)
    , PyQBase(obj)
  {}

  /// @brief Prints the content of the current filter
  std::string print() const override
  {
    return "LibKet::filter::range<" + std::to_string(begin) + "," +
           std::to_string(end) + ">(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current filter object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQRange> tmp = std::make_shared<PyQRange>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::size_t begin, end;
};

/**
   @brief Quantum register filter class

   @ingroup py_filters
*/
class PyQRegister : public PyQBase
{
public:
  /// @brief Default constructor
  PyQRegister() = default;

  /// @brief Constructor
  PyQRegister(std::shared_ptr<PyQObject> obj)
    : begin(0)
    , qubits(0)
    , PyQBase(obj)
  {}

  /// @brief Constructor
  PyQRegister(std::size_t begin, std::size_t qubits)
    : begin(begin)
    , qubits(qubits)
    , PyQBase()
  {}

  /// @brief Constructor
  PyQRegister(std::size_t begin,
              std::size_t qubits,
              std::shared_ptr<PyQObject> obj)
    : begin(begin)
    , qubits(qubits)
    , PyQBase(obj)
  {}

  /// @brief Prints the content of the current filter
  std::string print() const override
  {
    return "LibKet::filter::qureg<" + std::to_string(begin) + "," +
           std::to_string(qubits) + ">(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current filter object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQRegister> tmp = std::make_shared<PyQRegister>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::size_t begin, qubits;
};

/**
   @brief Qubit filter class

   @ingroup py_filters
*/
class PyQBit : public PyQBase
{
public:
  /// @brief Default constructor
  PyQBit() = default;

  /// @brief Constructor
  PyQBit(std::shared_ptr<PyQObject> obj)
    : id(0)
    , PyQBase(obj)
  {}

  /// @brief Constructor
  PyQBit(std::size_t id)
    : id(id)
    , PyQBase()
  {}

  /// @brief Constructor
  PyQBit(std::size_t id, std::shared_ptr<PyQObject> obj)
    : id(id)
    , PyQBase(obj)
  {}

  /// @brief Prints the content of the current filter
  std::string print() const override
  {
    return "LibKet::filter::qubit<" + std::to_string(id) + ">(" +
           _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current filter object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQBit> tmp = std::make_shared<PyQBit>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::size_t id;
};

/**
   @brief Creates quantum filter Python module

   @ingroup py_filters
*/
void
pybind11_init_filter(pybind11::module& m);

#endif // PYTHON_API_QFILTERS_HPP
