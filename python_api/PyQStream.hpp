/** @file python_api/PyQStream.hpp

    @brief Python API quantum stream class

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef PYTHON_API_QSTREAM_HPP
#define PYTHON_API_QSTREAM_HPP

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "PyQBase.hpp"

/**
   @brief Quantum stream class

   @ingroup py_api
*/
class PyQStream : public PyQBase
{};

/**
   @brief Creates LibKet quantum job execution stream Python module

   @ingroup py_api
*/
void
pybind11_init_stream(pybind11::module& m);

#endif // PYTHON_API_QSTREAM_HPP
