/** @file python_api/PyQExpression.hpp

    @brief Python API quantum data classes

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef PYTHON_API_QDATA_HPP
#define PYTHON_API_QDATA_HPP

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "json/json.hpp"
using json = nlohmann::json;

#include "PyQBase.hpp"

/**
   @brief Quantum backends

   @ingroup py_api
*/
enum class PyQBackendType
{
  AQASM,
  cQASMv1,
  OpenQASMv2,
  OpenQL,
  QASM,
  Quil,
  QX
};

/**
   @brief Quantum data class

   @ingroup py_api
*/
class PyQExpression : public PyQBase
{
public:
  /// Default constructor
  PyQExpression()
    : PyQExpression(0, PyQBackendType::QASM)
  {}

  /// @brief Constructor
  PyQExpression(std::size_t qubits)
    : PyQExpression(qubits, PyQBackendType::QASM)
  {}

  /// @brief Constructor
  PyQExpression(std::size_t qubits, PyQBackendType backend)
    : qubits(qubits)
    , backend(backend)
  {}

  /// @brief Prints filter
  std::string print() const override;

  /// @brief Generates QASM code
  std::string to_qasm(std::shared_ptr<PyQBase> obj) const
  {
    return "Not implemented yet";//gen_expression(obj->print(), )
  }

public:
  std::size_t qubits;
  PyQBackendType backend;
};

/**
   @brief Creates LibKet quantum data Python module

   @ingroup py_api
*/
void
pybind11_init_data(pybind11::module& m);

#endif // PYTHON_API_QDATA_HPP
