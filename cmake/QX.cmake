########################################################################
# QX.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# QX
########################################################################

include(FetchContent)
FetchContent_Declare(
  qx-simulator
  GIT_REPOSITORY https://github.com/QE-Lab/qx-simulator.git
  GIT_TAG        v0.4.2
  )

if (LIBKET_BUILTIN_QX)

  FetchContent_GetProperties(qx)
  if(NOT qx-simulator_POPULATED)
    message(STATUS "Fetching QX-simulator")
    FetchContent_Populate(qx-simulator)
    message(STATUS "Fetching QX-simulator -- done")
  endif()
  
  set(QX_BINARY_DIR ${qx-simulator_BINARY_DIR}/build)
  set(QX_SOURCE_DIR ${qx-simulator_SOURCE_DIR})
  
  # Add include directory
  include_directories("${QX_SOURCE_DIR}/include")
  
else()

  # Add include directory
  if(QX_INCLUDE_PATH)
    include_directories(${QX_INCLUDE_PATH})
  else()
    message(WARNING "Variable QX_INCLUDE_PATH is not defined. LibKet might be unable to find QX include files.")
  endif()
    
endif()

# Process QX project
if (NOT TARGET qx)
  add_subdirectory(${QX_SOURCE_DIR} ${QX_BINARY_DIR})
endif()

# Get compile definitions
get_target_property(QX_COMPILE_DEFINITIONS qx COMPILE_DEFINITIONS)

# Add include directory
include_directories("${QX_SOURCE_DIR}/include")

# Add link directory
link_directories("${QX_BINARY_DIR}")

# Add library
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   qx)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES qx)
