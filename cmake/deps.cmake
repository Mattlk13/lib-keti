########################################################################
# deps.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# external dependencies
########################################################################

include(FetchContent)
message(STATUS "Fetching external dependencies")

########################################################################
# Armadillo
########################################################################

FetchContent_Declare(
  armadillo
  URL http://sourceforge.net/projects/arma/files/armadillo-10.8.2.tar.xz
  )

FetchContent_GetProperties(armadillo)
if(NOT armadillo_POPULATED)
  message(STATUS "  Armadillo")
  FetchContent_Populate(armadillo)
endif()

########################################################################
# OptimLib
########################################################################

FetchContent_Declare(
  optim
  GIT_REPOSITORY https://github.com/kthohr/optim
  )

FetchContent_GetProperties(optim)
if(NOT optim_POPULATED)
  message(STATUS "  OptimLib")
  FetchContent_Populate(optim)
endif()

########################################################################
# PEGTL
########################################################################

FetchContent_Declare(
  pegtl
  URL https://github.com/taocpp/PEGTL/archive/refs/tags/2.8.3.zip
  )

FetchContent_GetProperties(pegtl)
if(NOT pegtl_POPULATED)
  message(STATUS "  PEGTL")
  FetchContent_Populate(pegtl)
endif()

########################################################################
# PyBind11
########################################################################

FetchContent_Declare(
  pybind11
  URL https://github.com/pybind/pybind11/archive/refs/tags/v2.9.1.zip
  )

FetchContent_GetProperties(pybind11)
if(NOT pybind11_POPULATED)
  message(STATUS "  PyBind11")
  FetchContent_Populate(pybind11)
endif()

########################################################################
# Universal
########################################################################

FetchContent_Declare(
  universal
  URL https://github.com/stillwater-sc/universal/archive/refs/tags/v3.52.zip
  )

FetchContent_GetProperties(universal)
if(NOT universal_POPULATED)
  message(STATUS "  Universal")
  FetchContent_Populate(universal)
endif()

message(STATUS "Fetching external dependencies -- done")
