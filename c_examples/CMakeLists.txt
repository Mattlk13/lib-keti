########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

file(GLOB LIBKET_C_EXAMPLES_SRC RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.C *.c *.c++ *.cc *.cpp *.cxx)
include_directories(${CMAKE_SOURCE_DIR}/c_api)
add_executables("${LIBKET_C_EXAMPLES_SRC}" "")
