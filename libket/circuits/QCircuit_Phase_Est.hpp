/** @file libket/circuits/QCircuit_Phase_Est.hpp

    @brief C++ API phase estimation circuit class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers

    @defgroup phaseest Phase estimation circuit

    @ingroup circuits
*/

#pragma once
#ifndef QCIRCUIT_PHASE_EST_HPP
#define QCIRCUIT_PHASE_EST_HPP

#include <QCircuits.hpp>
#include <QFilter.hpp>
#include <QGates.hpp>
#include <QUtils.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;
  
namespace circuits {
/**
@brief Phase estimation circuit class

The LibKet phase estimation circuit class implements a circuit which estimates
the phase of a given unitary

@ingroup phaseest
*/
template<typename _Ugate, typename _tol = QConst_M_ZERO_t>
class QCircuit_Phase_Est : public QCircuit
{
private:
public:
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& to, T1&& t1) const noexcept;

  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

  /// Show show type
  template<std::size_t level = 1>
  std::string show() const noexcept
  {
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }
  
  /// Apply function
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           QBackendType _qbackend>
  inline static QExpression<_qubits, _qbackend>& apply(
    QExpression<_qubits, _qbackend>& expr) noexcept
  {
    auto e = qft<>(_filter0);
    return e(expr);
  }
};

///@ingroup phaseest
///@{
  
/**

@brief Phase estimation circuit creator

This overload of the LibKet::circuits:phase_est() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::phase_est(expr);
\endcode
*/

template<typename _tol = QConst_M_ZERO_t>
inline constexpr auto
phase_est() noexcept
{
  return TernaryQGate<filters::QFilter,
                      filters::QFilter,
                      QCircuit_Phase_Est<_tol>>(filters::QFilter{},
                                                filters::QFilter{});
}

/**

@brief Phase estimation circuit creator


This overload of the LibKet::circuits:phase_est() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::phase_est(expr);
\endcode
*/

template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1>
inline constexpr auto
phase_est(_Ugate, const _expr0& expr0, const _expr1& expr1) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      QCircuit_Phase_Est<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}
/**
@brief Phase estimation circuit creator

This overload of the LibKet::circuits::phase_est() function accepts an
expression as universal reference
*/
template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1>
inline constexpr auto
phase_est(_Ugate, const _expr0& expr0, _expr1&& expr1) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      QCircuit_Phase_Est<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1>
inline constexpr auto
phase_est(_Ugate, _expr0&& expr0, const _expr1& expr1) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      QCircuit_Phase_Est<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1>
inline constexpr auto
phase_est(_Ugate, _expr0&& expr0, _expr1&& expr1) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      QCircuit_Phase_Est<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief Phase estimation circuit creator

Function alias for LibKet::circuits::phase_est
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
PHASE_EST(Args&&... args)
{
  return phase_est<_tol>(std::forward<Args>(args)...);
}

/**
@brief Phase estimation circuit creator

Function alias for LibKet::circuits::phase_est
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
Phase_Est(Args&&... args)
{
  return phase_est<_tol>(std::forward<Args>(args)...);
}

///@}

/// Operator() - by constant reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Phase_Est<_Ugate, _tol>::operator()(const T0& t0,
                                             const T1& t1) const noexcept
{
  return phase_est<_Ugate, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}
/// Operator() - by universal reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Phase_Est<_Ugate, _tol>::operator()(T0&& t0, T1&& t1) const noexcept
{
  return arb_ctrl<_Ugate, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Phase_Est<_Ugate, _tol>::operator()(T0&& t0,
                                             const T1& t1) const noexcept
{
  return arb_ctrl<_Ugate, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Phase_Est<_Ugate, _tol>::operator()(const T0& t0,
                                             T1&& t1) const noexcept
{
  return arb_ctrl<_Ugate, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief Libket show circuit type - specialization for phase estimation circuit
   objects

   @ingroup phaseest
*/
template<std::size_t level = 1, typename _Ugate, typename _tol = QConst_M_ZERO_t>
inline static auto
show(const QCircuit_Phase_Est<_Ugate, _tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "PHASE_EST\n";

  return circuit;
}

} // namespace circuits

} // namespace libket

#endif // QCIRCUIT_PHASE_EST_HPP
