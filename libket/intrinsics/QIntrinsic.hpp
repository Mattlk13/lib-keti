/** @file libket/intrinsics/QIntrinsic.hpp

    @brief C++ API quantum intrinsic data type classes

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup intrinsics
 */

#pragma once
#ifndef QINTRINSIC_HPP
#define QINTRINSIC_HPP

#include <QBase.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>

namespace LibKet {

/** @namespace LibKet::intrinsics

    @brief
    The LibKet::intrinsics namespace, containing all intrinsic data types of the
    LibKet project

    The LibKet::intrinsics namespace contains all intrinsic data types
    of the LibKet project that is exposed to the end-user. All
    functionality in this namespace has a stable API over future
    LibKet releases.
 */
namespace intrinsics {

/**
@brief Quantum intrinsic data type base class

The Quantum intrinsic data type base class is the base class of
all Quantum intrinsic classes.
*/
class QIntrinsic : public QBase
{};

} // namespace intrinsics

} // namespace LibKet

#endif // QINTRINSIC_HPP
