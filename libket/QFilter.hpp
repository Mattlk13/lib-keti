/** @file libket/QFilter.hpp

    @brief C++ API quantum filter classes

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup filters Quantum filters

    @ingroup cxx_api

    Quantum filters are used to select a subset of qubits of a
    LibKet::QExpression object to which a particular quantum gate is
    applied. The following code creates a quantum filter that selects
    the first three qubits:

    \code
    auto filter = QFilterSelectRange<0,2>();
    \endcode

    The same can be achieved using wrapper functions:

    \code
    auto filter = filters::range<0,2>();
    \endcode

    Quantum filters can be combined to a quantum filter chain. The
    following code creates a quantum filter that selects the first
    three even qubits and shifts them by +1 afterwards:

    \code
    auto filter = filters::shift<1>(filters::sel<0,2,4>());
    \endcode

    Quantum filter chains can also be created by creating individual
    LibKet::filters::QFilter objects and using their
    LibKet::filters::QFilter::operator()

    \code
    auto filter = QFilterShift<1>() ( QFilterSelect<0,2,4>() );
    \endcode

    LibKet provides the following quantum filters (with wrapper
    functions):

    - LibKet::filters::QFilterSelect      (LibKet::filters::sel)
    - LibKet::filters::QFilterSelectAll   (LibKet::filters::all)
    - LibKet::filters::QFilterShift       (LibKet::filters::shift)
    - LibKet::filters::QFilterSelectRange (LibKet::filters::range)
    - LibKet::filters::QRegister          (LibKet::filters::qureg)
    - LibKet::filters::QBit               (LibKet::filters::qubit)

    All quantum filters are implemented as expression templates so
    that, when multiple filters are combined to a filter chain, a
    single LibKet::filters::QFilter object is created at compile time.
 */

#pragma once
#ifndef QFILTER_HPP
#define QFILTER_HPP

#include <QBase.hpp>
#include <QExpression.hpp>
#include <QUtils.hpp>

#include <list>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _expr, typename _gate, typename _filter>
class UnaryQGate;
template<typename _expr0, typename _expr1, typename _gate, typename _filter>
class BinaryQGate;
template<typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter>
class TernaryQGate;

} // namespace gates

/** @namespace LibKet::filters

   @brief
   The LibKet::filters namespace, containing all filters of the LibKet project

   The LibKet::filters namespace contains all filters of the LibKet
   project that is exposed to the end-user. All functionality in
   this namespace has a stable API over future LibKet releases.
 */
namespace filters {

/// Forward declarations
class QFilter;
template<std::size_t... _ids>
class QFilterSelect;
class QFilterSelectAll;
template<index_t _offset>
class QFilterShift;
template<std::size_t _tag, typename _filter, typename _tagged_filter>
class QFilterTag;
template<std::size_t _tag, typename _filter>
class QFilterGotoTag;
class LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION;
class LIBKET_ERROR_TAG_NOT_FOUND;
  
/// Fake compile-time error: QFilter does not provide range function
class LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION : public QBase
{
public:
  /// Compile-time integer sequence for range loops
  template<std::size_t _qubits, QBackendType _qbackend>
  inline static constexpr auto range(const QExpression<_qubits, _qbackend>&) noexcept
  {
    return utils::sequence<>{};
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint filter
  ///
  /// @result expression representing the adjoint filter
  // auto dagger() const noexcept
  // {    
  //   using ::LibKet::dagger;
  //   return dagger(*this);
  // }
};

/// Fake compile-time error: Tag not found
class LIBKET_ERROR_TAG_NOT_FOUND : public QBase
{
public:
  /// Quantum filter type
  using filter_t = QFilter;

  /// Compile-time size
  template<std::size_t _qubits>
  inline static constexpr std::size_t size() noexcept
  {
    return 0;
  }

  /// Compile-time integer sequence for range loops
  template<std::size_t _qubits, QBackendType _qbackend>
  inline static constexpr auto range(const QExpression<_qubits, _qbackend>&) noexcept
  {
    return LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION{};
  }

  /// Operator<< - specialization of concatenation operator for all object
  template<typename _expr>
  inline constexpr auto operator<<(const _expr& expr) const noexcept
  {
    return expr;
  }

  /// Operator<< - specialization of concatenation operator for all object
  template<typename _expr>
  inline constexpr auto operator<<(_expr&& expr) const noexcept
  {
    return expr;
  }

  /// Operator>> - specialization of overwrite operator for all object
  template<typename _expr>
  inline constexpr auto operator>>(const _expr& expr) const noexcept
  {
    return expr;
  }

  /// Operator>> - specialization of overwrite operator for all object
  template<typename _expr>
  inline constexpr auto operator>>(_expr&& expr) const noexcept
  {
    return expr;
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint filter
  ///
  /// @result expression representing the adjoint filter
  // auto dagger() const noexcept
  // {    
  //   using ::LibKet::dagger;
  //   return dagger(*this);
  // }
};

/**
   @brief Type trait extracts the filter from an expression

   @ingroup filters
*/
template<typename _expr,
         bool = std::is_base_of<filters::QFilter,
                                typename std::decay<_expr>::type>::value>
struct getFilter;

template<typename _expr>
struct getFilter<_expr, true>
{
  using type = typename std::decay<_expr>::type;
};

template<typename _expr>
struct getFilter<_expr, false>
{
  using type =
    typename getFilter<typename std::decay<_expr>::type::filter_t>::type;
};

/**
   @brief Type trait extracts the previous filter from an expression

   @ingroup filters
*/
template<typename _expr,
         bool = std::is_base_of<filters::QFilter,
                                typename std::decay<_expr>::type>::value>
struct getPreviousFilter;

template<typename _expr>
struct getPreviousFilter<_expr, true>
{
  using type = typename std::decay<_expr>::type;
};

template<typename _expr, typename _gate, typename _filter>
struct getPreviousFilter<const gates::UnaryQGate<_expr, _gate, _filter>, false>
{
  using type =
    typename getFilter<typename std::decay<_expr>::type::filter_t>::type;
};

template<typename _expr, typename _gate, typename _filter>
struct getPreviousFilter<gates::UnaryQGate<_expr, _gate, _filter>, false>
{
  using type =
    typename getFilter<typename std::decay<_expr>::type::filter_t>::type;
};

template<typename _expr0, typename _expr1, typename _gate, typename _filter>
struct getPreviousFilter<
  const gates::BinaryQGate<_expr0, _expr1, _gate, _filter>,
  false>
{
  using type = decltype(
    typename getFilter<typename std::decay<_expr0>::type::filter_t>::type{}
    << typename getFilter<typename std::decay<_expr1>::type::filter_t>::type{});
};

template<typename _expr0, typename _expr1, typename _gate, typename _filter>
struct getPreviousFilter<gates::BinaryQGate<_expr0, _expr1, _gate, _filter>,
                         false>
{
  using type = decltype(
    typename getFilter<typename std::decay<_expr0>::type::filter_t>::type{}
    << typename getFilter<typename std::decay<_expr1>::type::filter_t>::type{});
};

template<typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter>
struct getPreviousFilter<
  const gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>,
  false>
{
  using type = decltype(
    typename getFilter<typename std::decay<_expr0>::type::filter_t>::type{}
    << typename getFilter<typename std::decay<_expr1>::type::filter_t>::type{}
    << typename getFilter<typename std::decay<_expr2>::type::filter_t>::type{});
};

template<typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter>
struct getPreviousFilter<
  gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>,
  false>
{
  using type = decltype(
    typename getFilter<typename std::decay<_expr0>::type::filter_t>::type{}
    << typename getFilter<typename std::decay<_expr1>::type::filter_t>::type{}
    << typename getFilter<typename std::decay<_expr2>::type::filter_t>::type{});
};

/**
@brief Creates a QFilterSelect object, deducing the indices from
the indices of the argument

   @ingroup select
*/
template<std::size_t... _ids>
inline constexpr auto make_QFilterSelect(utils::sequence<_ids...>) noexcept
{
  return QFilterSelect<_ids...>{};
}

/**
@brief Filter base class

The Quantum filter base class is the base class of all
Quantum filter classes. It is used to identify a generic
type `T` as Quantum filter, e.g.

\code
template <typename T
bool is_filter(T t) { return std::is_base_of<QFilter, typename
std::decay<T>::type>::value; }
\endcode

@ingroup filters
*/
class QFilter : public QBase
{
public:
  /// Quantum filter type
  using filter_t = QFilter;

  /// Quantum expression type
  using expr_t = QFilter;

  /// Quantum gate type
  using gate_t = QFilter;

  /// Compile-time size
  template<std::size_t _qubits>
  inline static constexpr std::size_t size() noexcept
  {
    return _qubits;
  }

  /// Compile-time integer sequence for range loops
  template<std::size_t _qubits, QBackendType _qbackend>
  inline static constexpr auto range(const QExpression<_qubits, _qbackend>&) noexcept
  {
    return LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION{};
  }

  /// Constructor
  constexpr QFilter() = default;

  /// Operator()
  inline constexpr auto operator()() const noexcept
  {
    return *this;
  }
  
  /// Operator()
  template<typename _expr>
  inline constexpr auto operator()(const _expr& expr) const noexcept
  {
    return expr;
  }

  /// Operator()
  template<typename _expr>
  inline constexpr auto operator()(_expr&& expr) const noexcept
  {
    return expr;
  }

  /// Operator<< - concatenation operator
  template<typename _expr>
  inline constexpr auto operator<<(const _expr& expr) const noexcept
  {
    return expr;
  }

  /// Operator<< - concatenation operator
  template<typename _expr>
  inline constexpr auto operator<<(_expr&& expr) const noexcept
  {
    return expr;
  }

  /// Operator>> - overwrite operator
  template<typename _expr>
  inline constexpr auto operator>>(const _expr&) const noexcept
  {
    return *this;
  }

  /// Operator>> - overwrite operator
  template<typename _expr>
  inline constexpr auto operator>>(_expr&&) const noexcept
  {
    return *this;
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint filter
  ///
  /// @result expression representing the adjoint filter
  // auto dagger() const noexcept
  // {    
  //   using ::LibKet::dagger;
  //   return dagger(*this);
  // }
};

/**
 @brief Serialize operation
*/
std::ostream&
operator<<(std::ostream& os, const QFilter& filter)
{
  os << "filter(";
  if (!is_set(os))
    os << ")";
  else
    os << set;
  return os;
}

/**
@brief Select-range filter class

The select-range filter class extracts a range of quantum bits
(qubits) from a LibKet::QExpression object. The following code selects the
first three qubits

\code
auto filter = QFilterSelectRange<0,2>();
\endcode

   @defgroup range Range filter

   @ingroup filters
*/
template<std::size_t _begin, std::size_t _end>
using QFilterSelectRange =
  decltype(make_QFilterSelect(utils::sequence_t<_begin, _end>{}));

/**
@brief Quantum register class

The Quantum register class, LibKet::QRegister, filters a
set of consecutive quantum bits (qubits) from a LibKet::QExpression
object. The following code selects a quantum register of length 8
starting at position 2

\code
auto filter = QRegisteri<2,8>();
\endcode

   @defgroup register Quantum register

   @ingroup filters
*/
template<std::size_t _begin, std::size_t _qubits>
using QRegister = QFilterSelectRange<_begin, _begin + _qubits - 1>;

/**
@brief Quantum bit class

The Quantum bit class, LibKet::QBit, filters an individual
quantum bit (qubits) from a LibKet::QExpression object. The following
code selects a quantum qubit at position 3

\code
auto filter = QBit<3>();
\endcode

@defgroup qubit Quantum bit

@ingroup filters
*/
template<std::size_t _id>
using QBit = QRegister<_id, 1>;

/**
@brief Select-all filter class

The select-all filter class resets all filters and
selects all available quantum bits (qubits) from a LibKet::QExpression
object. The following code selects all qubits

\code
auto filter = QFilterSelectAll();
\endcode

@defgroup all Select all filter

@ingroup filters
*/
class QFilterSelectAll : public QFilter
{
public:
  /// Quantum filter type
  using filter_t = QFilterSelectAll;

  /// Compile-time size
  template<std::size_t _qubits>
  inline static constexpr std::size_t size() noexcept
  {
    return _qubits;
  }

  /// Compile-time integer sequence for range loops
  template<std::size_t _qubits, QBackendType _qbackend>
  inline static constexpr auto range(const QExpression<_qubits, _qbackend>&) noexcept
  {
    return utils::sequence_t<_qubits>{};
  }

  /// Constructor
  constexpr QFilterSelectAll() = default;

  /// Operator()
  inline constexpr auto operator()() const noexcept
  {
    return *this;
  }
  
  /// Operator() - specialization for QFilter object
  inline constexpr auto operator()(const QFilter&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilter object
  inline constexpr auto operator()(QFilter&&) const noexcept { return *this; }

  /// Operator() - specialization for QFilterSelect object
  template<std::size_t... __ids>
  inline constexpr auto operator()(
    const QFilterSelect<__ids...>&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilterSelect object
  template<std::size_t... __ids>
  inline constexpr auto operator()(QFilterSelect<__ids...>&&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilterSelectAll object
  inline constexpr auto operator()(const QFilterSelectAll&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilterSelectAll object
  inline constexpr auto operator()(QFilterSelectAll&&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilterShift object
  template<index_t _offset>
  inline constexpr auto operator()(const QFilterShift<_offset>&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilterShift object
  template<index_t _offset>
  inline constexpr auto operator()(QFilterShift<_offset>&&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilterTag object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator()(
    const QFilterTag<_tag, _filter, _tagged_filter>&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this)(_filter{})), _tagged_filter>{};
  }

  /// Operator() - specialization for QFilterTag object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator()(
    QFilterTag<_tag, _filter, _tagged_filter>&&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this)(_filter{})), _tagged_filter>{};
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename _filter>
  inline constexpr auto operator()(
    const gates::UnaryQGate<_expr, _gate, _filter>& gate) const noexcept
  {
    return gates::UnaryQGate<_expr, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename _filter>
  inline constexpr auto operator()(
    gates::UnaryQGate<_expr, _gate, _filter>&& gate) const noexcept
  {
    return gates::UnaryQGate<_expr, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename _filter>
  inline constexpr auto operator()(
    const gates::BinaryQGate<_expr0, _expr1, _gate, _filter>& gate)
    const noexcept
  {
    return gates::
      BinaryQGate<_expr0, _expr1, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename _filter>
  inline constexpr auto operator()(
    gates::BinaryQGate<_expr0, _expr1, _gate, _filter>&& gate) const noexcept
  {
    return gates::
      BinaryQGate<_expr0, _expr1, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename _filter>
  inline constexpr auto operator()(
    const gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>& gate)
    const noexcept
  {
    return gates::
      TernaryQGate<_expr0, _expr1, _expr2, _gate, decltype((*this)(_filter{}))>(
        gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename _filter>
  inline constexpr auto operator()(
    gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>&& gate)
    const noexcept
  {
    return gates::
      TernaryQGate<_expr0, _expr1, _expr2, _gate, decltype((*this)(_filter{}))>(
        gate);
  }

  /// Operator<< - specialization of concatenation operator for QFilter object
  inline constexpr auto operator<<(const QFilter&) const noexcept
  {
    return *this;
  }

  /// Operator<< - specialization of concatenation operator for QFilter object
  inline constexpr auto operator<<(QFilter&&) const noexcept { return *this; }

  /// Operator<< - specialization of concatenation operator for QFilterSelect
  /// object
  template<std::size_t... __ids>
  inline constexpr auto operator<<(
    const QFilterSelect<__ids...>&) const noexcept
  {
    return *this;
  }

  /// Operator<< - specialization of concatenation operator for QFilterSelect
  /// object
  template<std::size_t... __ids>
  inline constexpr auto operator<<(QFilterSelect<__ids...>&&) const noexcept
  {
    return *this;
  }

  /// Operator<< - specialization of concatenation operator for QFilterSelectAll
  /// object
  inline constexpr auto operator<<(const QFilterSelectAll&) const noexcept
  {
    return *this;
  }

  /// Operator<< - specialization of concatenation operator for QFilterSelectAll
  /// object
  inline constexpr auto operator<<(QFilterSelectAll&&) const noexcept
  {
    return *this;
  }

  /// Operator<< - specialization of concatenation operator for QFilterTag
  /// object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator<<(
    const QFilterTag<_tag, _filter, _tagged_filter>&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this) << _filter{}), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterTag
  /// object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator<<(
    QFilterTag<_tag, _filter, _tagged_filter>&&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this) << _filter{}), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for
  /// LIBKET_ERROR_TAG_NOT_FOUND object
  inline constexpr auto operator<<(
    const LIBKET_ERROR_TAG_NOT_FOUND&) const noexcept
  {
    return *this;
  }

  /// Operator<< - specialization of concatenation operator for
  /// LIBKET_ERROR_TAG_NOT_FOUND object
  inline constexpr auto operator<<(
    const LIBKET_ERROR_TAG_NOT_FOUND&&) const noexcept
  {
    return *this;
  }

  /// Operator>> - overwrite operator
  template<typename _expr>
  inline constexpr auto operator>>(const _expr&) const noexcept
  {
    return *this;
  }

  /// Operator>> - overwrite operator
  template<typename _expr>
  inline constexpr auto operator>>(_expr&&) const noexcept
  {
    return *this;
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint filter
  ///
  /// @result expression representing the adjoint filter
  // auto dagger() const noexcept
  // {    
  //   using ::LibKet::dagger;
  //   return dagger(*this);
  // }
};

/**
 @brief Serialize operation
*/
std::ostream&
operator<<(std::ostream& os, const QFilterSelectAll& filter)
{
  os << "all(";
  if (!is_set(os))
    os << ")";
  else
    os << set;
  return os;
}

/**
@brief Select filter class

The select filter class extracts a subset of
quantum bits (qubits) from a LibKet::QExpression object. The following
code select the first three even qubits

\code
auto filter = QFilterSelect<0,2,4>();
\endcode

@defgroup select Select filter

@ingroup filters
*/
template<std::size_t... _ids>
class QFilterSelect : public QFilter
{
public:
  /// Quantum filter type
  using filter_t = QFilterSelect<_ids...>;

  /// Compile-time size
  template<std::size_t _qubits>
  inline static constexpr std::size_t size() noexcept
  {
    return sizeof...(_ids);
  };

  /// Compile-time integer sequence for range loops
  template<std::size_t _qubits, QBackendType _qbackend>
  inline static constexpr auto range(const QExpression<_qubits, _qbackend>&) noexcept
  {
    // TODO: Must add bound checks
    return utils::sequence<_ids...>{};
  }

  /// Constructor
  constexpr QFilterSelect() = default;

  /// Operator()
  inline constexpr auto operator()() const noexcept
  {
    return *this;
  }
  
  /// Operator() - specialization for QFilter object
  inline constexpr auto operator()(const QFilter&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilter object
  inline constexpr auto operator()(QFilter&&) const noexcept { return *this; }

  /// Operator() - specialization for QFilterSelect object
  template<std::size_t... __ids>
  inline constexpr auto operator()(
    const QFilterSelect<__ids...>&) const noexcept
  {
    return QFilterSelect<std::get<_ids>(
      utils::forward_as_tuple(__ids...))...>{};
  }

  /// Operator() - specialization for QFilterSelect object
  template<std::size_t... __ids>
  inline constexpr auto operator()(QFilterSelect<__ids...>&&) const noexcept
  {
    return QFilterSelect<std::get<_ids>(
      utils::forward_as_tuple(__ids...))...>{};
  }

  /// Operator() - specialization for QFilterSelectAll object
  inline constexpr auto operator()(const QFilterSelectAll&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilterSelectAll object
  inline constexpr auto operator()(QFilterSelectAll&&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilterShift object
  template<index_t _offset>
  inline constexpr auto operator()(const QFilterShift<_offset>&) const noexcept
  {
    return QFilterSelect<-_offset + _ids...>{};
  }

  /// Operator() - specialization for QFilterShift object
  template<index_t _offset>
  inline constexpr auto operator()(QFilterShift<_offset>&&) const noexcept
  {
    return QFilterSelect<-_offset + _ids...>{};
  }

  /// Operator() - specialization for QFilterTag object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator()(
    const QFilterTag<_tag, _filter, _tagged_filter>&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this)(_filter{})), _tagged_filter>{};
  }

  /// Operator() - specialization for QFilterTag object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator()(
    QFilterTag<_tag, _filter, _tagged_filter>&&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this)(_filter{})), _tagged_filter>{};
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename _filter>
  inline constexpr auto operator()(
    const gates::UnaryQGate<_expr, _gate, _filter>& gate) const noexcept
  {
    return gates::UnaryQGate<_expr, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename _filter>
  inline constexpr auto operator()(
    gates::UnaryQGate<_expr, _gate, _filter>&& gate) const noexcept
  {
    return gates::UnaryQGate<_expr, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename _filter>
  inline constexpr auto operator()(
    const gates::BinaryQGate<_expr0, _expr1, _gate, _filter>& gate)
    const noexcept
  {
    return gates::
      BinaryQGate<_expr0, _expr1, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename _filter>
  inline constexpr auto operator()(
    gates::BinaryQGate<_expr0, _expr1, _gate, _filter>&& gate) const noexcept
  {
    return gates::
      BinaryQGate<_expr0, _expr1, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename _filter>
  inline constexpr auto operator()(
    const gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>& gate)
    const noexcept
  {
    return gates::
      TernaryQGate<_expr0, _expr1, _expr2, _gate, decltype((*this)(_filter{}))>(
        gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename _filter>
  inline constexpr auto operator()(
    gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>&& gate)
    const noexcept
  {
    return gates::
      TernaryQGate<_expr0, _expr1, _expr2, _gate, decltype((*this)(_filter{}))>(
        gate);
  }

  /// Operator<< - specialization of concatenation operator for QFilter object
  inline constexpr auto operator<<(const QFilter&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization of concatenation operator for QFilter object
  inline constexpr auto operator<<(QFilter&&) const noexcept { return *this; }

  /// Operator<< - specialization of concatenation operator for QFilterSelect
  /// object
  template<std::size_t... __ids>
  inline constexpr auto operator<<(
    const QFilterSelect<__ids...>&) const noexcept
  {
    return QFilterSelect<_ids..., __ids...>{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterSelect
  /// object
  template<std::size_t... __ids>
  inline constexpr auto operator<<(QFilterSelect<__ids...>&&) const noexcept
  {
    return QFilterSelect<_ids..., __ids...>{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterSelectAll
  /// object
  inline constexpr auto operator<<(const QFilterSelectAll&) const noexcept
  {
    return QFilterSelectAll{};
  }

  /// Operator() - specialization of concatenation operator for QFilterSelectAll
  /// object
  inline constexpr auto operator<<(QFilterSelectAll&&) const noexcept
  {
    return QFilterSelectAll{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterTag
  /// object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator<<(
    const QFilterTag<_tag, _filter, _tagged_filter>&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this) << _filter{}), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterTag
  /// object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator<<(
    QFilterTag<_tag, _filter, _tagged_filter>&&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this) << _filter{}), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for
  /// LIBKET_ERROR_TAG_NOT_FOUND object
  inline constexpr auto operator<<(
    const LIBKET_ERROR_TAG_NOT_FOUND&) const noexcept
  {
    return *this;
  }

  /// Operator<< - specialization of concatenation operator for
  /// LIBKET_ERROR_TAG_NOT_FOUND object
  inline constexpr auto operator<<(
    const LIBKET_ERROR_TAG_NOT_FOUND&&) const noexcept
  {
    return *this;
  }

  /// Operator>> - overwrite operator
  template<typename _expr>
  inline constexpr auto operator>>(const _expr&) const noexcept
  {
    return *this;
  }

  /// Operator>> - overwrite operator
  template<typename _expr>
  inline constexpr auto operator>>(_expr&&) const noexcept
  {
    return *this;
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint filter
  ///
  /// @result expression representing the adjoint filter
  auto dagger() const noexcept
  {    
    using ::LibKet::dagger;
    return dagger(*this);
  }
};

/**
 @brief Serialize operation
*/
template<std::size_t _id, std::size_t... _ids>
std::ostream&
operator<<(std::ostream& os, const QFilterSelect<_id, _ids...>& filter)
{
  // Note that we must use the selection filter with prior reset since
  // the filter object contains absolute qubit selections
  os << "sel_<" << std::to_string(_id);
  using expander = int[];
  (void)expander{ 0, (void(os << ',' << std::to_string(_ids)), 0)... };
  os << ">(";
  if (!is_set(os))
    os << ")";
  else
    os << set;
  return os;
}

/**
@brief select-shift filter class

The select-shift filter class applies a shift to the quantum filter
chain. The following code selects the first three odd qubits

\code
auto filter = QFiltershift<1>() ( QFilterSelect<0,2,4>() );
\endcode

@defgroup shift Shift filter

@ingroup filters
*/
template<index_t _offset>
class QFilterShift : public QFilter
{
public:
  /// Quantum filter type
  using filter_t = QFilterShift<_offset>;

  /// Compile-time size
  template<std::size_t _qubits>
  inline static constexpr std::size_t size() noexcept
  {
    return _qubits;
  }

  /// Compile-time integer sequence for range loops
  template<std::size_t _qubits, QBackendType _qbackend>
  inline static constexpr auto range(const QExpression<_qubits, _qbackend>&) noexcept
  {
    return LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION{};
  }

  /// Constructor
  constexpr QFilterShift() = default;

  /// Operator()
  inline constexpr auto operator()() const noexcept
  {
    return *this;
  }
  
  /// Operator() - specialization for QFilter object
  inline constexpr auto operator()(const QFilter&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilter object
  inline constexpr auto operator()(QFilter&&) const noexcept { return *this; }

  /// Operator() - specialization for QFilterSelect object
  template<std::size_t... __ids>
  inline constexpr auto operator()(
    const QFilterSelect<__ids...>&) const noexcept
  {
    return QFilterSelect<_offset + __ids...>{};
  }

  /// Operator() - specialization for QFilterSelect object
  template<std::size_t... __ids>
  inline constexpr auto operator()(QFilterSelect<__ids...>&&) const noexcept
  {
    return QFilterSelect<_offset + __ids...>{};
  }

  /// Operator() - specialization for QFilterSelectAll object
  inline constexpr auto operator()(const QFilterSelectAll&) const noexcept
  {
    return QFilterSelectAll{};
  }

  /// Operator() - specialization for QFilterSelectAll object
  inline constexpr auto operator()(QFilterSelectAll&&) const noexcept
  {
    return QFilterSelectAll{};
  }

  /// Operator() - specialization for QFilterShift object
  template<index_t __offset>
  inline constexpr auto operator()(const QFilterShift<__offset>&) const noexcept
  {
    return QFilterShift<_offset + __offset>{};
  }

  /// Operator() - specialization for QFilterShift object
  template<index_t __offset>
  inline constexpr auto operator()(QFilterShift<__offset>&&) const noexcept
  {
    return QFilterShift<_offset + __offset>{};
  }

  /// Operator() - specialization for QFilterTag object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator()(
    const QFilterTag<_tag, _filter, _tagged_filter>&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this)(_filter{})), _tagged_filter>{};
  }

  /// Operator() - specialization for QFilterTag object
  template<std::size_t _tag, typename _filter, typename _tagged_filter>
  inline constexpr auto operator()(
    QFilterTag<_tag, _filter, _tagged_filter>&&) const noexcept
  {
    return QFilterTag<_tag, decltype((*this)(_filter{})), _tagged_filter>{};
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename _filter>
  inline constexpr auto operator()(
    const gates::UnaryQGate<_expr, _gate, _filter>& gate) const noexcept
  {
    return gates::UnaryQGate<_expr, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename _filter>
  inline constexpr auto operator()(
    gates::UnaryQGate<_expr, _gate, _filter>&& gate) const noexcept
  {
    return gates::UnaryQGate<_expr, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename _filter>
  inline constexpr auto operator()(
    const gates::BinaryQGate<_expr0, _expr1, _gate, _filter>& gate)
    const noexcept
  {
    return gates::
      BinaryQGate<_expr0, _expr1, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename _filter>
  inline constexpr auto operator()(
    gates::BinaryQGate<_expr0, _expr1, _gate, _filter>&& gate) const noexcept
  {
    return gates::
      BinaryQGate<_expr0, _expr1, _gate, decltype((*this)(_filter{}))>(gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename _filter>
  inline constexpr auto operator()(
    const gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>& gate)
    const noexcept
  {
    return gates::
      TernaryQGate<_expr0, _expr1, _expr2, _gate, decltype((*this)(_filter{}))>(
        gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename _filter>
  inline constexpr auto operator()(
    gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>&& gate)
    const noexcept
  {
    return gates::
      TernaryQGate<_expr0, _expr1, _expr2, _gate, decltype((*this)(_filter{}))>(
        gate);
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint filter
  ///
  /// @result expression representing the adjoint filter
  auto dagger() const noexcept
  {    
    using ::LibKet::dagger;
    return dagger(*this);
  }
};

/**
 @brief Serialize operation
*/
template<index_t _offset>
std::ostream&
operator<<(std::ostream& os, const QFilterShift<_offset>& filter)
{
  os << "shift<" << std::to_string(_offset) << ">(";
  if (!is_set(os))
    os << ")";
  else
    os << set;
  return os;
}

/**
@brief Tag filter class

The tag filter class assigns a unique tag to a position in a quantum
filter chain. It can be used together with the quantum goto-tag filter
to revert to a previous location in the quantum filter chain.

The design decision that is made here is the following: It is not easy
to ensure that throughout the entire project no two tags that might be
used together in a common quantum expression are the same. That would
require automatic generation of the tag value at compile time which
can be realized via a compile-time counter. However, this would
increase the complexity and hinder users to manually assign easy to
remember tags at user defined quantum expressions. It has therefore
been decided that the same tag value can be reused provided that a tag
filter is complemented by the corresponding gototab filter before the
same tag value is used again.

Consider the following example code snippet:

\code
tag<0>(
       sel<0>(
              gototag<0>(
                         h(
                           tag<0>(
                                  all(
                                      expr
                                      )
                                  )
                           )
                         )
              )
       )
\endcode

The innermost occurence of tag<0> captures the all filter and
the complementing gototag<0> goes back to it and selects the
0-th qubit thereof. The outermost occurence of tag<0> captures
the single qubit filter and any further use of gototag<0> would
go back to the latter.

What is not allowed is this:

\code
gototag<0>(
           all(
               tag<0>(
                      sel<0>(
                             tag<0>(
                                    all(
                                        expr
                                        )
                                    )
                             )
                        )
               )
           )
\endcode

Here, the innermost tag<0> is shadowed by the outermost tag<0>
and cannot be reached at all.

@defgroup tag Tag filter

@ingroup filters
*/
template<std::size_t _tag, typename _filter, typename _tagged_filter>
class QFilterTag : public QFilter
{
public:
  /// Quantum filter type
  using filter_t = typename std::decay<_filter>::type;

  /// Tagged quantum filter type
  using tagged_filter_t = typename std::decay<_tagged_filter>::type;

  /// Compile-time size
  template<std::size_t _qubits>
  inline static constexpr std::size_t size() noexcept
  {
    return filter_t::template size<_qubits>();
  }

  /// Compile-time integer sequence for range loops
  template<std::size_t _qubits, QBackendType _qbackend>
  inline static constexpr auto range(
    const QExpression<_qubits, _qbackend>& expr) noexcept
  {
    return filter_t::range(expr);
  }

  /// Constructor
  constexpr QFilterTag() = default;

  /// Operator()
  inline constexpr auto operator()() const noexcept
  {
    return *this;
  }
  
  /// Operator() - specialization for QFilter object
  inline constexpr auto operator()(const QFilter&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization for QFilter object
  inline constexpr auto operator()(QFilter&&) const noexcept { return *this; }

  /// Operator() - specialization for QFilterSelect object
  template<std::size_t... __ids>
  inline constexpr auto operator()(
    const QFilterSelect<__ids...>& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(filter)),
                      decltype(_tagged_filter{}(filter))>{};
  }

  /// Operator() - specialization for QFilterSelect object
  template<std::size_t... __ids>
  inline constexpr auto operator()(
    QFilterSelect<__ids...>&& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(filter)),
                      decltype(_tagged_filter{}(filter))>{};
  }

  /// Operator() - specialization for QFilterSelectAll object
  inline constexpr auto operator()(
    const QFilterSelectAll& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(filter)),
                      decltype(_tagged_filter{}(filter))>{};
  }

  /// Operator() - specialization for QFilterSelectAll object
  inline constexpr auto operator()(QFilterSelectAll&& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(filter)),
                      decltype(_tagged_filter{}(filter))>{};
  }

  /// Operator() - specialization for QFilterShift object
  template<index_t _offset>
  inline constexpr auto operator()(
    const QFilterShift<_offset>& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(filter)),
                      decltype(_tagged_filter{}(filter))>{};
  }

  /// Operator() - specialization for QFilterShift object
  template<index_t _offset>
  inline constexpr auto operator()(
    QFilterShift<_offset>&& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(filter)),
                      decltype(_tagged_filter{}(filter))>{};
  }

  /// Operator() - specialization for QFilterTag object
  template<std::size_t __tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    const QFilterTag<__tag, __filter, __tagged_filter>& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(filter)),
                      decltype(_tagged_filter{}(filter))>{};
  }

  /// Operator() - specialization for QFilterTag object
  template<std::size_t __tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    QFilterTag<__tag, __filter, __tagged_filter>&& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(filter)),
                      decltype(_tagged_filter{}(filter))>{};
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename __filter>
  inline constexpr auto operator()(
    const gates::UnaryQGate<_expr, _gate, __filter>& gate) const noexcept
  {
    return gates::UnaryQGate<_expr, _gate, decltype((*this)(__filter{}))>(gate);
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename __filter>
  inline constexpr auto operator()(
    gates::UnaryQGate<_expr, _gate, __filter>&& gate) const noexcept
  {
    return gates::UnaryQGate<_expr, _gate, decltype((*this)(__filter{}))>(gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename __filter>
  inline constexpr auto operator()(
    const gates::BinaryQGate<_expr0, _expr1, _gate, __filter>& gate)
    const noexcept
  {
    return gates::
      BinaryQGate<_expr0, _expr1, _gate, decltype((*this)(__filter{}))>(gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename __filter>
  inline constexpr auto operator()(
    gates::BinaryQGate<_expr0, _expr1, _gate, __filter>&& gate) const noexcept
  {
    return gates::
      BinaryQGate<_expr0, _expr1, _gate, decltype((*this)(__filter{}))>(gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename __filter>
  inline constexpr auto operator()(
    const gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, __filter>& gate)
    const noexcept
  {
    return gates::TernaryQGate<_expr0,
                               _expr1,
                               _expr2,
                               _gate,
                               decltype((*this)(__filter{}))>(gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename __filter>
  inline constexpr auto operator()(
    gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, __filter>&& gate)
    const noexcept
  {
    return gates::TernaryQGate<_expr0,
                               _expr1,
                               _expr2,
                               _gate,
                               decltype((*this)(__filter{}))>(gate);
  }

  /// Operator<< - specialization of concatenation operator for QFilter object
  inline constexpr auto operator<<(const QFilter&) const noexcept
  {
    return *this;
  }

  /// Operator() - specialization of concatenation operator for QFilter object
  inline constexpr auto operator<<(QFilter&&) const noexcept { return *this; }

  /// Operator<< - specialization of concatenation operator for QFilterSelect
  /// object
  template<std::size_t... __ids>
  inline constexpr auto operator<<(
    const QFilterSelect<__ids...>& filter) const noexcept
  {
    return QFilterTag<_tag, decltype(_filter{} << filter), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterSelect
  /// object
  template<std::size_t... __ids>
  inline constexpr auto operator<<(
    QFilterSelect<__ids...>&& filter) const noexcept
  {
    return QFilterTag<_tag, decltype(_filter{} << filter), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterSelectAll
  /// object
  inline constexpr auto operator<<(
    const QFilterSelectAll& filter) const noexcept
  {
    return QFilterTag<_tag, decltype(_filter{} << filter), _tagged_filter>{};
  }

  /// Operator() - specialization of concatenation operator for QFilterSelectAll
  /// object
  inline constexpr auto operator<<(QFilterSelectAll&& filter) const noexcept
  {
    return QFilterTag<_tag, decltype(_filter{} << filter), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterTag
  /// object
  template<std::size_t __tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator<<(
    const QFilterTag<__tag, __filter, __tagged_filter>& filter) const noexcept
  {
    return QFilterTag<_tag, decltype(_filter{} << filter), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for QFilterTag
  /// object
  template<std::size_t __tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator<<(
    QFilterTag<__tag, __filter, __tagged_filter>&& filter) const noexcept
  {
    return QFilterTag<_tag, decltype(_filter{} << filter), _tagged_filter>{};
  }

  /// Operator<< - specialization of concatenation operator for
  /// LIBKET_ERROR_TAG_NOT_FOUND object
  inline constexpr auto operator<<(
    const LIBKET_ERROR_TAG_NOT_FOUND&) const noexcept
  {
    return *this;
  }

  /// Operator<< - specialization of concatenation operator for
  /// LIBKET_ERROR_TAG_NOT_FOUND object
  inline constexpr auto operator<<(
    const LIBKET_ERROR_TAG_NOT_FOUND&&) const noexcept
  {
    return *this;
  }

  /// Operator>> - overwrite operator
  template<typename _expr>
  inline constexpr auto operator>>(const _expr&) const noexcept
  {
    return *this;
  }

  /// Operator>> - overwrite operator
  template<typename _expr>
  inline constexpr auto operator>>(_expr&&) const noexcept
  {
    return *this;
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint filter
  ///
  /// @result expression representing the adjoint filter
  auto dagger() const noexcept
  {    
    using ::LibKet::dagger;
    return dagger(*this);
  }
};

/**
 @brief Serialize operation
*/
template<std::size_t _tag, typename _filter, typename _tagged_filter>
std::ostream&
operator<<(std::ostream& os,
           const QFilterTag<_tag, _filter, _tagged_filter>& filter)
{
  os << "tag<" << std::to_string(_tag) << ">(";
  if (!is_set(os))
    os << ")";
  else
    os << set;
  return os;
}

/**
@brief Goto-tag filter class

The goto-tag filter class reverts all quantum filters that have been
applied between the current quantum filter and the quantum filter with
the specified tag.

The following code snipped illustrates the use of the quantum tag
and goto-tag filters

\code
auto filter = QFilterGotoTag<42>() (
                QFilterSelect<0>() (
                  QFilterSelectAll() (
                    QFilterTag<42>() (
                      QFilterSelect<0,1,2,3>()
                    )
                  )
                )
              )
\endcode

The resulting filter object equals the filter chain at tag 42, that is
`QFilterSelect<0,1,2,3>()`. The quantum tag and goto-tag filters are
particularly useful at the beginning and end of a quantum circiut.

For further details see \see QFilterTag.

@defgroup gototag GotoTag filter

@ingroup filters
*/
template<std::size_t _tag, typename _filter>
class QFilterGotoTag : public QFilter
{
public:
  /// Quantum filter type
  using filter_t = typename std::decay<_filter>::type;

  /// Compile-time size
  template<std::size_t _qubits>
  inline static constexpr std::size_t size() noexcept
  {
    return filter_t::template size<_qubits>();
  }

  /// Compile-time integer sequence for range loops
  template<std::size_t _qubits, QBackendType _qbackend>
  inline static constexpr auto range(
    const QExpression<_qubits, _qbackend>& expr) noexcept
  {
    return filter_t::range(expr);
  }

  /// Constructor
  constexpr QFilterGotoTag() = default;

  /// Operator()
  inline constexpr auto operator()() const noexcept
  {
    return *this;
  }
  
  /// Operator() - specialization for QFilterTag object with identical tag
  template<typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    const QFilterTag<_tag, __filter, __tagged_filter>& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(__tagged_filter{})),
                      __tagged_filter>{};
  }

  /// Operator() - specialization for QFilterTag object with identical tag
  template<typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    QFilterTag<_tag, __filter, __tagged_filter>&& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(__tagged_filter{})),
                      __tagged_filter>{};
  }

  /// Operator() - specialization for QFilterTag object with different tag
  template<std::size_t __tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    const QFilterTag<__tag, __filter, __tagged_filter>& filter) const noexcept
  {
    return QFilterTag<__tag, decltype((*this)(__filter{})), __tagged_filter>{};
  }

  /// Operator() - specialization for QFilterTag object with different tag
  template<std::size_t __tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    QFilterTag<__tag, __filter, __tagged_filter>&& filter) const noexcept
  {
    return QFilterTag<__tag, decltype((*this)(__filter{})), __tagged_filter>{};
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename __filter>
  inline constexpr auto operator()(
    const gates::UnaryQGate<_expr, _gate, __filter>& gate) const noexcept
  {
    return gates::
      UnaryQGate<_expr, _gate, decltype(search(gate) >> (*this)(__filter{}))>(
        gate);
  }

  /// Operator() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename __filter>
  inline constexpr auto operator()(
    gates::UnaryQGate<_expr, _gate, __filter>&& gate) const noexcept
  {
    return gates::
      UnaryQGate<_expr, _gate, decltype(search(gate) >> (*this)(__filter{}))>(
        gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename __filter>
  inline constexpr auto operator()(
    const gates::BinaryQGate<_expr0, _expr1, _gate, __filter>& gate)
    const noexcept
  {
    return gates::BinaryQGate<_expr0,
                              _expr1,
                              _gate,
                              decltype(search(gate) >> (*this)(__filter{}))>(
      gate);
  }

  /// Operator() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename __filter>
  inline constexpr auto operator()(
    gates::BinaryQGate<_expr0, _expr1, _gate, __filter>&& gate) const noexcept
  {
    return gates::BinaryQGate<_expr0,
                              _expr1,
                              _gate,
                              decltype(search(gate) >> (*this)(__filter{}))>(
      gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename __filter>
  inline constexpr auto operator()(
    const gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, __filter>& gate)
    const noexcept
  {
    return gates::TernaryQGate<_expr0,
                               _expr1,
                               _expr2,
                               _gate,
                               decltype(search(gate) >> (*this)(__filter{}))>(
      gate);
  }

  /// Operator() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename __filter>
  inline constexpr auto operator()(
    gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, __filter>&& gate)
    const noexcept
  {
    return gates::TernaryQGate<_expr0,
                               _expr1,
                               _expr2,
                               _gate,
                               decltype(search(gate) >> (*this)(__filter{}))>(
      gate);
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint filter
  ///
  /// @result expression representing the adjoint filter
  auto dagger() const noexcept
  {    
    using ::LibKet::dagger;
    return dagger(*this);
  }

private:
  /// search() - specialization for QFilterTag object with identical tag
  template<typename __filter, typename __tagged_filter>
  inline constexpr auto search(
    const QFilterTag<_tag, __filter, __tagged_filter>& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(__tagged_filter{})),
                      __tagged_filter>{};
  }

  /// search() - specialization for QFilterTag object with identical tag
  template<typename __filter, typename __tagged_filter>
  inline constexpr auto search(
    QFilterTag<_tag, __filter, __tagged_filter>&& filter) const noexcept
  {
    return QFilterTag<_tag,
                      decltype(_filter{}(__tagged_filter{})),
                      __tagged_filter>{};
  }

  /// search() - specialization for QFilterTag object with different tag
  template<std::size_t __tag, typename __filter, typename __tagged_filter>
  inline constexpr auto search(
    const QFilterTag<__tag, __filter, __tagged_filter>& filter) const noexcept
  {
    return QFilterTag<__tag, decltype(search(__filter{})), __tagged_filter>{};
  }

  /// search() - specialization for QFilterTag object with different tag
  template<std::size_t __tag, typename __filter, typename __tagged_filter>
  inline constexpr auto search(
    QFilterTag<__tag, __filter, __tagged_filter>&& filter) const noexcept
  {
    return QFilterTag<__tag, decltype(search(__filter{})), __tagged_filter>{};
  }

  /// search() = specialized for all other objects
  template<typename _expr>
  inline constexpr auto operator()(const _expr&) const noexcept
  {
    return LIBKET_ERROR_TAG_NOT_FOUND{};
  }

  /// search() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename __filter>
  inline constexpr auto search(
    const gates::UnaryQGate<_expr, _gate, __filter>& gate) const noexcept
  {
    return search(typename std::decay<_expr>::type{});
  }

  /// search() - specialization for UnaryQGate object
  template<typename _expr, typename _gate, typename __filter>
  inline constexpr auto search(
    gates::UnaryQGate<_expr, _gate, __filter>&& gate) const noexcept
  {
    return search(typename std::decay<_expr>::type{});
  }

  /// search() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename __filter>
  inline constexpr auto search(
    const gates::BinaryQGate<_expr0, _expr1, _gate, __filter>& gate)
    const noexcept
  {
    return search(typename std::decay<_expr0>::type{})
           << search(typename std::decay<_expr1>::type{});
  }

  /// search() - specialization for BinaryQGate object
  template<typename _expr0, typename _expr1, typename _gate, typename __filter>
  inline constexpr auto search(
    gates::BinaryQGate<_expr0, _expr1, _gate, __filter>&& gate) const noexcept
  {
    return search(typename std::decay<_expr0>::type{})
           << search(typename std::decay<_expr1>::type{});
  }

  /// search() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename __filter>
  inline constexpr auto search(
    const gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, __filter>& gate)
    const noexcept
  {
    return search(typename std::decay<_expr0>::type{})
           << search(typename std::decay<_expr1>::type{})
           << search(typename std::decay<_expr2>::type{});
  }

  /// search() - specialization for TernaryQGate object
  template<typename _expr0,
           typename _expr1,
           typename _expr2,
           typename _gate,
           typename __filter>
  inline constexpr auto search(
    gates::TernaryQGate<_expr0, _expr1, _expr2, _gate, __filter>&& gate)
    const noexcept
  {
    return search(typename std::decay<_expr0>::type{})
           << search(typename std::decay<_expr1>::type{})
           << search(typename std::decay<_expr2>::type{});
  }

  /// search() - specialization for all other objects
  template<typename _expr>
  inline constexpr auto search(const _expr&) const noexcept
  {
    return LIBKET_ERROR_TAG_NOT_FOUND{};
  }
};

/**
 @brief Serialize operation
*/
template<std::size_t _tag, typename _filter>
std::ostream&
operator<<(std::ostream& os, const QFilterGotoTag<_tag, _filter>& filter)
{
  os << "gototag<" << std::to_string(_tag) << ">(";
  if (!is_set(os))
    os << ")";
  else
    os << set;
  return os;
}

/**
@brief Filter evaluator

This overload of the LibKet::filters::filter() function can be used as
terminal, i.e. the inner-most filter in a quantum filter chain

\code
auto filter = filters::filter();
\endcode

@ingroup all
*/
inline static constexpr auto
filter() noexcept
{
  return QFilter{};
}

/**
   @brief Filter evaluator

   @ingroup all
*/
template<typename Expr>
inline static constexpr auto
filter(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilter{}(expr))>::type
{
  return QFilter{}(expr);
}

/**
@brief Select-all filter evaluator

This overload of the LibKet::filters::all() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::all();
\endcode

@ingroup all
*/
inline static constexpr auto
all() noexcept
{
  return QFilterSelectAll{};
}

/**
   @brief Select-all filter evaluator

   @ingroup all
*/
template<typename Expr>
inline static constexpr auto
all(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilterSelectAll{}(expr))>::type
{
  return QFilterSelectAll{}(expr);
}

/**
@brief Select-range filter evaluator

This overload of the LibKet::filters::range() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::range<1,3>();
\endcode

@ingroup range
*/
template<std::size_t _begin, std::size_t _end>
inline static constexpr auto
range() noexcept
{
  return QFilterSelectRange<_begin, _end>{};
}

/**
   @brief Select-range filter evaluator

   @ingroup range
*/
template<std::size_t _begin, std::size_t _end, typename Expr>
inline static constexpr auto
range(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilterSelectRange<_begin, _end>{}(expr))>::type
{
  return QFilterSelectRange<_begin, _end>{}(expr);
}

/**
@brief Select-range filter evaluator (with prior reset)

This overload of the LibKet::filters::range_() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::range_<1,3>();
\endcode

@ingroup range
*/
template<std::size_t _begin, std::size_t _end>
inline static constexpr auto
range_() noexcept
{
  return QFilterSelectRange<_begin, _end>{}(QFilterSelectAll{});
}

/**
   @brief Select-range filter evaluator (with prior reset)

   @ingroup range
*/
template<std::size_t _begin, std::size_t _end, typename Expr>
inline static constexpr auto
range_(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilterSelectRange<_begin, _end>{}(QFilterSelectAll{}(expr)))>::type
{
  return QFilterSelectRange<_begin, _end>{}(QFilterSelectAll{}(expr));
}

/**
@brief Select filter evaluator

This overload of the LibKet::filters::sel() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::sel<0,2,4>();
\endcode

@ingroup select
*/
template<std::size_t... _ids>
inline static constexpr auto
sel() noexcept
{
  return QFilterSelect<_ids...>{};
}

/**
   @brief Select filter evaluator

   @ingroup select
*/
template<std::size_t... _ids, typename Expr>
inline static constexpr auto
sel(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilterSelect<_ids...>{}(expr))>::type
{
  return QFilterSelect<_ids...>{}(expr);
}

/**
@brief Select filter evaluator (with prior reset)

This overload of the LibKet::filters::sel_() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::sel_<0,2,4>();
\endcode

@ingroup select
*/
template<std::size_t... _ids>
inline static constexpr auto
sel_() noexcept
{
  return QFilterSelect<_ids...>{}(QFilterSelectAll{});
}

/**
   @brief Select filter evaluator

   @ingroup select
*/
template<std::size_t... _ids, typename Expr>
inline static constexpr auto
sel_(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilterSelect<_ids...>{}(QFilterSelectAll{}(expr)))>::type
{
  return QFilterSelect<_ids...>{}(QFilterSelectAll{}(expr));
}

/**
@brief Shift filter evaluator

This overload of the LibKet::filters::shift() function can be
used as terminal, i.e. the inner-most filter in a quantum
filter chain

\code
auto filter = filters::shift<1>();
\endcode

@ingroup shift
*/
template<index_t _offset>
inline static constexpr auto
shift() noexcept
{
  return QFilterShift<_offset>{};
}

/**
   @brief Shift filter evaluator

   @ingroup shift
*/
template<index_t _offset, typename Expr>
inline static constexpr auto
shift(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilterShift<_offset>{}(expr))>::type
{
  return QFilterShift<_offset>{}(expr);
}

/**
@brief Quantum register filter evaluator

This overload of the LibKet::filters::qureg() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::qureg<0,6>();
\endcode

@ingroup register
*/
template<std::size_t _begin, std::size_t _qubits>
inline static constexpr auto
qureg() noexcept
{
  return QRegister<_begin, _qubits>{};
}

/**
   @brief Quantum register filter evaluator

   @ingroup register
*/
template<std::size_t _begin, std::size_t _qubits, typename Expr>
inline static constexpr auto
qureg(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QRegister<_begin, _qubits>{}(expr))>::type
{
  return QRegister<_begin, _qubits>{}(expr);
}

/**
@brief Quantum register filter evaluator (with prior reset)

This overload of the LibKet::filters::qureg_() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::qureq_<0,6>();
\endcode

@ingroup register
*/
template<std::size_t _begin, std::size_t _qubits>
inline static constexpr auto
qureg_() noexcept
{
  return QRegister<_begin, _qubits>{}(QFilterSelectAll{});
}

/**
   @brief Quantum register filter evaluator

   @ingroup register
*/
template<std::size_t _begin, std::size_t _qubits, typename Expr>
inline static constexpr auto
qureg_(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QRegister<_begin, _qubits>{}(QFilterSelectAll{}(expr)))>::type
{
  return QRegister<_begin, _qubits>{}(QFilterSelectAll{}(expr));
}

/**
@brief Quantum bit filter evaluator

This overload of the LibKet::filters::qubit() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::qubit<0>();
\endcode

@ingroup qubit
*/
template<std::size_t _id>
inline static constexpr auto
qubit() noexcept
{
  return QBit<_id>{};
}

/**
   @brief Quantum bit filter evaluator

   @ingroup qubit
*/
template<std::size_t _id, typename Expr>
inline static constexpr auto
qubit(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QBit<_id>{}(expr))>::type
{
  return QBit<_id>{}(expr);
}

/**
@brief Quantum bit filter evaluator (with prior reset)

This overload of the LibKet::filters::qubit_() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::qubit_<0>();
\endcode

@ingroup qubit
*/
template<std::size_t _id>
inline static constexpr auto
qubit_() noexcept
{
  return QBit<_id>{}(QFilterSelectAll{});
}

/**
   @brief Quantum bit filter evaluator

   @ingroup qubit
*/
template<std::size_t _id, typename Expr>
inline static constexpr auto
qubit_(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QBit<_id>{}(QFilterSelectAll{}(expr)))>::type
{
  return QBit<_id>{}(QFilterSelectAll{}(expr));
}

/**
@brief Tag filter evaluator

This overload of the LibKet::filters::tag() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::tag<0>();
\endcode

@ingroup tag
*/
template<std::size_t Tag>
inline static constexpr auto
tag() noexcept
{
  return QFilterTag<Tag, QFilter, QFilter>{};
}

/**
   @brief Tag filter evaluator

   @ingroup tag
*/
template<std::size_t Tag, typename Expr>
inline static constexpr auto
tag(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilterTag<Tag, QFilter, QFilter>{}(expr))>::type
{
  return QFilterTag<Tag, QFilter, QFilter>{}(expr);
}

/**
@brief Goto-tag filter evaluator

This overload of the LibKet::filters::gototag() function can be used
as terminal, i.e. the inner-most filter in a quantum filter
chain

\code
auto filter = filters::gototag<0>();
\endcode

@ingroup gototag
*/
template<std::size_t Tag>
inline static constexpr auto
gototag() noexcept
{
  return QFilterGotoTag<Tag, QFilter>{};
}

/**
   @brief Goto-tag filter evaluator

   @ingroup gototag
*/
template<std::size_t Tag, typename Expr>
inline static constexpr auto
gototag(Expr expr) noexcept -> typename std::enable_if<
  std::is_base_of<QBase, typename std::decay<Expr>::type>::value,
  decltype(QFilterGotoTag<Tag, QFilter>{}(expr))>::type
{
  return QFilterGotoTag<Tag, QFilter>{}(expr);
}

/**

   @brief Prints the abstract syntex tree of the filter
   
   @note specialization for QFilter objects

   @ingroup filters
*/
template<std::size_t level = 1>
inline static auto
show(const QFilter& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilter\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter
   
   @note specialization for QFilterSelectAll objects

   @ingroup all
*/
template<std::size_t level = 1>
inline static auto
show(const QFilterSelectAll& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterSelectAll\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter
   
   @note specialization for QFilterSelect objects

   @ingroup select
*/
template<std::size_t level = 1, std::size_t... _ids>
inline static auto
show(const QFilterSelect<_ids...>& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterSelect [";
  using expander = int[];
  (void)expander{ 0,
                  (void(os << ' ' << std::forward<std::size_t>(_ids)), 0)... };
  os << " ]\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter
   
   @note specialization for QFilterShift objects

   @ingroup shift
*/
template<std::size_t level = 1, index_t _offset>
inline static auto
show(const QFilterShift<_offset>& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterShift [" + utils::to_string(_offset) + "]\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter
   
   @note specialization for QFilterTag objects

   @ingroup tag
*/
template<std::size_t level = 1,
         std::size_t _tag,
         typename _filter,
         typename _tagged_filter>
inline static auto
show(const QFilterTag<_tag, _filter, _tagged_filter>& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterTag <" + utils::to_string(_tag) + ">\n";
  os << prefix << "| filter = ";
  show<level - 1>(_filter{}, os, prefix + "|          ");
  os << prefix << "| tagged = ";
  show<level - 1>(_tagged_filter{}, os, prefix + "|          ");

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter
   
   @note specialization for QFilterGotoTag objects

   @ingroup gototag
*/
template<std::size_t level = 1, std::size_t _tag, typename _filter>
inline static auto
show(const QFilterGotoTag<_tag, _filter>& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterGotoTag <" + utils::to_string(_tag) + ">\n";
  os << prefix << "| filter = ";
  show<level - 1>(_filter{}, os, prefix + "|          ");

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter
   
   @note specialization for LIBKET_ERROR_TAG_NOT_FOUND objects

   @ingroup filters
*/
template<std::size_t level = 1>
inline static auto
show(const LIBKET_ERROR_TAG_NOT_FOUND& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "*** LibKet error *** : Tag not found!\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter

   @note specialization for
   LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION objects

   @ingroup filters
*/
template<std::size_t level = 1>
inline static auto
show(const LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "*** LibKet error *** : QFilter does not provide range function!\n";

  return filter;
}


/**
   @brief Prints the abstract syntex tree of the filter (DOT-language)
   
   @note specialization for QFilter objects

   @ingroup filters
*/
template<std::size_t level = 1>
inline static auto
dot(const QFilter& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilter\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter (DOT-language)
   
   @note specialization for QFilterSelectAll objects

   @ingroup all
*/
template<std::size_t level = 1>
inline static auto
dot(const QFilterSelectAll& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterSelectAll\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter (DOT-language)
   
   @note specialization for QFilterSelect objects

   @ingroup select
*/
template<std::size_t level = 1, std::size_t... _ids>
inline static auto
dot(const QFilterSelect<_ids...>& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterSelect [";
  using expander = int[];
  (void)expander{ 0,
                  (void(os << ' ' << std::forward<std::size_t>(_ids)), 0)... };
  os << " ]\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter (DOT-language)
   
   @note specialization for QFilterShift objects

   @ingroup shift
*/
template<std::size_t level = 1, index_t _offset>
inline static auto
dot(const QFilterShift<_offset>& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterShift [" + utils::to_string(_offset) + "]\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter (DOT-language)
   
   @note specialization for QFilterTag objects

   @ingroup tag
*/
template<std::size_t level = 1,
         std::size_t _tag,
         typename _filter,
         typename _tagged_filter>
inline static auto
dot(const QFilterTag<_tag, _filter, _tagged_filter>& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterTag <" + utils::to_string(_tag) + ">\n";
  
  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter (DOT-language)
   
   @note specialization for QFilterGotoTag objects

   @ingroup gototag
*/
template<std::size_t level = 1, std::size_t _tag, typename _filter>
inline static auto
dot(const QFilterGotoTag<_tag, _filter>& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "QFilterGotoTag <" + utils::to_string(_tag) + ">\n";  

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter (DOT-language)
   
   @note specialization for LIBKET_ERROR_TAG_NOT_FOUND objects

   @ingroup filters
*/
template<std::size_t level = 1>
inline static auto
dot(const LIBKET_ERROR_TAG_NOT_FOUND& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "*** LibKet error *** : Tag not found!\n";

  return filter;
}

/**
   @brief Prints the abstract syntex tree of the filter (DOT-language)

   @note specialization for
   LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION objects

   @ingroup filters
*/
template<std::size_t level = 1>
inline static auto
dot(const LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION& filter,
     std::ostream& os = std::cout,
     const std::string& prefix = "") noexcept
{
  os << "*** LibKet error *** : QFilter does not provide range function!\n";

  return filter;
}

/** @brief Creates adjoint filter (passthrough operation)
 *  @{
 */ 
inline static auto dagger(const QFilter& filter) { return filter; }

inline static auto dagger(const QFilterSelectAll& filter) { return filter; }

template<std::size_t... _ids>
inline static auto dagger(const QFilterSelect<_ids...>& filter) { return filter; }

template<index_t _offset>
inline static auto dagger(const QFilterShift<_offset>& filter) { return filter; }

template<std::size_t _tag,
         typename _filter,
         typename _tagged_filter>
inline static auto dagger(const QFilterTag<_tag, _filter, _tagged_filter>& filter) { return filter; }

template<std::size_t _tag,
         typename _filter>
inline static auto dagger(const QFilterGotoTag<_tag, _filter>& filter) { return filter; }

inline static auto dagger(const LIBKET_ERROR_TAG_NOT_FOUND& filter) { return filter; }

inline static auto dagger(const LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION& filter) { return filter; }
/** @}
 *  @brief Returns the list of variables in the filter (no-operation)
 *  @{
 */ 
inline static auto vars(const QFilter& filter) { return nullptr; }

inline static auto vars(const QFilterSelectAll& filter) { return nullptr; }

template<std::size_t... _ids>
inline static auto vars(const QFilterSelect<_ids...>& filter) { return nullptr; }

template<index_t _offset>
inline static auto vars(const QFilterShift<_offset>& filter) { return nullptr; }

template<std::size_t _tag,
         typename _filter,
         typename _tagged_filter>
inline static auto vars(const QFilterTag<_tag, _filter, _tagged_filter>& filter) { return nullptr; }

template<std::size_t _tag,
         typename _filter>
inline static auto vars(const QFilterGotoTag<_tag, _filter>& filter) { return nullptr; }

inline static auto vars(const LIBKET_ERROR_TAG_NOT_FOUND& filter) { return nullptr; }

inline static auto vars(const LIBKET_ERROR_QFILTER_DOES_NOT_PROVIDE_RANGE_FUNCTION& filter) { return nullptr; }
/** @}
 */
} // namespace filters

} // namespace LibKet

#endif // QFILTER_HPP
