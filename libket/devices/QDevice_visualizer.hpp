/** @file libket/devices/QDevice_visualizer.hpp

    @brief C++ API Circuit visualizer class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_VISUALIZER_HPP
#define QDEVICE_VISUALIZER_HPP

#include <string>

#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_OPENQASM
/**
   @brief Qiskit visualizer class

   This class visualizes quantum circuits using the Qiskit
   backend. It adopts the OpenQASMv2 quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_Qiskit_visualizer
  : public QExpression<_qubits, QBackendType::OpenQASMv2>
{
private:
  /// Base type
  using Base = QExpression<_qubits, QBackendType::OpenQASMv2>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_Qiskit_visualizer() = default;

  /// Constructor from JSON object
  QDevice_Qiskit_visualizer(const utils::json& config)
    : QDevice_Qiskit_visualizer()
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_Qiskit_visualizer& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_Qiskit_visualizer& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Write quantum circuit to file
  std::string to_file(const std::string& filename,
                      const std::string& script_init = "",
                      const std::string& script_before = "",
                      const std::string& script_after = "",
                      QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tfrom qiskit import QuantumRegister, ClassicalRegister, "
          "QuantumCircuit\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    // Prepare quantum circuit
    ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    // Draw circuit to latex source
    ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn result\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }
};

#else

/**
   @brief Qiskit visualizer class

   This class visualizes quantum circuits using the Qiskit
   backend. It adopts the OpenQASMv2 quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_Qiskit_visualizer : public QDevice_Dummy
{
  using QDevice_Dummy::QDevice_Dummy;
};

#endif

#ifdef LIBKET_WITH_QASM
/**
   @brief QASM2TEX visualizer class

   This class visualizes quantum circuits using the QASM2TEX
   backend. It adopts the QASM quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_QASM2TEX_visualizer : public QExpression<_qubits, QBackendType::QASM>
{
private:
  /// Base type
  using Base = QExpression<_qubits, QBackendType::QASM>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_QASM2TEX_visualizer() = default;

  /// Constructor from JSON object
  QDevice_QASM2TEX_visualizer(const utils::json& config)
    : QDevice_QASM2TEX_visualizer()
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_QASM2TEX_visualizer& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_QASM2TEX_visualizer& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Write quantum circuit to file
  std::string to_file(const std::string& filename,
                      const std::string& script_init = "",
                      const std::string& script_before = "",
                      const std::string& script_after = "",
                      QStream<QJobType::Python>* stream = NULL)
  {
    std::ofstream file;
    file.open(filename + ".qasm");
    file << *this;
    file.close();

    std::stringstream ss;
    utils::json result;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    ss << "\timport json\n"
       << "\timport subprocess\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    ss << "\tresult=subprocess.run(['python', "
          "'"
       << QASM2TEX_PY << "', '" << filename
       << ".qasm'], stdout=subprocess.PIPE)\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    ss << "\treturn json.dumps(result.stdout.decode('UTF-8'))\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        result = stream->run(ss.str(), "run", "", "")->get();
      else
        result = _qstream_python.run(ss.str(), "run", "", "")->get();

      file.open(filename + ".tex");
      file << result.get<std::string>();
      file.close();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
    return result.get<std::string>();
  }
};

#else

/**
   @brief QASM2TEX visualizer class

   This class visualizes quantum circuits using the QASM2TEX
   backend. It adopts the QASM quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_QASM2TEX_visualizer : public QDevice_Dummy
{};

#endif

namespace device {

QDevicePropertyDefine(QDeviceType::qiskit_visualizer,
                      "qiskit",
                      1024,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qasm2tex_visualizer,
                      "qasm2tex",
                      1024,
                      true,
                      QEndianness::lsb);

} // namespace device

template<std::size_t _qubits>
class QDevice<
  QDeviceType::qiskit_visualizer,
  _qubits,
  device::QDeviceProperty<QDeviceType::qiskit_visualizer>::simulator,
  device::QDeviceProperty<QDeviceType::qiskit_visualizer>::endianness>
  : public QDevice_Qiskit_visualizer<_qubits>
{
public:
  QDevice()
    : QDevice_Qiskit_visualizer<_qubits>()
  {
    static_assert(
      _qubits <=
        device::QDeviceProperty<QDeviceType::qiskit_visualizer>::qubits,
      "#qubits exceeds device capacity");
  }

  QDevice(const utils::json& config)
    : QDevice_Qiskit_visualizer<_qubits>(config)
  {
    static_assert(
      _qubits <=
        device::QDeviceProperty<QDeviceType::qiskit_visualizer>::qubits,
      "#qubits exceeds device capacity");
  }
};

template<std::size_t _qubits>
class QDevice<
  QDeviceType::qasm2tex_visualizer,
  _qubits,
  device::QDeviceProperty<QDeviceType::qasm2tex_visualizer>::simulator,
  device::QDeviceProperty<QDeviceType::qasm2tex_visualizer>::endianness>
  : public QDevice_QASM2TEX_visualizer<_qubits>
{
public:
  QDevice()
    : QDevice_QASM2TEX_visualizer<_qubits>()
  {
    static_assert(
      _qubits <=
        device::QDeviceProperty<QDeviceType::qasm2tex_visualizer>::qubits,
      "#qubits exceeds device capacity");
  }

  QDevice(const utils::json& config)
    : QDevice_QASM2TEX_visualizer<_qubits>(config)
  {
    static_assert(
      _qubits <=
        device::QDeviceProperty<QDeviceType::qasm2tex_visualizer>::qubits,
      "#qubits exceeds device capacity");
  }
};

} // namespace LibKet

#endif // QDEVICE_VISUALIZER_HPP
