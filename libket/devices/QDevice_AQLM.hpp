/** @file libket/devices/QDevice_AQLM.hpp

    @brief C++ API Atos QLM device class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_AQLM_HPP
#define QDEVICE_AQLM_HPP

#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_AQASM
/**
   @brief Atos Quantum Learning Machine (QLM) device class

   This class executes quantum circuits on the Atos Quantum Learning
   Machine (QLM) simulator. It adopts Atos' AQASM quantum assembly
   language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_AQLM_simulator : public QExpression<_qubits, QBackendType::AQASM>
{
private:
  /// Name of the quantum backend
  const std::string backend;

  /// Topology of the quantum backend
  const std::string topology;

  /// Number of shots to run the quantum kernel
  const std::size_t shots;

  /// Base type
  using Base = QExpression<_qubits, QBackendType::AQASM>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_AQLM_simulator(
    const std::string& backend = LibKet::getenv("AQLM_BACKEND", "feynman"),
    const std::string& topology = LibKet::getenv("AQLM_TOPOLOGY"),
    const std::size_t& shots = std::atoi(LibKet::getenv("AQLM_SHOTS", "1024")))
    : backend(backend)
    , topology(topology)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_AQLM_simulator(const utils::json& config)
    : QDevice_AQLM_simulator(
        config.find("backend") != config.end()
          ? config["backend"]
          : LibKet::getenv("AQLM_BACKEND", "feynman"),
        config.find("topology") != config.end()
          ? config["topology"]
          : LibKet::getenv("AQLM_TOPOLOGY"),
        config.find("shots") != config.end()
          ? config["shots"].get<size_t>()
          : std::atoi(LibKet::getenv("AQLM_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_AQLM_simulator& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_AQLM_simulator& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit remotely on Atos QLM asynchronously
  /// and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Import modules
    ss << "\timport sys\n"
       << "\tif not hasattr(sys, 'argv'):\n"
       << "\t\tsys.argv  = ['']\n"
       << "\timport json\n"
       << "\timport qat.core.qpu\n"
       << "\timport qat.core.qpu.agent\n"
       << "\timport qat.core.qpu.stateanalyzer\n"
       << "\timport qat.core.task\n"
       << "\timport qat.lang.parser.aqasm_parser\n";

    if (backend == "feynman") {
      ss << "\timport qat.feynman\n";
    } else if (backend == "linalg") {
      ss << "\timport qat.linalg\n";
    } else if (backend == "stabs") {
      ss << "\timport qat.stabs\n";
    } else if (backend == "mps") {
      ss << "\timport qat.mps\n";
    } else {
      ss << "\traise Exception('Invalid backend selected')\n";
    }

    // Generate AQASM code
    ss << "\taqasm = '''\n"
       << Base::to_string()
       << "'''\n"

       // Parse AQASM code and generate circuit
       << "\tparser = qat.lang.parser.aqasm_parser.AqasmParser()\n"
       << "\tparser.build()\n"
       << "\tif parser.parse(aqasm) == 1:\n"
       << "\t\tcircuit = parser.compiler.gen_circuit()\n"
       << "\telse:\n"
       << "\t\treturn '\"An error occured while parsing the AQASM program\"'\n";

    // Create task
    // ss << "\ttask = qat.core.task.Task(circuit)\n";

    // Create simulator backend (and optimize circuit of needed
    if (backend == "feynman")
      ss << "\tsimulator = qat.feynman.Feynman()\n";
    else if (backend == "linalg")
      ss << "\tsimulator = qat.linalg.LinAlg()\n";
    else if (backend == "stabs")
      ss << "\tsimulator = qat.stabs.Stabs()\n";
    else if (backend == "mps")
      ss << "\tsimulator = qat.mps.MPS(lnnize=False, no_merge=False, "
            "n_trunc=None, threshold=None)\n"
         << "\toptimizer = qat.nnize.Nnizer(topology='" << topology << "')\n"
         << "\tcircuit   = optimizer.Optimize(task).circuit\n";
    //<< "\ttask      = qat.core.task.Task(circuit)\n";

    // Specify plugins
    ss << "\thandlers  = {'agent': qat.core.qpu.agent.GenericAgent, "
          "'state_analyzer': qat.core.qpu.stateanalyzer.StateAnalyzer}\n"

       // Run circuit on selected QPU
       << "\tqpu       = qat.core.qpu.Server(simulator, handlers=handlers)\n"
       << "\ttask      = qat.core.task.Task(circuit)\n"
       << "\ttask.attach(qpu)\n"
       << "\trangeset  = range(circuit.nbqbits)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    ss << "\tresult    = task.execute(list(rangeset))\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Serialize backend
    ss << "\tdata                   = {}\n"
       << "\tdata['backend']        = '" << backend
       << "'\n"

       // Serialize amplitude
       << "\tif isinstance(result.amplitude, complex):\n"
       << "\t\tdata['amplitude'] = result.amplitude.real\n"
       << "\telse:\n"
       << "\t\tdata['amplitude'] = result.amplitude\n"

       // Serialize cbits
       << "\tif result.cbits is None:\n"
       << "\t\tdata['cbits']     = result.cbits\n"
       << "\telse:\n"
       << "\t\tdata_cbits        = {}\n"
       << "\t\tdata_cbits['bin'] = result.cbits.bin\n"
       << "\t\tfor i in range(result.cbits.len):\n"
       << "\t\t\tdata_cbits[str(i)] = int(result.cbits[i])\n"
       << "\t\tdata['cbits']     = data_cbits\n"

       // Serialize error and probability
       << "\tdata['err']            = result.err\n"
       << "\tdata['probability']    = result.probability\n"

       // Serialize history
       << "\tif result.history is None:\n"
       << "\t\tdata['history']   = result.history\n"
       << "\telse:\n"
       << "\t\tdata['history']   = None\n"

       // Serialize UsageReport
       << "\tdata_report            = {}\n"
       << "\tdata_report['elapsed'] = result.report.elapsed\n"
       << "\tdata_report['max_rss'] = result.report.max_rss\n"
       << "\tdata_report['system']  = result.report.system\n"
       << "\tdata_report['user']    = result.report.user\n"
       << "\tdata['report']         = data_report\n"

       // Serialize state
       << "\tdata_state = {}\n"
       << "\tdata_state['bin'] = result.state.bin\n"
       << "\tfor i in range(result.state.len):\n"
       << "\t\tdata_state[str(i)] = int(result.state[i])\n"
       << "\tdata['state'] = data_state\n"

       << "\treturn json.dumps(data)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit remotely on Atos QLM synchronously and
  /// return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit remotely on Atos QLM synchronously and
  /// return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));

    return 0;
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(0);
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;
    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return std::string("0");
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return true;
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));
    return std::time(nullptr);
  }
};

#else

/**
   @brief Atos Quantum Learning Machine (QLM) device class

   This class executes quantum circuits on the Atos Quantum Learning
   Machine (QLM) simulator. It adopts Atos' AQASM quantum assembly
   language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_AQLM_simulator : public QDevice_Dummy
{
  using QDevice_Dummy::QDevice_Dummy;  
};

#endif

#define QDeviceDefineAQLM(_name, _qubits)                               \
  namespace device {                                                    \
    QDevicePropertyDefine(QDeviceType::atos_qlm_##_name##_simulator,    \
                          #_name,                                       \
                          _qubits,                                      \
                          true,                                         \
                          QEndianness::lsb);                            \
  }                                                                     \
                                                                        \
  template<std::size_t __qubits>                                        \
  class QDevice<QDeviceType::atos_qlm_##_name##_simulator,              \
                __qubits,                                                       \
                device::QDeviceProperty<QDeviceType::atos_qlm_##_name##_simulator>::simulator, \
                device::QDeviceProperty<QDeviceType::atos_qlm_##_name##_simulator>::endianness> \
    : public QDevice_AQLM_simulator<__qubits>                                   \
  {                                                                            \
  public:                                                                      \
    QDevice(const std::string& topology = LibKet::getenv("AQLM_TOPOLOGY"),     \
            const std::size_t& shots = std::atoi(LibKet::getenv("AQLM_SHOTS",  \
                                                                "1024")))      \
      : QDevice_AQLM_simulator<__qubits>(device::QDeviceProperty<QDeviceType::atos_qlm_##_name##_simulator>::name, \
                                        topology,                              \
                                        shots)                                 \
    {                                                                          \
      static_assert(__qubits <= device::QDeviceProperty<QDeviceType::atos_qlm_##_name##_simulator>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("topology") != config.end()                        \
                  ? config["topology"]                                         \
                  : LibKet::getenv("AQLM_TOPOLOGYE"),                          \
                config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("AQLM_SHOTS", "1024")))           \
      {                                                                 \
      static_assert(__qubits <= device::QDeviceProperty<QDeviceType::atos_qlm_##_name##_simulator>::qubits, \
                    "#qubits exceeds device capacity");                 \
    }                                                                   \
  };

QDeviceDefineAQLM(feynman, 1024);
QDeviceDefineAQLM(linalg, 1024);
QDeviceDefineAQLM(stabs, 1024);
QDeviceDefineAQLM(mps, 1024);

} // namespace LibKet

#endif // QDEVICE_AQLM_HPP
