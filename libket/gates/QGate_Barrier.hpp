/** @file libket/gates/QGate_Barrier.hpp

    @brief C++ API quantum synchronization gate class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup barrier Barrier gate
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_BARRIER_HPP
#define QGATE_BARRIER_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief Barrier gate class

The quantum synchronization gate ensures that quantum expression
optimization does not proceed beyond the synchronization barrier.

The following code creates a quantum expression consisting of
three Hadamard gates applied consecutively, which by the built-in
optimization of quantum expressions yields a single Hadamard gate

\code
auto expr = gates::hadamard( gates::hadamard( gates::hadamard() ) ); // same as
auto expr = gates::hadamard();
\endcode

To prevent the built-in optimization from taking place, the
quantum synchronization gate can be inserted between the gates

\code
auto expr = gates::hadamard( gates::barrier ( gates::hadamard( gates::barrier ( gates::hadamard() ) ) ) );
\endcode

@ingroup barrier
*/
class QBarrier : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL(QBarrier, QBarrier);

  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ////
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    expr.append_kernel("barrier q;");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    expr.append_kernel([&]() {
      expr.kernel().wait(std::vector<std::size_t>(_filter::range(expr)), 0);
    });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    // QuEST does not support waiting
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param[in] expr QExpression object on input
  /// @result         QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    // QX does not provide a wait gate
    // for (auto i : _filter::range(expr))
    // expr.append_kernel(new qx::wait(i));

    return expr;
  }
#endif
  /// @}
};

/**
@brief Barrier gate creator
@ingroup barrier

This overload of the LibKet::gates::barrier() function can be used
as terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr1 = gates::hadamard( gates::barrier() );
\endcode

If the so-defined expression is used as sub-expression then the
synchronization gate ensures that no optimization takes place. In the
following example, the double application of the Hadamard gate would
be eliminated without the barrier inbetween the two gates

\code
auto expr2 = expr1( gates::hadamard() );
\endcode

@result UnaryQGate object
*/
inline constexpr auto
barrier() noexcept
{
  return UnaryQGate<filters::QFilter, QBarrier, filters::QFilter>(
    filters::QFilter{});
}

/// @brief Barrier gate default implementation
/// @defgroup barrier_aliases Aliases
/// @ingroup barrier
/// @{
  UNARY_GATE_OPTIMIZE_CREATOR_SINGLE(QBarrier, barrier);
UNARY_GATE_DEFAULT_CREATOR(QBarrier, barrier);
GATE_ALIAS(barrier, BARRIER);
UNARY_GATE_DEFAULT_IMPL(QBarrier, barrier);
/// @}

} // namespace gates
  
} // namespace LibKet

#endif // QGATE_BARRIER_HPP
