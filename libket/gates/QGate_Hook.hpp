/** @file libket/gates/QGate_Hook.hpp

    @brief C++ API quantum Hook class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup hook Hook gate
    @ingroup  unarygates
*/

#pragma once
#ifndef QGATE_HOOK_HPP
#define QGATE_HOOK_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
   @brief Hook gate class

   The LibKet Hook gate class implements a universal quantum
   gate that allows the user to inject code that is executed when
   the gate is applied

   @ingroup hook
*/
template<typename _functor, typename _functor_dag = _functor>
class QHook : public QGate
{
private:
  const _functor functor;
  const _functor_dag functor_dag;

public:
  QHook()
    : functor(_functor{})
    , functor_dag(_functor_dag{})
  {}

  QHook(const _functor& functor, const _functor_dag& functor_dag)
    : functor(functor)
    , functor_dag(functor_dag)
  {}

  UNARY_GATE_DEFAULT_DECL_FTORS(QHook, QHook);
  
  /// @brief Apply function
  /// @note specialization for LibKet expressions
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  inline static auto apply(QExpression<_qubits, _qbackend>& expr) noexcept ->
    typename std::enable_if<_functor::isExpr, QExpression<_qubits, _qbackend>>::type&
  {
    auto ftor = _functor{}();
    return ftor(expr);
  }

  /// @brief Apply function
  /// @note specialization for non-LibKet expressions
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  inline static auto apply(QExpression<_qubits, _qbackend>& expr) noexcept ->
    typename std::enable_if<!_functor::isExpr, QExpression<_qubits, _qbackend>>::type&
  {
    std::string hook = _functor{}();
    for (auto i : _filter::range(expr)) {
      for (auto c : hook)
        expr.append_kernel((c == '#' ? std::to_string(i) : std::string(1, c)));
    }
    return expr;
  }
};
  
/**
   @brief Hook gate creator
   @ingroup hook

   This overload of the LibKet::gates::hook() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression

   \code
   QFunctor_alias(expr_ftor, measure(h(init())) );
   auto expr = gates::hook<expr_ftor>();
   \endcode
*/
template<typename _functor, typename _functor_dag = _functor>
inline constexpr auto
hook() noexcept
{
  return UnaryQGate<filters::QFilter, QHook<_functor, _functor_dag>>(
    filters::QFilter{});
}

template<typename _functor, typename _functor_dag = _functor>
inline constexpr auto
hookdag() noexcept
{
  return UnaryQGate<filters::QFilter, QHook<_functor_dag, _functor>>(
    filters::QFilter{});
}

/// @brief Hook gate default implementation
/// @ingroup hook
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_FTORS_IDENTITY(QHook, hook);
  UNARY_GATE_DEFAULT_CREATOR_FTORS(QHook, hook, hookdag);
GATE_ALIAS_FTORS(hook, HOOK);
GATE_ALIAS_FTORS(hookdag, HOOKdag);
UNARY_GATE_DEFAULT_IMPL_FTORS(QHook, hook);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_HOOK_HPP
