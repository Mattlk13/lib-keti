/** @file libket/gates/QGate_Rotate_Ydag.hpp

    @brief C++ API quantum Rotate_Ydag class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers

    @defgroup rotate_ydag Rotate-Y gate (conjugate transpose)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_ROTATE_YDAG_HPP
#define QGATE_ROTATE_YDAG_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QRotate_Y;

/**
@brief Rotate_Ydag gate class

The Rotate_Ydag gate class implements the inverse quantum
Rotate_Y gate for an arbitrary number of quantum bits.

The \f$R_{y}^{\dagger}(\theta)\f$ gate is defined as the conjugate
transpose of the Rotate_Y gate.

The unitary matrix reads

\f[
R_{y}^{\dagger}(\theta) =
\begin{pmatrix}
\cos(\frac{\theta}{2}) & \sin(\frac{\theta}{2})\\
-\sin(\frac{\theta}{2}) & \cos(\frac{\theta}{2})
\end{pmatrix}
\f]

@ingroup rotate_ydag
*/
template<typename _angle, typename _tol = QConst_M_ZERO_t>
class QRotate_Ydag : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  UNARY_GATE_DEFAULT_DECL_AT(QRotate_Ydag, QRotate_Y);

  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RY[" + (-_angle{}).to_string() + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("cirq.YPowGate(exponent=" + (-_angle{}).to_string() +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "ry q[";
      for (auto i : _filter::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter::range(expr).end() - 1) ? "," : "], ");
      _expr += (-_angle{}).to_string() + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("ry(" + (-_angle{}).to_string(true) + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel([&]() { expr.kernel().ry(i, (-_angle{}).value()); });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("\trydag q" + utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RY (" + (-_angle{}).to_string(true) + ") " +
                           utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        quest::rotateY(expr.reg(), i, (-_angle{}).value());
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel(new qx::ry(i, (-_angle{}).value()));
    }
    return expr;
  }
#endif
  /// @}
};

/**
   @brief Rotate_Ydag gate creator
   @ingroup rotate_ydag
   
   This overload of the LibKet::gates::rotate_ydag() function can be
   used as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::rotate_ydag();
   \endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _angle>
inline constexpr auto rotate_ydag(_angle) noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_Ydag<_angle, _tol>>(
    filters::QFilter{});
}

/// @brief Rotate_Ydag gate default implementation
/// @defgroup rotate_ydag_aliases Aliases
/// @ingroup rotate_ydag
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_AT_NEGATIVE_IDENTITY(QRotate_Ydag, rotate_ydag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SAME_IDENTITY(QRotate_Y, rotate_ydag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_ADD_SINGLE(QRotate_Ydag, rotate_ydag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SUB_SINGLE(QRotate_Y, rotate_ydag);    
UNARY_GATE_DEFAULT_CREATOR_AT(QRotate_Ydag, rotate_ydag);
GATE_ALIAS_T(rotate_ydag, ROTATE_Ydag);
GATE_ALIAS_T(rotate_ydag, rydag);
GATE_ALIAS_T(rotate_ydag, RYdag);
GATE_ALIAS_T(rotate_ydag, Rydag);
UNARY_GATE_DEFAULT_IMPL_AT(QRotate_Ydag, rotate_ydag);
/// @}

/// @brief Rotate_Y gate default implementation
/// @defgroup rotate_y_aliases Aliases
/// @ingroup rotate_y
/// @{
UNARY_GATE_DEFAULT_IMPL_AT(QRotate_Y, rotate_y);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_YDAG_HPP
