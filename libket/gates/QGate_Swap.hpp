/** @file libket/gates/QGate_Swap.hpp

    @brief C++ API quantum swap class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup swap Swap gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_SWAP_HPP
#define QGATE_SWAP_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief Swap gate class

The LibKet SWAP gate class implements the quantum swap gate for an
arbitrary number of quantum bits.

The SWAP gate is a two-qubit operation that maps the basis state
\f$\left|00\right>\f$ to \f$\left|00\right>\f$,
\f$\left|01\right>\f$ to \f$\left|10\right>\f$,
\f$\left|10\right>\f$ to \f$\left|01\right>\f$ and
\f$\left|11\right>\f$ to \f$\left|11\right>\f$.

The SWAP gate swaps the state of the two qubits involved in the
operation.

The unitary matrix reads

\f[
\text{SWAP} =
\begin{pmatrix}
1 & 0 & 0 & 0\\                                          \
0 & 0 & 1 & 0\\
0 & 1 & 0 & 0\\
0 & 0 & 0 & 1
\end{pmatrix}
\f]

@ingroup swap
*/
class QSwap : public QGate
{
public:
  BINARY_GATE_DEFAULT_DECL(QSwap, QSwap);
  
  ///@{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("SWAP q[" + utils::to_string(std::get<0>(i)) + "],q[" +
                         utils::to_string(std::get<1>(i)) + "]\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("cirq.SWAP.on(q[" + utils::to_string(std::get<0>(i)) +
                         "],q[" + utils::to_string(std::get<1>(i)) + "])\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    std::string _expr = "swap q[";
    for (auto i : _filter0::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("swap q[" + utils::to_string(std::get<0>(i)) +
                         "], q[" + utils::to_string(std::get<1>(i)) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel(
        [&]() { expr.kernel().swap(std::get<0>(i), std::get<1>(i)); });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("\tswap q" + utils::to_string(std::get<0>(i)) + ",q" +
                         utils::to_string(std::get<1>(i)) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("SWAP " + utils::to_string(std::get<0>(i)) + " " +
                         utils::to_string(std::get<1>(i)) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      quest::swapGate(expr.reg(), std::get<0>(i), std::get<1>(i));

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SWAP gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel(new qx::swap(std::get<0>(i), std::get<1>(i)));

    return expr;
  }
#endif
  ///@}
};

/**
@brief Swap gate creator
@ingroup swap

This overload of the LibKet::gate:swap() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::swap();
\endcode
*/
inline constexpr auto
swap() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QSwap>(
    filters::QFilter{}, filters::QFilter{});
}

namespace detail {
template<typename _expr0, typename _expr1, typename = void>
struct swap_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QSwap,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

/// @defgroup swap_aliases Aliases
/// @ingroup swap
///@{  
#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0_0,
         typename __expr0_1,
         typename __filter0,
         typename __expr1_0,
         typename __expr1_1,
         typename __filter1>
struct swap_impl<
  BinaryQGate<__expr0_0, __expr0_1, QSwap, __filter0>,
  BinaryQGate<__expr1_0, __expr1_1, QSwap, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value) ||
     (std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_1>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_0>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0_0, __expr0_1, QSwap, __filter0>& expr0,
    const BinaryQGate<__expr1_0, __expr1_1, QSwap, __filter1>& expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return swap_impl<decltype(typename filters::getFilter<__filter0>::type{}(
                       all(expr1.expr1(all(
                         expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
                     typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return swap_impl<typename filters::getFilter<__filter0>::type,
                     decltype(typename filters::getFilter<__filter1>::type{}(
                       all(expr0.expr0(all(
                         expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QSwap here */
    !std::is_same<typename gates::getGate<_expr0>::type, QSwap>::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct swap_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Swap gate is handled explicitly */
    (!std::is_base_of<
      QSwap,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QSwap here */
    !std::is_same<typename gates::getGate<_expr1>::type, QSwap>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief Swap gate creator

   This overload of the LibKet::gates::swap() function
   eliminates the double-application of the Swap gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct swap_impl<
  BinaryQGate<__expr0, __expr1, QSwap, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value) ||
     (std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Swap gate is handled explicitly */
    (!std::is_base_of<
      QSwap,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QSwap, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the Swap gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief Swap gate creator

This overload of the LibKet::gates::swap() function accepts
two expressions as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
swap(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::swap_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief Swap gate creator

This overload of the LibKet::gates::swap() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
swap(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::swap_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief Swap gate creator

This overload of the LibKet::gates::swap() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
swap(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::swap_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief Swap gate creator

This overload of the LibKet::gates::swap() function accepts
two expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
swap(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::swap_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief Swap gate creator

Function alias for LibKet::gates::swap()
*/
template<typename... Args>
inline constexpr auto
SWAP(Args&&... args)
{
  return swap(std::forward<Args>(args)...);
}

/**
@brief Swap gate creator

Function alias for LibKet::gates::swap()
*/
template<typename... Args>
inline constexpr auto
swapdag(Args&&... args)
{
  return swap(std::forward<Args>(args)...);
}

/**
@brief Swap gate creator

Function alias for LibKet::gates::swap()
*/
template<typename... Args>
inline constexpr auto
SWAPdag(Args&&... args)
{
  return swap(std::forward<Args>(args)...);
}

BINARY_GATE_DEFAULT_IMPL(QSwap, swap);
///@}

} // namespace gates

} // namespace LibKet

#endif // QGATE_SWAP_HPP
