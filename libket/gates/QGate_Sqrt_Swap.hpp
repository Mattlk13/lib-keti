/** @file libket/gates/QGate_Sqrt_Swap.hpp

    @brief C++ API quantum square-root-of-swap class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers

    @defgroup sqrtswap Sqrt-Swap gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_SQRT_SWAP_HPP
#define QGATE_SQRT_SWAP_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>
#include <gates/QGate_CNOT.hpp>
#include <gates/QGate_Hadamard.hpp>
#include <gates/QGate_T.hpp>
#include <gates/QGate_Tdag.hpp>

namespace LibKet {

namespace gates {

/**
@brief Swap gate class

The Square root of SWAP gate class implements the quantum
square root of SWAP gate for an arbitrary number of quantum bits.

The square root of SWAP gate (or square root of SWAP,
\f$\sqrt{\text{SWAP}}\f$) is two qubit operation which implements the
square root of SWAP gate.

The unitary matrix reads

\f[
\sqrt{\text{SWAP}} =
\begin{pmatrix}
1 & 0 & 0 & 0 \\
0 & \frac{1+i}{2} & \frac{1-i}{2} & 0 \\
0 & \frac{1-i}{2} & \frac{1+i}{2} & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}
\f]

@ingroup sqrtswap
*/
class QSqrt_Swap : public QGate
{
public:
  BINARY_GATE_DEFAULT_DECL(QSqrt_Swap, QSqrt_Swap);
  
  /// @brief Apply function
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           QBackendType _qbackend>
  inline static QExpression<_qubits, _qbackend>& apply(
    QExpression<_qubits, _qbackend>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "SQRT_SWAP gate can only be applied to quantum objects of the same size");

    auto e = S(gototag<1>(Sdag(gototag<0>(
      cnot(h(Tdag(gototag<0>(
             h(cnot(h(gototag<0>(h(Tdag(gototag<1>(T(h(gototag<0>(
                      cnot(tag<0>(_filter0{}), tag<1>(_filter1{})))))))))),
                    tag<2>(_filter1{})))))),
           tag<3>(_filter1{}))))));

    return e(expr);
  }
};

/**
@brief Square-root-of-swap gate creator
@ingroup sqrtswap

This overload of the LibKet::gate:sqrt_swap() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::sqrt_swap();
\endcode
*/
inline constexpr auto
sqrt_swap() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QSqrt_Swap>(
    filters::QFilter{}, filters::QFilter{});
}

/// @defgroup sqrtswap_aliases Aliases
/// @ingroup sqrtswap
///@{  
#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(
  const _expr0& expr0,
  const BinaryQGate<typename std::decay<_expr0>::type,
                    _expr1,
                    QSqrt_Swap,
                    typename filters::getFilter<_expr1>::type>& expr1) noexcept
{
  std::cout << "TYPE1a\n";
  return expr1.expr1;
}

/**
@brief Swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(
  _expr0&& expr0,
  const BinaryQGate<typename std::decay<_expr0>::type,
                    _expr1,
                    QSqrt_Swap,
                    typename filters::getFilter<_expr1>::type>& expr1) noexcept
{
  std::cout << "TYPE1b\n";
  return expr1.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(
  const _expr0& expr0,
  BinaryQGate<typename std::decay<_expr0>::type,
              _expr1,
              QSqrt_Swap,
              typename filters::getFilter<_expr1>::type>&& expr1) noexcept
{
  std::cout << "TYPE1c\n";
  return expr1.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(
  _expr0&& expr0,
  BinaryQGate<typename std::decay<_expr0>::type,
              _expr1,
              QSqrt_Swap,
              typename filters::getFilter<_expr1>::type>&& expr1) noexcept
{
  std::cout << "TYPE1d\n";
  return expr1.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename __expr0, typename _expr1>
inline constexpr auto
sqrt_swap(const _expr0& expr0,
          const BinaryQGate<__expr0,
                            _expr1,
                            QSqrt_Swap,
                            typename std::decay<__expr0>::type>& expr1) noexcept
  -> typename std::enable_if<
    std::is_same<typename std::decay<_expr0>::type,
                 typename filters::getFilter<_expr1>::type>::value,
    decltype(expr1.expr1)>::type

{
  std::cout << "TYPE2a\n";
  return expr1.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename __expr0, typename _expr1>
inline constexpr auto
sqrt_swap(_expr0&& expr0,
          const BinaryQGate<__expr0,
                            _expr1,
                            QSqrt_Swap,
                            typename std::decay<__expr0>::type>& expr1) noexcept
  -> typename std::enable_if<
    std::is_same<typename std::decay<_expr0>::type,
                 typename filters::getFilter<_expr1>::type>::value,
    decltype(expr1.expr1)>::type

{
  std::cout << "TYPE2b\n";
  return expr1.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename __expr0, typename _expr1>
inline constexpr auto
sqrt_swap(
  const _expr0& expr0,
  BinaryQGate<__expr0, _expr1, QSqrt_Swap, typename std::decay<__expr0>::type>&&
    expr1) noexcept ->
  typename std::enable_if<
    std::is_same<typename std::decay<_expr0>::type,
                 typename filters::getFilter<_expr1>::type>::value,
    decltype(expr1.expr1)>::type

{
  std::cout << "TYPE2c\n";
  return expr1.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename __expr0, typename _expr1>
inline constexpr auto
sqrt_swap(
  _expr0&& expr0,
  BinaryQGate<__expr0, _expr1, QSqrt_Swap, typename std::decay<__expr0>::type>&&
    expr1) noexcept ->
  typename std::enable_if<
    std::is_same<typename std::decay<_expr0>::type,
                 typename filters::getFilter<_expr1>::type>::value,
    decltype(expr1.expr1)>::type

{
  std::cout << "TYPE2d\n";
  return expr1.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(const BinaryQGate<_expr0,
                            _expr1,
                            QSqrt_Swap,
                            typename filters::getFilter<_expr1>::type>& expr0,
          const typename std::decay<_expr0>::type& expr1) noexcept
{
  std::cout << "TYPE3a\n";
  return expr0.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(BinaryQGate<_expr0,
                      _expr1,
                      QSqrt_Swap,
                      typename filters::getFilter<_expr1>::type>&& expr0,
          const typename std::decay<_expr0>::type& expr1) noexcept
{
  std::cout << "TYPE3b\n";
  return expr0.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(const BinaryQGate<_expr0,
                            _expr1,
                            QSqrt_Swap,
                            typename filters::getFilter<_expr1>::type>& expr0,
          typename std::decay<_expr0>::type&& expr1) noexcept
{
  std::cout << "TYPE3b\n";
  return expr0.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(BinaryQGate<_expr0,
                      _expr1,
                      QSqrt_Swap,
                      typename filters::getFilter<_expr1>::type>&& expr0,
          typename std::decay<_expr0>::type&& expr1) noexcept
{
  std::cout << "TYPE3d\n";
  return expr0.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(const BinaryQGate<_expr0,
                            _expr1,
                            QSqrt_Swap,
                            typename filters::getFilter<_expr0>::type>& expr0,
          const typename filters::getFilter<_expr1>::type& expr1) noexcept
{
  std::cout << "TYPE4a\n";
  return expr0.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(BinaryQGate<_expr0,
                      _expr1,
                      QSqrt_Swap,
                      typename filters::getFilter<_expr0>::type>&& expr0,
          const typename filters::getFilter<_expr1>::type& expr1) noexcept
{
  std::cout << "TYPE4b\n";
  return expr0.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(const BinaryQGate<_expr0,
                            _expr1,
                            QSqrt_Swap,
                            typename filters::getFilter<_expr0>::type>& expr0,
          typename filters::getFilter<_expr1>::type&& expr1) noexcept
{
  std::cout << "TYPE4c\n";
  return expr0.expr1;
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function
eliminates the double-application of the square-root-of-swap gate
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(BinaryQGate<_expr0,
                      _expr1,
                      QSqrt_Swap,
                      typename filters::getFilter<_expr0>::type>&& expr0,
          typename filters::getFilter<_expr1>::type&& expr1) noexcept
{
  std::cout << "TYPE4d\n";
  return expr0.expr1;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function accepts
two expressions as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QSqrt_Swap,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QSqrt_Swap,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QSqrt_Swap,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief Square-root-of-swap gate creator

This overload of the LibKet::gates::sqrt_swap() function accepts
two expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
sqrt_swap(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QSqrt_Swap,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief Square-root-of-swap gate creator

Function alias for LibKet::gates::sqrt_swap()
*/
template<typename... Args>
inline constexpr auto
SQRT_SWAP(Args&&... args)
{
  return sqrt_swap(std::forward<Args>(args)...);
}

/**
@brief Square-root-of-swap gate creator

Function alias for LibKet::gates::sqrt_swap()
*/
template<typename... Args>
inline constexpr auto
sswap(Args&&... args)
{
  return sqrt_swap(std::forward<Args>(args)...);
}

/**
@brief Square-root-of-swap gate creator

Function alias for LibKet::gates::sqrt_swap()
*/
template<typename... Args>
inline constexpr auto
SSWAP(Args&&... args)
{
  return sqrt_swap(std::forward<Args>(args)...);
}

/**
@brief Square-root-of-swap gate creator

Function alias for LibKet::gates::sqrt_swap()
*/
template<typename... Args>
inline constexpr auto
sqrt_swapdag(Args&&... args)
{
  return sqrt_swap(std::forward<Args>(args)...);
}

/**
@brief Square-root-of-swap gate creator

Function alias for LibKet::gates::sqrt_swap()
*/
template<typename... Args>
inline constexpr auto
SQRT_SWAPdag(Args&&... args)
{
  return sqrt_swap(std::forward<Args>(args)...);
}

/**
@brief Square-root-of-swap gate creator

Function alias for LibKet::gates::sqrt_swap()
*/
template<typename... Args>
inline constexpr auto
sswapdag(Args&&... args)
{
  return sqrt_swap(std::forward<Args>(args)...);
}

/**
@brief Square-root-of-swap gate creator

Function alias for LibKet::gates::sqrt_swap()
*/
template<typename... Args>
inline constexpr auto
SSWAPdag(Args&&... args)
{
  return sqrt_swap(std::forward<Args>(args)...);
}

BINARY_GATE_DEFAULT_IMPL(QSqrt_Swap, sqrt_swap);
///@}

} // namespace gates

} // namespace LibKet

#endif // QGATE_SQRT_SWAP_HPP
