/** @file libket/gates/QGate_CUnitary2.hpp

    @brief C++ API \f$2\times 2\f$ controlled unitary gate class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup cu2 Controlled-U2 gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_CUNITARY2_HPP
#define QGATE_CUNITARY2_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

template<typename _functor, typename _tol>
class QCUnitary2dag;
  
/**
@brief \f$2\times 2\f$ controlled unitary gate class

The \f$2\times2\f$ controlled unitary gate accepts an arbitrary \f$2\times 2\f$
unitary matrix \f$U\f$ as input and performs the controlled ZYZ decomposition of
\f$U\f$

\f[
U = \exp(i\Phi) R_z(\alpha)R_y(\beta)R_z(\gamma)
\f]

with \f$\Phi,\alpha,\beta,\gamma\f$ are rotation angles.

@ingroup cu2
*/
template<typename _functor, typename _tol = QConst_M_ZERO_t>
class QCUnitary2 : public QGate
{
public:
  BINARY_GATE_DEFAULT_DECL_FTOR(QCUnitary2, QCUnitary2dag);
  
  #ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr))){
        expr.append_kernel("CTRL(RZ[" + std::to_string(x(0)) + "]) q[" +
                           utils::to_string(std::get<0>(i)) + "],q[" +
                           utils::to_string(std::get<1>(i)) + "]\n");
      }  
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr))){
        expr.append_kernel("CTRL(RZ[" + std::to_string(x(1)) + "]) q[" +
                           utils::to_string(std::get<0>(i)) + "],q[" +
                           utils::to_string(std::get<1>(i)) + "]\n");
      }  
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr))){
        expr.append_kernel("CTRL(RZ[" + std::to_string(x(2)) + "]) q[" +
                           utils::to_string(std::get<0>(i)) + "],q[" +
                           utils::to_string(std::get<1>(i)) + "]\n");
      }  
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cirq.ControlledGate(cirq.rz(rads=" + std::to_string(x(0)) +
                           ")).on(q[" + utils::to_string(std::get<0>(i)) +
                           "],q[" + utils::to_string(std::get<1>(i)) + "])\n");
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cirq.ControlledGate(cirq.ry(rads=" + std::to_string(x(1)) +
                           ")).on(q[" + utils::to_string(std::get<0>(i)) +
                           "],q[" + utils::to_string(std::get<1>(i)) + "])\n");
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cirq.ControlledGate(cirq.rz(rads=" + std::to_string(x(2)) +
                           ")).on(q[" + utils::to_string(std::get<0>(i)) +
                           "],q[" + utils::to_string(std::get<1>(i)) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      std::string _expr = "c-Rz q[";
      for (auto i : _filter0::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(expr).end() - 1) ? "," : "], ");
      _expr += std::to_string(x(0)) + "\n";
      expr.append_kernel(_expr);
    }
    if (tolerance(x(1), _tol{})) {
      std::string _expr = "c-Ry q[";
      for (auto i : _filter0::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(expr).end() - 1) ? "," : "], ");
      _expr += std::to_string(x(1)) + "\n";
      expr.append_kernel(_expr);
    }
    if (tolerance(x(2), _tol{})) {
      std::string _expr = "c-Rz q[";
      for (auto i : _filter0::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(expr).end() - 1) ? "," : "], ");
      _expr += std::to_string(x(2)) + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  { 
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("crz(" + std::to_string(x(0)) + 
                           ") q[" + utils::to_string(std::get<0>(i)) + "] " +
                           ", q[" + utils::to_string(std::get<1>(i)) + "];\n");
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cry(" + std::to_string(x(1)) + 
                           ") q[" + utils::to_string(std::get<0>(i)) + "] " +
                           ", q[" + utils::to_string(std::get<1>(i)) + "];\n");
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("crz(" + std::to_string(x(2)) + 
                           ") q[" + utils::to_string(std::get<0>(i)) + "] " +
                           ", q[" + utils::to_string(std::get<1>(i)) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel([&]() {
          expr.kernel().controlled_rz(
            std::get<0>(i), std::get<1>(i), x(0));
        });
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel([&]() {
          expr.kernel().controlled_ry(
            std::get<0>(i), std::get<1>(i), x(1));
        });
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel([&]() {
          expr.kernel().controlled_rz(
            std::get<0>(i), std::get<1>(i), x(2));
        });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("\tdef c-Ry, 1, 'Rx'\n \tc-Rx q" + utils::to_string(std::get<0>(i)) +
                           ",q" + utils::to_string(std::get<1>(i)) + " # " +
                           std::to_string(x(0)) + "\n");
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("\tdef c-Rz, 1, 'Rx'\n \tc-Rx q" + utils::to_string(std::get<0>(i)) +
                           ",q" + utils::to_string(std::get<1>(i)) + " # " +
                           std::to_string(x(1)) + "\n");
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("\tdef c-Ry, 1, 'Rx'\n \tc-Rx q" + utils::to_string(std::get<0>(i)) +
                           ",q" + utils::to_string(std::get<1>(i)) + " # " +
                           std::to_string(x(2)) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CONTROLLED RZ(" + std::to_string(x(0)) + ") " +
                           utils::to_string(std::get<0>(i)) + " " +
                           utils::to_string(std::get<1>(i)) + "\n");
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CONTROLLED RY(" + std::to_string(x(1)) + ") " +
                           utils::to_string(std::get<0>(i)) + " " +
                           utils::to_string(std::get<1>(i)) + "\n");
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CONTROLLED RZ(" + std::to_string(x(2)) + ") " +
                           utils::to_string(std::get<0>(i)) + " " +
                           utils::to_string(std::get<1>(i)) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        quest::controlledRotateZ(
          expr.reg(), std::get<0>(i), std::get<1>(i), (qreal)x(0));
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        quest::controlledRotateY(
          expr.reg(), std::get<0>(i), std::get<1>(i), (qreal)x(1));
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        quest::controlledRotateZ(
          expr.reg(), std::get<0>(i), std::get<1>(i), (qreal)x(2));
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CU2 gate can only be applied to quantum "
                  "objects of the same size");
    auto x = decompose();
    if (tolerance(x(0), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel(new qx::bin_ctrl(std::get<0>(i), new qx::rx(std::get<1>(i), (double)x(0))));
    }
    if (tolerance(x(1), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel(new qx::bin_ctrl(std::get<0>(i), new qx::rx(std::get<1>(i), (double)x(1))));
    }
    if (tolerance(x(2), _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel(new qx::bin_ctrl(std::get<0>(i), new qx::rx(std::get<1>(i), (double)x(2))));
    }
    return expr;
  }
#endif

private:
  inline static arma::vec decompose()
  {
    arma::Mat<arma::cx_double> U = arma::conv_to<arma::Mat<arma::cx_double> >::from(_functor{}());
    arma::vec x = { 0.0, 0.0, 0.0 };

    //Check for unitarity
    //Get identity matrix values from input matrix
    auto I0 = U[0]*conj(U[0]) + U[1]*conj(U[2]);
    auto I1 = U[0]*conj(U[1]) + U[1]*conj(U[3]);
    auto I2 = U[2]*conj(U[0]) + U[3]*conj(U[2]);
    auto I3 = U[2]*conj(U[1]) + U[3]*conj(U[3]);

    //Check if values fall within tolerance values
    double tolerance = 0.001;
    if(abs(real(I0) - 1) > tolerance or 
       abs(real(I1)) > tolerance or
       abs(real(I2)) > tolerance or 
       abs(real(I3) - 1) > tolerance or 
       abs(imag(I0)) > tolerance or 
       abs(imag(I1)) > tolerance or
       abs(imag(I2)) > tolerance or 
       abs(imag(I3)) > tolerance ){
      
      std::cout << "Matrix is not unitary! Returning zeros..." << std::endl;
    }
    else{

      if(abs(U[0]) >= abs(U[1])){
        x[1] = 2*acos(abs(U[0]));
      } else{
        x[1] = 2*acos(abs(U[1]));
      }

      double plus_term;
      double min_term; 
      if(cos(0.5*x[1]) == 0){
        plus_term = 0.0;
      }
      else{
        plus_term = 2*atan2(imag(U[3]/cos(0.5*x[1])), real(U[3]/cos(0.5*x[1])));
      }

      if(sin(0.5*x[1]) == 0){
        min_term = 0.0;
      }
      else{
        min_term =  2*atan2(imag(U[2]/sin(0.5*x[1])), real(U[2]/sin(0.5*x[1])));
      }

      x[0] = (plus_term + min_term)/2;
      x[2] = (plus_term - min_term)/2;  
    }    

    return x;
  }
  
};
  
/**
@brief \f$2\times 2\f$ controlled unitary gate creator
@ingroup cu2

This overload of the LibKet::gates::cunitary2() function can be used
as terminal, i.e. the inner-most gate in a quantum expression

\code
struct CU2_ftor {
  inline auto operator()() const noexcept
  {
    return arma::vec{ 1.0/sqrt(2.0), -1.0/sqrt(2.0),                               
                      1.0/sqrt(2.0),  1.0/sqrt(2.0)};
  }
};

auto expr = gates::cunitary2<CU2_ftor>();
\endcode
*/
template<typename _functor, typename _tol = QConst_M_ZERO_t>
inline constexpr auto cunitary2() noexcept
{
  return BinaryQGate<filters::QFilter, QCUnitary2<_functor, _tol>, filters::QFilter>(
    filters::QFilter{}, filters::QFilter{});
}

/// @brief \f$2\times 2\f$ controlled unitary gate default implementation
/// @defgroup cu2_aliases Aliases
/// @ingroup cu2
/// @{
BINARY_GATE_DEFAULT_CREATOR_FTOR_T(QCUnitary2, cunitary2);
GATE_ALIAS_FTOR_T(cunitary2, cu2);
GATE_ALIAS_FTOR_T(cunitary2, CUNITARY2);
GATE_ALIAS_FTOR_T(cunitary2, CU2);
///@}  

} // namespace gates

} // namespace LibKet

#endif // QGATE_UNITARY2_HPP
