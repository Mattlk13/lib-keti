/** @file libket/gates/QGate_Phasedag.hpp

    @brief C++ API quantum phasedag class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup phasedag Phase gate (conjugate transpose)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_PHASEDAG_HPP
#define QGATE_PHASEDAG_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QPhase;
  
/**
@brief PHASEdag gate class

The PHASEdag gate class implements the inverse quantum phase gate for
an arbitrary number of quantum bits.

The PHASEdag gate is a single-qubit operation that maps the basis state
\f$\left|0\right>\f$ to \f$\left|0\right>\f$ and \f$\left|1\right>\f$
to \f$\exp(-i\theta)\left|1\right>\f$.

The unitary matrix reads

\f[
\text{PHASE}^{\dagger} =
\begin{pmatrix}
1 & 0\\
0 & \exp(-i\theta)
\end{pmatrix}
\f]

@ingroup phasedag
*/
template<typename _angle, typename _tol = QConst_M_ZERO_t>
class QPhasedag : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  UNARY_GATE_DEFAULT_DECL_AT(QPhasedag, QPhase);

  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("PH[" + (-_angle{}).to_string() + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ////
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("cirq.ZPowGate(exponent=" + (-_angle{}).to_string() +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    QWarn << "cQASM does not have an inverse phase gate implemented, we estimate "
          << "this up to a phase shift by using RZ"
          << std::endl;
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "rz q[";
      for (auto i : _filter::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter::range(expr).end() - 1) ? "," : "], ");
      _expr += (-_angle{}).to_string() + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("u1(" + (-_angle{}).to_string(true) + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel([&]() { expr.kernel().rz(i, (-_angle{}).value()); });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("\tphdag q" + utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("PHASE (" + (-_angle{}).to_string(true) + ") " +
                           utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        quest::rotateX(expr.reg(), i, (qreal)(-_angle{}).value());
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel(new qx::rz(i, (-_angle{}).value()));
    }
    return expr;
  }
#endif
  /// @}
};

/**
   @brief Phasedag gate creator
   @ingroup phasedag
   
   This overload of the LibKet::gates::phasedag() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::phasedag();
   \endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _angle>
inline constexpr auto phasedag(_angle) noexcept
{
  return UnaryQGate<filters::QFilter, QPhasedag<_angle, _tol>>(filters::QFilter{});
}

/// @brief Phasedag gate default implementation
/// @defgroup phasedag_aliases Aliases
/// @ingroup phasedag
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_AT_NEGATIVE_IDENTITY(QPhasedag, phasedag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SAME_IDENTITY(QPhase, phasedag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_ADD_SINGLE(QPhasedag, phasedag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SUB_SINGLE(QPhase, phasedag);    
UNARY_GATE_DEFAULT_CREATOR_AT(QPhasedag, phasedag);
GATE_ALIAS_T(phasedag, PHASEdag);
GATE_ALIAS_T(phasedag, phdag);
GATE_ALIAS_T(phasedag, PHdag);
UNARY_GATE_DEFAULT_IMPL_AT(QPhasedag, phasedag);
/// @}

/// @brief Phase gate default implementation
/// @ingroup phase
/// @{
UNARY_GATE_DEFAULT_IMPL_AT(QPhase, phase);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_PHASEDAG_HPP
