/** @file libket/gates/QGate_Pauli_Y.hpp

    @brief C++ API quantum Pauli_Y class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup pauli_y Pauli-Y gate
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_PAULI_Y_HPP
#define QGATE_PAULI_Y_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief Pauli_Y gate class

The Pauli_Y gate class implements the quantum Pauli_Y
gate for an arbitrary number of quantum bits.

The Pauli_Y gate is a single-qubit operation that maps the basis
state \f$\left|0\right>\f$ to
\f$-i\left|1\right>\f$ and \f$\left|1\right>\f$ to
\f$i\left|0\right>\f$.

The Pauli-Y gate is a single-qubit rotation through \f$\pi\f$ radians around the
y-axis.

The unitary matrix reads

\f[
Y =
\begin{pmatrix}
0 & -i\\
i & 0
\end{pmatrix}
\f]

@ingroup pauli_y
*/
class QPauli_Y : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL(QPauli_Y, QPauli_Y);

  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("Y q[" + utils::to_string(i) + "]\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ////
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("cirq.Y.on(q[" + utils::to_string(i) + "])\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    std::string _expr = "y q[";
    for (auto i : _filter::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("y q[" + utils::to_string(i) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel([&]() { expr.kernel().y(i); });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("\ty q" + utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel("Y " + utils::to_string(i) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      quest::pauliY(expr.reg(), i);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    for (auto i : _filter::range(expr))
      expr.append_kernel(new qx::pauli_y(i));

    return expr;
  }
#endif
  /// @}
};

/**
   @brief Pauli_Y gate creator
   @ingroup pauli_y
   
   This overload of the LibKet::gates::pauli_y() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::pauli_y();
   \endcode
*/
inline constexpr auto
pauli_y() noexcept
{
  return UnaryQGate<filters::QFilter, QPauli_Y>(filters::QFilter{});
}

/// @brief Pauli_Y gate default implementation
/// @defgroup pauli_y_aliases Aliases
/// @ingroup pauli_y
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_IDENTITY(QPauli_Y, pauli_y);
UNARY_GATE_DEFAULT_CREATOR(QPauli_Y, pauli_y);
GATE_ALIAS(pauli_y, PAULI_Y);
GATE_ALIAS(pauli_y, y);
GATE_ALIAS(pauli_y, Y);
GATE_ALIAS(pauli_y, pauli_ydag);
GATE_ALIAS(pauli_y, PAULI_Ydag);
GATE_ALIAS(pauli_y, ydag);
GATE_ALIAS(pauli_y, Ydag);
UNARY_GATE_DEFAULT_IMPL(QPauli_Y, pauli_y);
/// @}

} // namespace gates
  
} // namespace LibKet

#endif // QGATE_PAULI_Y_HPP
