/** @file libket/gates/QGate_Rotate_Zdag.hpp

    @brief C++ API quantum Rotate_Zdag class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers

    @defgroup rotate_zdag Rotate-Z gate (conjugate transpose)
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_ROTATE_ZDAG_HPP
#define QGATE_ROTATE_ZDAG_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QRotate_Z;

/**
@brief Rotate_Zdag gate class

The Rotate_Zdag gate class implements the inverse quantum
Rotate_Z gate for an arbitrary number of quantum bits.

The \f$R_{z}^{\dagger}(\theta) \f$ gate is defined as the conjugate
transpose of the Rotate_Z gate.

The unitary matrix reads

\f[
R_{z}^{\dagger}(\theta)=
\begin{pmatrix}
\exp(i\frac{\theta}{2}) & 0\\
0 & \exp(-i\frac{\theta}{2})
\end{pmatrix}
\f]

@ingroup rotate_zdag
*/
template<typename _angle, typename _tol = QConst_M_ZERO_t>
class QRotate_Zdag : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;
  
  UNARY_GATE_DEFAULT_DECL_AT(QRotate_Zdag, QRotate_Z);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RZ[" + (-_angle{}).to_string() + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("cirq.ZPowGate(exponent=" + (-_angle{}).to_string() +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "rz q[";
      for (auto i : _filter::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter::range(expr).end() - 1) ? "," : "], ");
      _expr += (-_angle{}).to_string() + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("rz(" + (-_angle{}).to_string(true) + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel([&]() { expr.kernel().rz(i, (-_angle{}).value()); });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("\trzdag q" + utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RZ (" + (-_angle{}).to_string(true) + ") " +
                           utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        quest::rotateZ(expr.reg(), i, (-_angle{}).value());
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel(new qx::rz(i, (-_angle{}).value()));
    }
    return expr;
  }
#endif
  /// @}
};

/**
   @brief Rotate_Zdag gate creator
   @ingroup rotate_zdag

   This overload of the LibKet::gates::rotate_zdag() function can be
   used as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::rotate_zdag();
   \endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _angle>
inline constexpr auto rotate_zdag(_angle) noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_Zdag<_angle, _tol>>(
    filters::QFilter{});
}

/// @brief Rotate_Zdag gate default implementation
/// @defgroup rotate_zdag_aliases Aliases
/// @ingroup rotate_zdag
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_AT_NEGATIVE_IDENTITY(QRotate_Zdag, rotate_zdag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SAME_IDENTITY(QRotate_Z, rotate_zdag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_ADD_SINGLE(QRotate_Zdag, rotate_zdag);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SUB_SINGLE(QRotate_Z, rotate_zdag);    
UNARY_GATE_DEFAULT_CREATOR_AT(QRotate_Zdag, rotate_zdag);
GATE_ALIAS_T(rotate_zdag, ROTATE_Zdag);
GATE_ALIAS_T(rotate_zdag, rzdag);
GATE_ALIAS_T(rotate_zdag, RZdag);
GATE_ALIAS_T(rotate_zdag, Rzdag);
UNARY_GATE_DEFAULT_IMPL_AT(QRotate_Zdag, rotate_zdag);
/// @}

/// @brief Rotate_Z gate default implementation
/// @defgroup rotate_z_aliases Aliases
/// @ingroup rotate_z
/// @{
UNARY_GATE_DEFAULT_IMPL_AT(QRotate_Z, rotate_z);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_ZDAG_HPP
