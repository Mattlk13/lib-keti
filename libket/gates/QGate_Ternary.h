/** @file libket/gates/QGate_Ternary.h

    @brief C++ API: Quantum gate macros for ternary gates

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_TERNARY_H
#define QGATE_TERNARY_H

/// @brief Macro declares default (member) functions for ternary gates
#define TERNARY_GATE_DEFAULT_DECL(_gate, _dagger)                \
  using self_t   = _gate;                                        \
  using dagger_t = _dagger;                                      \
                                                                 \
  inline constexpr auto operator()() const noexcept;             \
                                                                 \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto operator()(const T0& t0, const T1& t1, const T2& t2) const noexcept; \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto operator()(const T0& t0, const T1& t1, T2&& t2) const noexcept; \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto operator()(const T0& t0, T1&& t1, const T2& t2) const noexcept; \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto operator()(const T0& t0, T1&& t1, T2&& t2) const noexcept; \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto operator()(T0&& t0, const T1& t1, const T2& t2) const noexcept; \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto operator()(T0&& t0, const T1& t1, T2&& t2) const noexcept; \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto operator()(T0&& t0, T1&& t1, const T2& t2) const noexcept; \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto operator()(T0&& t0, T1&& t1, T2&& t2) const noexcept; \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string show() const noexcept;                             \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string dot() const noexcept;                              \
                                                                        \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro implements default (member) functions for ternary gates
#define TERNARY_GATE_DEFAULT_IMPL(_class, _func)                        \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class& gate)                      \
  {                                                                     \
    os << #_func"(";                                                    \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline static auto                                                    \
  show(const _class& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"\n";                                                  \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline static auto                                                    \
  dot(const _class& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"\n";                                                  \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  inline static constexpr auto                                          \
  dagger(const _class& gate) noexcept                                   \
  {                                                                     \
    return _class::dagger_t();                                          \
  }                                                                     \
                                                                        \
  inline constexpr auto                                                 \
  _class::operator()() const noexcept                                   \
  {                                                                     \
    return LibKet::gates::_func();                                      \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto                                                 \
  _class::operator()(const T0& t0, const T1& t1, const T2& t2) const noexcept \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1),                   \
                                std::forward<T2>(t2));                  \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto                                                 \
  _class::operator()(const T0& t0, const T1& t1, T2&& t2) const noexcept \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1),                   \
                                std::forward<T2>(t2));                  \
  }                                                                     \
  template<typename T0, typename T1, typename T2>                       \
                                                                        \
  inline constexpr auto                                                 \
  _class::operator()(const T0& t0, T1&& t1, const T2& t2) const noexcept \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1),                   \
                                std::forward<T2>(t2));                  \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto                                                 \
  _class::operator()(const T0& t0, T1&& t1, T2&& t2) const noexcept     \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1),                   \
                                std::forward<T2>(t2));                  \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto                                                 \
  _class::operator()(T0&& t0, const T1& t1, const T2& t2) const noexcept \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1),                   \
                                std::forward<T2>(t2));                  \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto                                                 \
  _class::operator()(T0&& t0, const T1& t1, T2&& t2) const noexcept     \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1),                   \
                                std::forward<T2>(t2));                  \
  }                                                                     \
  template<typename T0, typename T1, typename T2>                       \
                                                                        \
  inline constexpr auto                                                 \
  _class::operator()(T0&& t0, T1&& t1, const T2& t2) const noexcept     \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1),                   \
                                std::forward<T2>(t2));                  \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1, typename T2>                       \
  inline constexpr auto                                                 \
  _class::operator()(T0&& t0, T1&& t1, T2&& t2) const noexcept          \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1),                   \
                                std::forward<T2>(t2));                  \
  }                                                                     \
                                                                        \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class::show() const noexcept                                         \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class::dot() const noexcept                                          \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  inline constexpr auto                                                 \
  _class::dagger() const noexcept                                       \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

#endif // QGATE_TERNARY_H
