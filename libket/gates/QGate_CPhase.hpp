/** @file libket/gates/QGate_CPhase.hpp

    @brief C++ API quantum CPHASE (controlled phase shift) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup cphase CPhase gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_CPHASE_HPP
#define QGATE_CPHASE_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QCPhasedag;
  
/**
@brief CPHASE (controlled phase shift) gate class

The CPHASE (controlled phase shift) gate class implements the
quantum controlled phase shift gate for an arbitrary number of quantum
bits

The CPHASE (controlled phase shift) gate is a two-qubit operation,
where the first qubit is usually referred to as the control qubit and
the second qubit as the target qubit. It maps the basis state
\f$\left|00\right>\f$ to \f$\left|00\right>\f$, \f$\left|01\right>\f$
to \f$\left|01\right>\f$, \f$\left|10\right>\f$ to
\f$\left|10\right>\f$ and \f$\left|11\right>\f$ to
\f$\exp(i\theta)\left|11\right>\f$.

The CPHASE (controlled phase shift) gates leaves the control qubit
unchanged and performs a Phase gate on the target qubit only when the
control qubit is in state \f$\left|1\right>\f$.

The unitary matrix reads

\f[
\text{CPHASE} =
\begin{pmatrix}
1 & 0 & 0 & 0\\                                          \
0 & 1 & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & \exp(i\theta)
\end{pmatrix}
\f]

@ingroup cphase
*/
template<typename _angle, typename _tol = QConst_M_ZERO_t>
class QCPhase : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  BINARY_GATE_DEFAULT_DECL_AT(QCPhase, QCPhasedag);
  
  ///@{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CTRL(PH[" + _angle::to_string() + "]) q[" +
                           utils::to_string(std::get<0>(i)) + "],q[" +
                           utils::to_string(std::get<1>(i)) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cirq.CZPowGate(exponent=" + _angle::to_string() +
                           ").on(q[" + utils::to_string(std::get<0>(i)) +
                           "],q[" + utils::to_string(std::get<1>(i)) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "cr q[";
      for (auto i : _filter0::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(expr).end() - 1) ? "," : "], ");
      _expr += _angle::to_string() + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cu1(" + _angle::to_string(true) + ") q[" +
                           utils::to_string(std::get<0>(i)) + "], q[" +
                           utils::to_string(std::get<1>(i)) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    std::cerr << "The CR/CPHASE gate is not implemented for OpenQL!!!\n";
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel([&]() {
          expr.kernel().controlled_rz(
            std::get<0>(i), std::get<1>(i), _angle::value());
        });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("\tcphase q" + utils::to_string(std::get<0>(i)) +
                           ",q" + utils::to_string(std::get<1>(i)) + " # " +
                           _angle::to_string() + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CPHASE (" + _angle::to_string(true) + ") " +
                           utils::to_string(std::get<0>(i)) + " " +
                           utils::to_string(std::get<1>(i)) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        quest::controlledPhaseShift(
          expr.reg(), std::get<0>(i), std::get<1>(i), (qreal)_angle::value());
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CPHASE gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel(new qx::ctrl_phase_shift(
          std::get<0>(i), std::get<1>(i), (double)_angle::value()));
    }
    return expr;
  }
#endif
  ///@}
};

/**
@brief CPHASE (controlled phase shift) gate creator
@ingroup cphase

This overload of the LibKet::gates::cphase() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::cphase();
\endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _angle>
inline constexpr auto cphase(_angle) noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCPhase<_angle, _tol>>(
    filters::QFilter{}, filters::QFilter{});
}

namespace detail {
/// @defgroup cphase_aliases Aliases
/// @ingroup cphase
///@{
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename _expr1,
         typename = void>
struct cphase_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QCPhase<_angle, _tol>,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0_0,
         typename __expr0_1,
         typename __filter0,
         typename __expr1_0,
         typename __expr1_1,
         typename __filter1>
struct cphase_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0_0, __expr0_1, QCPhase<_angle, _tol>, __filter0>,
  BinaryQGate<__expr1_0, __expr1_1, QCPhase<_angle, _tol>, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0_0, __expr0_1, QCPhase<_angle, _tol>, __filter0>&
      expr0,
    const BinaryQGate<__expr1_0, __expr1_1, QCPhase<_angle, _tol>, __filter1>&
      expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return cphase_impl<_tol,
                       _angle,
                       decltype(typename filters::getFilter<__filter0>::type{}(
                         all(expr1.expr1(all(
                           expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
                       typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return cphase_impl<
      _tol,
      _angle,
      typename filters::getFilter<__filter0>::type,
      decltype(typename filters::getFilter<__filter1>::type{}(all(
        expr0.expr0(all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphase_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphase_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphase_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphase_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphase_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphase_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphase_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphase_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphase_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCPhase here */
    !std::is_same<typename gates::getGate<_expr0>::type, QCPhase<_angle,_tol> >::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphase_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct cphase_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CPhase gate is handled explicitly */
    (!std::is_base_of<
      QCPhase<_angle, _tol>,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphase_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCPhase here */
    !std::is_same<typename gates::getGate<_expr1>::type,
                  QCPhase<_angle, _tol>>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphase_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief CPHASE (controlled phase shift) creator

   This overload of the LibKet::gates::cphase() function
   eliminates the double-application of the CPhase gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct cphase_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CPhase gate is handled explicitly */
    (!std::is_base_of<
      QCPhase<_angle, _tol>,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhase<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhase gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief CPHASE (controlled phase shift) creator

This overload of the LibKet::gates::cphase() function accepts
two expressions as constant reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphase(_angle, const _expr0& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::cphase_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief CPHASE (controlled phase shift) creator

This overload of the LibKet::gates::cphase() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphase(_angle, const _expr0& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::cphase_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief CPHASE (controlled phase shift) creator

This overload of the LibKet::gates::cphase() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphase(_angle, _expr0&& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::cphase_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief CPHASE (controlled phase shift) creator

This overload of the LibKet::gates::cphase() function accepts
two expression as universal reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphase(_angle, _expr0&& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::cphase_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief CPHASE (controlled phase shift) gate creator

Function alias for LibKet::gates::cphase()
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
CPHASE(Args&&... args)
{
  return cphase<_tol>(std::forward<Args>(args)...);
}

/**
@brief CPHASE (controlled phase shift) gate creator

Function alias for LibKet::gates::cphase()
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
cr(Args&&... args)
{
  return cphase<_tol>(std::forward<Args>(args)...);
}

/**
@brief CPHASE (controlled phase shift) gate creator

Function alias for LibKet::gates::cphase()
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
CR(Args&&... args)
{
  return cphase<_tol>(std::forward<Args>(args)...);
}

///@}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CPHASE_HPP
