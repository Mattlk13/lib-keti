/** @file libket/QExpression.hpp

    @brief C++ API quantum expression classes

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup cxx_api
 */

#pragma once
#ifndef QEXPRESSION_HPP
#define QEXPRESSION_HPP

#include <cstdio>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QUtils.hpp>

namespace LibKet {

/**
   @brief Quantum expression class

   The quantum expression class provides functionality to compose
   quantum expression from gates and circuits and to forther
   manipulate these quantum expression
*/
template<std::size_t _qubits, QBackendType _qbackend>
class QExpression;

#ifdef LIBKET_WITH_AQASM
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::AQASM backend

   This specialization of the quantum expression class implements the
   quantum expression class for the AQASM backend.

   @ingroup AQASM
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::AQASM> : public QBase
{
public:
  /// Default constructor
  QExpression() = default;

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&) = default;

  /// Constructor: More from universal reference
  QExpression(QExpression&&) = default;

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::AQASM> inline constexpr operator+(
    QExpression<__qubits, QBackendType::AQASM>) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::AQASM>();
  }

  /// Dump QExpression object to string
  const std::string to_string() const noexcept { return _kernel + "END\n"; }

  /// Append quantum gate expression to kernel
  QExpression& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_CIRQ
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::Cirq backend

   This specialization of the quantum expression class implements the
   quantum expression class for the Cirq backend.

   @ingroup CIRQ
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::Cirq> : public QBase
{
public:
  /// Default constructor
  QExpression() = default;

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&) = default;

  /// Constructor: More from universal reference
  QExpression(QExpression&&) = default;

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::Cirq> inline constexpr operator+(
    QExpression<__qubits, QBackendType::Cirq>) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::Cirq>();
  }

  /// Dump QExpression object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QExpression& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_CQASM
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::cQASMv1 backend

   This specialization of the quantum expression class implements the
   quantum expression class for the cQASM v1.0 backend.

   @ingroup CQASM
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::cQASMv1> : public QBase
{
public:
  /// Default constructor
  QExpression() = default;

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&) = default;

  /// Constructor: More from universal reference
  QExpression(QExpression&&) = default;

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::cQASMv1> inline constexpr operator+(
    QExpression<__qubits, QBackendType::cQASMv1>) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::cQASMv1>();
  }

  /// Dump QExpression object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QExpression& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_OPENQASM
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::OpenQASM2 backend

   This specialization of the quantum expression class implements the
   quantum expression class for the OpenQASM v2.0 backend.

   @ingroup OPENQASM
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::OpenQASMv2> : public QBase
{
public:
  /// Default constructor
  QExpression() = default;

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&) = default;

  /// Constructor: More from universal reference
  QExpression(QExpression&&) = default;

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::OpenQASMv2> inline constexpr
  operator+(QExpression<__qubits, QBackendType::OpenQASMv2>) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::OpenQASMv2>();
  }

  /// Dump QExpression object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QExpression& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_OPENQL
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::OpenQL backend

   This specialization of the quantum expression class implements the
   quantum expression class for the OpenQL backend.

   @ingroup OPENQL
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::OpenQL> : public QBase
{
public:
  /// Default constructor
  QExpression(const std::string& config_file = LIBKET_BINARY
        "test_config_default.json")
    : _platform("libket_platform", config_file)
    , _kernel("libket_kernel", _platform, _qubits, _qubits)
  {
#ifndef NDEBUG
    _platform.print_info();
    ql::options::set("log_level", "LOG_DEBUG");
#else
    ql::options::set("log_level", "LOG_NOTHING");
#endif
  }

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&)
    : QExpression()
  {}

  /// Constructor: More from universal reference
  QExpression(QExpression&&)
    : QExpression()
  {}

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::OpenQL> inline constexpr operator+(
    QExpression<__qubits, QBackendType::OpenQL>) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::OpenQL>();
  }

  /// Dump QExpression object to string
  std::string to_string() const noexcept
  {
    return const_cast<ql::quantum_kernel&>(_kernel).qasm();
  }

  /// Append quantum gate expression to kernel
  QExpression& append_kernel(std::function<void()> append_gate)
  {
    append_gate();
    return *this;
  }

  /// Get constant reference to kernel
  const ql::quantum_kernel& kernel() const { return _kernel; }

  /// Get reference to kernel
  ql::quantum_kernel& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.get_circuit().clear(); }

  /// Get constant reference to platform
  const ql::quantum_platform& platform() const { return _platform; }

  /// Get reference to platform
  ql::quantum_platform& platform() { return _platform; }

protected:
  ql::quantum_platform _platform;
  ql::quantum_kernel _kernel;
};
#endif

#ifdef LIBKET_WITH_QASM
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::QASM backend

   This specialization of the quantum expression class implements the
   quantum expression class for the QASM backend.

   @ingroup QASM
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::QASM> : public QBase
{
public:
  /// Default constructor
  QExpression() = default;

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&) = default;

  /// Constructor: More from universal reference
  QExpression(QExpression&&) = default;

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::QASM> inline constexpr operator+(
    QExpression<__qubits, QBackendType::QASM> other) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::QASM>();
    ;
  }

  /// Dump QExpression object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QExpression& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_QUIL
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::Quil backend

   This specialization of the quantum expression class implements the
   quantum expression class for the Quil backend.

   @ingroup QUIL
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::Quil> : public QBase
{
public:
  /// Default constructor
  QExpression() = default;

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&) = default;

  /// Constructor: More from universal reference
  QExpression(QExpression&&) = default;

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::Quil> inline constexpr operator+(
    QExpression<__qubits, QBackendType::Quil>) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::Quil>();
  }

  /// Dump QExpression object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QExpression& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_QUEST
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::QuEST backend

   This specialization of the quantum expression class implements the
   quantum expression class for the QuEST simulator backend.

   @ingroup QUEST
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::QuEST> : public QBase
{
public:
  /// Default constructor
  QExpression()
    : _creg{ 0 }
  {
    _env = quest::createQuESTEnv();
    _reg = quest::createQureg(_qubits, _env);
#ifndef NDEBUG
    quest::reportQuESTEnv(_env);
    quest::reportQuregParams(_reg);
#endif
    quest::startRecordingQASM(_reg);
  }

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&)
    : QExpression()
  {}

  /// Constructor: More from universal reference
  QExpression(QExpression&&)
    : QExpression()
  {}

  /// Destructor
  ~QExpression()
  {
    quest::stopRecordingQASM(_reg);
    quest::destroyQureg(_reg, _env);
    quest::destroyQuESTEnv(_env);
  }

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::QuEST> inline constexpr operator+(
    QExpression<__qubits, QBackendType::QuEST>) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::QuEST>();
  }

  /// Dump QExpression object to string
  std::string to_string() const noexcept
  {
    // QuEST does not provide functionality to dump the circuit to a
    // string or file. Instead, it can record all actions and dump the
    // generated OpenQASM kernel code to the standard output so that
    // the output stream needs to be captured temporarily.
    std::stringstream buffer;
    std::streambuf* sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());
    quest::printRecordedQASM(_reg);
    std::cout.rdbuf(sbuf);

    quest::reportState(_reg);

    return buffer.str();
  }

  /// Clear kernel
  void clear()
  {
    quest::clearRecordedQASM(_reg);
    quest::initZeroState(_reg);
    _creg = 0;
  }

  /// Get constant reference to quantum register
  const quest::Qureg& reg() const { return _reg; }

  /// Get reference to quantum register
  quest::Qureg& reg() { return _reg; }

  /// Get constant reference to classical register
  const QBitArray<_qubits>& creg() const { return _creg; }

  /// Get reference to classical register
  QBitArray<_qubits>& creg() { return _creg; }

  /// Reset quantum register
  void reset()
  {
    quest::initZeroState(_reg);
    _creg = 0;
  }

private:
  quest::QuESTEnv _env;
  quest::Qureg _reg;
  QBitArray<_qubits> _creg;
};

/**
 @brief Serialize operation
*/
std::ostream&
operator<<(std::ostream& os, const quest::Qureg& reg)
{
  if (reg.isDensityMatrix) {
    os << "--------------[density matrix]-------------\n";
    os << "Not implemented yet\n";
    os << "-------------------------------------------";
  } else {
    os << "--------------[quantum state]--------------\n";
    os << std::fixed << std::setprecision(8) << std::showpos;
    for (std::size_t i = 0; i < (2 << (reg.numQubitsRepresented - 1)); ++i)
      os << std::setw(6) << "(" << reg.stateVec.real[i] << ","
         << reg.stateVec.imag[i] << ") |"
         << utils::to_binary(i, reg.numQubitsRepresented) << "> +\n";
    os << std::defaultfloat << std::noshowpos
       << "-------------------------------------------";
  }
  return os;
}
#endif

#ifdef LIBKET_WITH_QX
/**
   @brief Quantum expression class specialization for the LibKet::QBackendType::QX backend

   This specialization of the quantum expression class implements the
   quantum expression class for the QX simulator backend.

   @ingroup QX
*/
template<std::size_t _qubits>
class QExpression<_qubits, QBackendType::QX> : public QBase
{
public:
  /// Default constructor
  QExpression()
    : _circuit(_qubits)
    , _reg(_qubits)
  {}

  /// Constructor: Copy from constant reference
  QExpression(const QExpression&)
    : QExpression()
  {}

  /// Constructor: More from universal reference
  QExpression(QExpression&&)
    : QExpression()
  {}

  /// Destructor
  ~QExpression()
  {}

  /// Copy assignment operator
  QExpression& operator=(const QExpression&) = default;

  /// Move assignment operator
  QExpression& operator=(QExpression&&) = default;

  /// Operator+: Concatenate two QExpression objects
  template<std::size_t __qubits>
  QExpression<_qubits + __qubits, QBackendType::QX> inline constexpr operator+(
    QExpression<__qubits, QBackendType::QX>) const noexcept
  {
    return QExpression<_qubits + __qubits, QBackendType::QX>();
  }

  /// Dump QExpression object to string
  std::string to_string() const noexcept
  {
    // QX, unfortunately, does not define output routines as const,
    // which makes the use of const_cast necessary. Moreover, the
    // dump() method prints its output to std::cout so that the
    // output stream needs to be captured temporarily
    std::stringstream buffer;
    std::streambuf* sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());
    const_cast<qx::circuit&>(_circuit).dump();
    std::cout.rdbuf(sbuf);

    return buffer.str();
  }

  /// Append quantum gate expression to kernel
  QExpression& append_kernel(qx::gate* gate)
  {
    _circuit.add(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const qx::circuit& kernel() const { return _circuit; }

  /// Get reference to kernel
  qx::circuit& kernel() { return _circuit; }

  /// Clear kernel
  void clear() { _circuit.clear(); }

  /// Get constant reference to circuit
  const qx::circuit& circuit() const { return _circuit; }

  /// Get reference to circuit
  qx::circuit& circuit() { return _circuit; }

  /// Get constant reference to quantum register
  const qx::qu_register& reg() const { return _reg; }

  /// Get reference to circuit
  qx::qu_register& reg() { return _reg; }

  /// Reset quantum register
  void reset() { _reg.reset(); }

private:
  qx::circuit _circuit;
  qx::qu_register _reg;
};
#endif

/**
   @brief Serialize operation   
*/
template<std::size_t _qubits, QBackendType _qbackend>
std::ostream&
operator<<(std::ostream& os, const QExpression<_qubits, _qbackend>& expr)
{
  os << expr.to_string();
  return os;
}

/**
   @brief Prints the abstract syntax tree

   @note  specialization for QExpression objects
*/
template<std::size_t level = 1, std::size_t _qubits, QBackendType _qbackend>
inline static auto
show(const QExpression<_qubits, _qbackend>& expr,
     std::ostream& os = std::cout,
     const std::string& prefix = "")
{
  os << "QExpression\n";
  if (level > 0) {
    os << prefix << "|   qubits = " << utils::to_string(_qubits) << std::endl;
    os << prefix << "| backend = " << utils::to_string((int)_qbackend)
       << std::endl;
  }

  return expr;
}

/**
   @brief Prints the abstract syntax tree (DOT-language)

   @note  specialization for QExpression objects
*/
template<std::size_t level = 1, std::size_t _qubits, QBackendType _qbackend>
inline static auto
dot(const QExpression<_qubits, _qbackend>& expr,
    std::ostream& os = std::cout)
{
  os << "QExpression\n";
  
  return expr;
}

/**
   @brief Returns the adjoint expression

   @note  specialization for QExpression objects (dummy operation)
*/
template<std::size_t _qubits, QBackendType _qbackend>
inline static auto
dagger(const QExpression<_qubits, _qbackend>& expr)
{  
  return expr;
}

} // namespace LibKet

#endif // QEXPRESSION_HPP
