/** @file libket/QBase.hpp

@brief C++ API quantum base classes and declarations

@copyright This file is part of the LibKet library (C++ API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller

@ingroup cxx_api
*/

#pragma once
#ifndef QBASE_HPP
#define QBASE_HPP

#include <iostream>
#include <map>

#define _USE_MATH_DEFINES
#include <math.h>

#ifdef __INTEL_COMPILER
#define ARMA_ALLOW_FAKE_GCC
#endif
#include <armadillo>
#define OPTIM_ENABLE_ARMA_WRAPPERS
#include <optim.hpp>

#include <json/json.hpp>

#include <QConfig.h>
#include <QDebug.hpp>

#ifdef LIBKET_WITH_OPENQL
#define NLOHMANN_JSON_HPP
#ifndef DOXYGEN
#include <openql.h>
#endif
#ifdef print
#undef print
#endif
#ifdef println
#undef println
#endif
#endif

#ifdef LIBKET_WITH_QUEST
#ifndef DOXYGEN
namespace quest {
#endif
#include <QuEST.h>
#ifndef DOXYGEN
}
#endif
#endif

#ifdef LIBKET_WITH_QX
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated"
#include <qx/core/circuit.h>
#include <qx/core/error_model.h>
#pragma GCC diagnostic pop

#ifdef print
#undef print
#endif
#ifdef println
#undef println
#endif
#endif

/**
   @namespace LibKet

   @brief
   The LibKet namespace, containing all functionality of the LibKet project

   The LibKet namespace contains all functionality of the LibKet
   project that is exposed to the end-user. All functionality in this
   namespace has a stable API over future LibKet releases.
 */
namespace LibKet {

  ///@defgroup backends Quantum backends
  ///@ingroup  cxx_api
  
/**
   @brief Quantum backends

   The Quantum backend enumerator defines the supported quantum backends
*/
enum class QBackendType
{
#ifdef LIBKET_WITH_AQASM
  /**
     @brief Atos Quantum Assembly Language

     @defgroup AQASM Atos QASM
     
     @ingroup  backends
*/
  AQASM,
#endif

#ifdef LIBKET_WITH_CIRQ
  /**
     @brief    Google Cirq
     
     @defgroup CIRQ Cirq
     
     @ingroup  backends
   */
  Cirq,
#endif

#ifdef LIBKET_WITH_CQASM
  /**
 @brief Common Quantum Assembly Language v1.0

 The common Quantum Assembly Language (cQASM) v1.0 is defined
 in the document https://arxiv.org/abs/1805.09607 by
 N. Khammassi, G.G. Guerreschi, I. Ashraf, J. W. Hogaboam,
 C. G. Almudever, K. Bertels

 @defgroup CQASM CommonQASM v1.0

 @ingroup  backends
*/
  cQASMv1,
#endif

#ifdef LIBKET_WITH_OPENQASM
  /**
 @brief Open Quantum Assembly Language v2.0

 The Open Quantum Assembly Language (openQASM) v2.0 is defined
 in the document https://arxiv.org/abs/1707.03429 by Andrew
 W. Cross, Lev S. Bishop, John A. Smolin, Jay M. Gambetta

 @defgroup OPENQASM OpenQASM v2.0

 @ingroup  backends
*/
  OpenQASMv2,
#endif

#ifdef LIBKET_WITH_OPENQL
  /**
 @brief OpenQL Framework

 OpenQL is a framework for high-level quantum programming in
 C++/Python. The framework provides a compiler for compiling
 and optimizing quantum code. The compiler produces the
 intermediate quantum assembly language and the compiled
 micro-code for various target platforms. While the microcode
 is platform-specific, the quantum assembly code (qasm) is
 hardware-agnostic and can be simulated on the QX simulator.

 The OpenQL Framework is developed at the Quantum Engineering
 Lab at TU Delft. The project can be found online at
 https://github.com/QE-Lab/OpenQL

 @defgroup OPENQL OpenQL

 @ingroup  backends
*/
  OpenQL,
#endif

#ifdef LIBKET_WITH_QASM
  /**
 @brief QASM for the quantum circuit viewer qasm2circ

 QASM is a simple text-format language for describing acyclic
 quantum circuits composed from single qubit and multiply
 controlled single-qubit gates.

 qasm2circ is a package which converts a QASM file into a
 graphical depiction of the quantum circuit, using standard
 quantum gate symbols (and other user-defined symbols).  This
 is done using latex (specifically, xypic), to produce
 high-quality output in epsf, pdf, or png formats.

 Figures of quantum circuits in the book "Quantum Computation
 and Quantum Information," by Nielsen and Chuang, were produced
 using an earlier version of this package.

 The qasm2circ package is developed by I. Chuang
 <ichuang@mit.edu> and can be obtained from
 https://www.media.mit.edu/quanta/qasm2circ

 @defgroup QASM QASM

 @ingroup  backends
*/
  QASM,
#endif

#ifdef LIBKET_WITH_QUIL
  /**
 @brief Rigetti's Quantum Instruction Language

 Rigetti's Quantum Instruction Language (Quil) is defined in
 the document https://arxiv.org/abs/1608.03355 by Robert
 S. Smith, Michael J. Curtis, William J. Zeng

 @defgroup QUIL Quil

 @ingroup  backends
*/
  Quil,
#endif

#ifdef LIBKET_WITH_QUEST
  /**
 @brief Quantum Exact Simulation Toolkit

 The Quantum Exact Simulation Toolkit is a high performance simulator
 of universal quantum circuits, state-vectors and density
 matrices. The project can be found online at
 https://github.com/QuEST-Kit/QuEST and is described in the document
 https://arxiv.org/abs/1802.08032 by T. Jones, A. Brown, I. Bush, and
 S. Benjamin

 @defgroup QUEST QuEST simulator

 @ingroup  backends
*/
  QuEST,
#endif

#ifdef LIBKET_WITH_QX
  /**
 @brief QX-simulator

 QX is a quantum simulator that is developed at the Quantum
 Engineering Lab at TU Delft. The project can be found online
 at https://github.com/QE-Lab/qx-simulator

 @defgroup QX QX simulator

 @ingroup  backends
*/
  QX,
#endif
};

/**
   @brief Quantum backend lookup table

   The Quantum backend lookup table defines the backend names as strings
 */
std::map<QBackendType, std::string> QBackendTypeMap = {
#ifdef LIBKET_WITH_AQASM
  { QBackendType::AQASM, "QBackendType::AQASM" },
#endif
#ifdef LIBKET_WITH_CQASM
  { QBackendType::cQASMv1, "QBackendType::cQASMv1" },
#endif
#ifdef LIBKET_WITH_CIRQ
  { QBackendType::Cirq, "QBackendType::Cirq" },
#endif
#ifdef LIBKET_WITH_OPENQASM
  { QBackendType::OpenQASMv2, "QBackendType::OpenQASMv2" },
#endif
#ifdef LIBKET_WITH_OPENQL
  { QBackendType::OpenQL, "QBackendType::OpenQL" },
#endif
#ifdef LIBKET_WITH_QASM
  { QBackendType::QASM, "QBackendType::QASM" },
#endif
#ifdef LIBKET_WITH_QUIL
  { QBackendType::Quil, "QBackendType::Quil" },
#endif
#ifdef LIBKET_WITH_QUEST
  { QBackendType::QuEST, "QBackendType::QuEST" },
#endif
#ifdef LIBKET_WITH_QX
  { QBackendType::QX, "QBackendType::QX" }
#endif
};

/**
   @brief Endianness

   The LibKet endianness enumerator defines the supported endiannesses
*/
enum class QEndianness
{
  lsb /**< Least significant bit (LSB) */,
  msb /**< Most significant bit  (MSB) */
};

/**
   @brief Quantum base class

   The Quantum base class is the base class of all LibKet classes.
*/
class QBase
{};

/**
   @brief Stream manipulator

   The LibKet stream manipulator
*/

inline int get_iomanip() { 
  static int i = std::ios_base::xalloc();
  return i;
}

std::ostream& set(std::ostream& os) { os.iword(get_iomanip()) = 1; return os; } 
std::ostream& unset(std::ostream& os) { os.iword(get_iomanip()) = 0; return os; }

bool is_set(std::ostream& os)   { return os.iword(get_iomanip()) != 0; }

} // namespace LibKet

#endif // QBASE_HPP
