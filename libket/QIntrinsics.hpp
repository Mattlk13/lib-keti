/** @file libket/QIntrinsics.hpp

    @brief C++ API intrinsics header file

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup intrinsics Quantum intrinsics
    
    @ingroup cxx_api
 */
#pragma once
#ifndef INTRINSICS_HPP
#define INTRINSICS_HPP

#include <intrinsics/QIntrinsic.hpp>
#include <intrinsics/QIntrinsic_Int.hpp>
#include <intrinsics/QIntrinsic_Posit.hpp>

#endif // INTRINSICS_HPP
