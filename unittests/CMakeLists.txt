########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
########################################################################

if (CMAKE_VERSION VERSION_LESS 3.2)
  set(UPDATE_DISCONNECTED_IF_AVAILABLE "")
else()
  set(UPDATE_DISCONNECTED_IF_AVAILABLE "UPDATE_DISCONNECTED 1")
endif()

########################################################################
# Option list
########################################################################
option(LIBKET_BUILD_UNITTESTS_BACKEND "Build backend unittests" OFF)

########################################################################
# Generate tests
########################################################################

# Define list of tests
if(NOT LIBKET_BUILD_UNITTESTS_TESTS STREQUAL "ALL")
  unset(LIBKET_UNITTESTS_HEADERS)
  foreach(TEST ${LIBKET_BUILD_UNITTESTS_TESTS})
    list(APPEND LIBKET_UNITTESTS_HEADERS "test_${TEST}.hpp")
  endforeach()
endif()

# Configure tests
set(LIBKET_BUILD_UNITTESTS_TESTS "ALL" CACHE STRING "Build unittests for specified tests")
set_property(CACHE LIBKET_BUILD_UNITTESTS_TESTS PROPERTY STRINGS ALL)

# List of tests to be included
if (NOT DEFINED LIBKET_UNITTESTS_TESTS_INCLUDE)
  set(LIBKET_UNITTESTS_TESTS_INCLUDE "test_*")
endif()

# List of tests to be excluded
if (NOT DEFINED LIBKET_UNITTESTS_TESTS_EXCLUDE)
  set(LIBKET_UNITTESTS_TESTS_EXCLUDE "")
endif()

# Generate list of included header files
foreach (LIST_ITEM ${LIBKET_UNITTESTS_TESTS_INCLUDE})
  # Remove suffix if present
  string(REPLACE ".hpp"  "" LIST_ITEM ${LIST_ITEM})
  # Collect all header files matching the given pattern
  file(GLOB HEADERS RELATIVE
    ${CMAKE_CURRENT_SOURCE_DIR}/src/
    ${CMAKE_CURRENT_SOURCE_DIR}/src/${LIST_ITEM}.hpp)
  if (NOT HEADERS STREQUAL "")
    list(APPEND LIBKET_UNITTESTS_HEADERS ${HEADERS})
  endif()
endforeach()

# Generate list of excluded header files
foreach (LIST_ITEM ${LIBKET_UNITTESTS_TESTS_EXCLUDE})
  # Remove suffix if present
  string(REPLACE ".hpp"  "" LIST_ITEM ${LIST_ITEM})
  # Collect all header files matching the given pattern
  file(GLOB HEADERS RELATIVE
    ${CMAKE_CURRENT_SOURCE_DIR}/src/
    ${CMAKE_CURRENT_SOURCE_DIR}/src/${LIST_ITEM}.hpp)
  if (NOT HEADERS STREQUAL "")
    list(REMOVE_ITEM LIBKET_UNITTESTS_HEADERS ${HEADERS})
  endif()
endforeach()

# Extract test names from header file names
foreach(TEST_HEADER ${LIBKET_UNITTESTS_HEADERS})
  string(REPLACE "test_" "" TEST_NAME ${TEST_HEADER})
  string(REPLACE ".hpp"  "" TEST_NAME ${TEST_NAME})
  set_property(CACHE LIBKET_BUILD_UNITTESTS_TESTS APPEND PROPERTY STRINGS ${TEST_NAME})
endforeach()

########################################################################
# Summary
########################################################################
message("")
message("UnitTests:")
message("LIBKET_BUILD_UNITTESTS_TESTS.......: ${LIBKET_BUILD_UNITTESTS_TESTS}")
message("LIBKET_BUILD_UNITTESTS_BACKEND.....: ${LIBKET_BUILD_UNITTESTS_BACKEND}")
message("")

########################################################################
# Generate configuration
########################################################################
configure_file("${CMAKE_CURRENT_SOURCE_DIR}/src/test_config.h.in"
               "${CMAKE_CURRENT_BINARY_DIR}/src/test_config.h")
include_directories(${CMAKE_CURRENT_BINARY_DIR}/src)

########################################################################
# Generate test suites and tests
########################################################################

# Define list of tests
if(NOT LIBKET_BUILD_UNITTESTS_TESTS STREQUAL "ALL")
  unset(LIBKET_UNITTESTS_HEADERS)
  foreach(TEST ${LIBKET_BUILD_UNITTESTS_TESTS})
    list(APPEND LIBKET_UNITTESTS_HEADERS "test_${TEST}.hpp")
  endforeach()
endif()

########################################################################
# UnitTest++
########################################################################
include(UnitTest++)

########################################################################
# Add src directory
########################################################################
include_directories(src)
file (GLOB_RECURSE LIBKET_HEADERS
  ${PROJECT_SOURCE_DIR}/src/*.h
  ${PROJECT_SOURCE_DIR}/src/*.hpp
  ${PROJECT_SOURCE_DIR}/src/detail/*.hpp)
source_group("Headers" FILES ${LIBKET_HEADERS})

########################################################################
# Add subdirectories
########################################################################

# AQASM tests
if(LIBKET_WITH_AQASM)
  add_subdirectory(AQASM)
endif()

# Cirq tests
if(LIBKET_WITH_CIRQ)
  add_subdirectory(Cirq)
endif()

# cQASM tests
if(LIBKET_WITH_CQASM)
  add_subdirectory(cQASM)
endif()

# OpenQASM tests
if(LIBKET_WITH_OPENQASM)
  add_subdirectory(OpenQASM)
endif()

# OpenQL tests
if(LIBKET_WITH_OPENQL)
  add_subdirectory(OpenQL)
endif()

# QASM tests
if(LIBKET_WITH_QASM)
  add_subdirectory(QASM)
endif()

# QuEST tests
if(LIBKET_WITH_QUEST)
  add_subdirectory(QuEST)
endif()

# Quil tests
if(LIBKET_WITH_QUIL)
  add_subdirectory(Quil)
endif()

# QX tests
if(LIBKET_WITH_QX)
  add_subdirectory(QX)
endif()
