/** @file test_gate_rotate_z.hpp
 *
 *  @brief LibKet::gates::rotate_z() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_z)
{
    TEST_API_CONSISTENCY( rotate_z<>, QConst(3.141) );
  TEST_API_CONSISTENCY( ROTATE_Z<>, QConst(3.141) );
  TEST_API_CONSISTENCY( rz<>,       QConst(3.141) );
  TEST_API_CONSISTENCY( RZ<>,       QConst(3.141) );
  TEST_API_CONSISTENCY( Rz<>,       QConst(3.141) );

  TEST_API_CONSISTENCY( QRotate_Z<QConst_t(3.141)>() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_z<>,    QConst(3.141),
                                        "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_z<>,    QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Z<>,    QConst(3.141),
                                        "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Z<>,    QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     rz<>,          QConst(3.141),
                                        "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    rz<>,          QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     RZ<>,          QConst(3.141),
                                        "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    RZ<>,          QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     Rz<>,          QConst(3.141),
                                        "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    Rz<>,          QConst(-3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_z<>,    QConst(3.141),
                                        "QRotate_Zdag<" QCONST_T "(3.141),QConst1_t(0)>",  rotate_zdag<>, QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Z<>,    QConst(3.141),
                                        "QRotate_Zdag<" QCONST_T "(3.141),QConst1_t(0)>",  ROTATE_Zdag<>, QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     rz<>,          QConst(3.141),
                                        "QRotate_Zdag<" QCONST_T "(3.141),QConst1_t(0)>",  rzdag<>,       QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     RZ<>,          QConst(3.141),
                                        "QRotate_Zdag<" QCONST_T "(3.141),QConst1_t(0)>",  RZdag<>,       QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     Rz<>,          QConst(3.141),
                                        "QRotate_Zdag<" QCONST_T "(3.141),QConst1_t(0)>",  Rzdag<>,       QConst(3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_z<>,    QConst(-3.141),
                                        "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_z<>,    QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Z<>,    QConst(-3.141),
                                        "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Z<>,    QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    rz<>,          QConst(-3.141),
                                        "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     rz<>,          QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    RZ<>,          QConst(-3.141),
                                        "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     RZ<>,          QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    Rz<>,          QConst(-3.141),
                                        "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",     Rz<>,          QConst(3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_z<>,    QConst(-3.141),
                                        "QRotate_Zdag<" QCONST_T "(-3.141),QConst1_t(0)>", rotate_zdag<>, QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Z<>,    QConst(-3.141),
                                        "QRotate_Zdag<" QCONST_T "(-3.141),QConst1_t(0)>", ROTATE_Zdag<>, QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    rz<>,          QConst(-3.141),
                                        "QRotate_Zdag<" QCONST_T "(-3.141),QConst1_t(0)>", rzdag<>,       QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    RZ<>,          QConst(-3.141),
                                        "QRotate_Zdag<" QCONST_T "(-3.141),QConst1_t(0)>", RZdag<>,       QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>",    Rz<>,          QConst(-3.141),
                                        "QRotate_Zdag<" QCONST_T "(-3.141),QConst1_t(0)>", Rzdag<>,       QConst(-3.141) );

  //TEST_UNARY_GATE( "QRotate_Z<" QCONST_T "(3.141),QConst1_t(0)>",  QRotate_Z<QConst_t(3.141)>() );
  //TEST_UNARY_GATE( "QRotate_Z<" QCONST_T "(-3.141),QConst1_t(0)>", QRotate_Z<QConst_t(-3.141)>() );
}
