/** @file COMMON.h

    @brief Unittests configuration

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef COMMON_H
#define COMMON_H

/**
   @brief Macro creates unit tests for API consistency

   These tests check that the expression supports the common
   functionality of all LibKet expressions, i.e.

   - show(expr), expr.show()
   - dot(expr), expr.dot()

   @param _expr expression object to test
   @param _args optional parameter objects, e.g., rotation angles
*/
#define TEST_API_CONSISTENCY(_expr, _args...)      \
  try {                                            \
    /** show() and expr.show() **/                 \
    std::stringstream ss;                          \
    auto expr = _expr(_args);                      \
    show<99>(expr, ss);                            \
    std::string str;                               \
    str = expr.show<99>();                         \
    CHECK_EQUAL(ss.str(), str);                    \
  } catch (const std::exception& e) {              \
    std::cerr << e.what() << std::endl;            \
    return;                                        \
  }                                                \
                                                   \
  try {                                            \
    /** dot() and expr.dot() **/                   \
    std::stringstream ss;                          \
    auto expr = _expr(_args);                      \
    dot<99>(expr, ss);                             \
    std::string str;                               \
    str = expr.dot<99>();                          \
    CHECK_EQUAL(ss.str(), str);                    \
  } catch (const std::exception& e) {              \
    std::cerr << e.what() << std::endl;            \
    return;                                        \
  }                                                \

#endif // COMMON_H
