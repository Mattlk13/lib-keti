/** @file test_gate_rotate_my90.hpp
 *
 *  @brief LibKet::gates::rotate_my90() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_my90)
{
  TEST_API_CONSISTENCY( rotate_my90  );
  TEST_API_CONSISTENCY( ROTATE_MY90  );
  TEST_API_CONSISTENCY( rmy90        );
  TEST_API_CONSISTENCY( RMY90        );
  TEST_API_CONSISTENCY( Rmy90        );
  TEST_API_CONSISTENCY( my90         );
  TEST_API_CONSISTENCY( MY90         );

  TEST_API_CONSISTENCY( rotate_my90dag );
  TEST_API_CONSISTENCY( ROTATE_MY90dag );
  TEST_API_CONSISTENCY( rmy90dag       );
  TEST_API_CONSISTENCY( RMY90dag       );
  TEST_API_CONSISTENCY( Rmy90dag       );
  TEST_API_CONSISTENCY( my90dag        );
  TEST_API_CONSISTENCY( MY90dag        );
  
  TEST_API_CONSISTENCY( QRotate_MY90() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", rotate_my90, "QRotate_Y90", rotate_my90dag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", ROTATE_MY90, "QRotate_Y90", ROTATE_MY90dag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", rmy90,       "QRotate_Y90", rmy90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", RMY90,       "QRotate_Y90", RMY90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", Rmy90,       "QRotate_Y90", Rmy90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", my90,        "QRotate_Y90", my90dag        );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", MY90,        "QRotate_Y90", MY90dag        );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", rotate_my90dag, "QRotate_MY90", rotate_my90 );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", ROTATE_MY90dag, "QRotate_MY90", ROTATE_MY90 );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", rmy90dag,       "QRotate_MY90", rmy90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", RMY90dag,       "QRotate_MY90", RMY90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", Rmy90dag,       "QRotate_MY90", Rmy90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", my90dag,        "QRotate_MY90", my90        );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", MY90dag,        "QRotate_MY90", MY90        );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", QRotate_MY90(),
                                         "QRotate_Y90", QRotate_Y90() );  
}
