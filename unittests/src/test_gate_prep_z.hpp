/** @file test_gate_prep_z.hpp
 *
 *  @brief LibKet::gates::prep_z() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_prep_z)
{
  TEST_API_CONSISTENCY( prep_z    );
  TEST_API_CONSISTENCY( PREP_Z    );
  TEST_API_CONSISTENCY( QPrep_Z() );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QPrep_Z", prep_z    );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QPrep_Z", PREP_Z    );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QPrep_Z", QPrep_Z() );
}
