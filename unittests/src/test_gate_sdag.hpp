/** @file test_gate_sdag.hpp
 *
 *  @brief LibKet::gates::sdag() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_sdag)
{
  TEST_API_CONSISTENCY( sdag );
  TEST_API_CONSISTENCY( Sdag );

  TEST_API_CONSISTENCY( QSdag() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QSdag", sdag, "QS", s );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QSdag", Sdag, "QS", S );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QSdag", QSdag(), "QS", QS() );  
}
