/** @file test_gate_cnot.hpp
 *
 *  @brief LibKet::gates::cnot() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cnot)
{
  TEST_API_CONSISTENCY( cx   );
  TEST_API_CONSISTENCY( CX   );
  TEST_API_CONSISTENCY( cnot );
  TEST_API_CONSISTENCY( CNOT );
  TEST_API_CONSISTENCY( cxdag    );
  TEST_API_CONSISTENCY( CXdag    );
  TEST_API_CONSISTENCY( cnotdag  );
  TEST_API_CONSISTENCY( CNOTdag  );

  TEST_API_CONSISTENCY( QCX()    );
  TEST_API_CONSISTENCY( QCNOT()  );

  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", cx   );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", CX   );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", cnot );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", CNOT );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", cxdag   );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", CXdag   );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", cnotdag );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", CNOTdag );
  
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", QCX()   );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCNOT", QCNOT() );
}
