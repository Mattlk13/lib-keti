/** @file test_gate_rotate_y.hpp
 *
 *  @brief LibKet::gates::rotate_y() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_y)
{
  TEST_API_CONSISTENCY( rotate_y<>, QConst(3.141) );
  TEST_API_CONSISTENCY( ROTATE_Y<>, QConst(3.141) );
  TEST_API_CONSISTENCY( ry<>,       QConst(3.141) );
  TEST_API_CONSISTENCY( RY<>,       QConst(3.141) );
  TEST_API_CONSISTENCY( Ry<>,       QConst(3.141) );

  TEST_API_CONSISTENCY( QRotate_Y<QConst_t(3.141)>() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_y<>,    QConst(3.141),
                                        "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_y<>,    QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Y<>,    QConst(3.141),
                                        "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Y<>,    QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     ry<>,          QConst(3.141),
                                        "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    ry<>,          QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     RY<>,          QConst(3.141),
                                        "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    RY<>,          QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     Ry<>,          QConst(3.141),
                                        "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    Ry<>,          QConst(-3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_y<>,    QConst(3.141),
                                        "QRotate_Ydag<" QCONST_T "(3.141),QConst1_t(0)>",  rotate_ydag<>, QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Y<>,    QConst(3.141),
                                        "QRotate_Ydag<" QCONST_T "(3.141),QConst1_t(0)>",  ROTATE_Ydag<>, QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     ry<>,          QConst(3.141),
                                        "QRotate_Ydag<" QCONST_T "(3.141),QConst1_t(0)>",  rydag<>,       QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     RY<>,          QConst(3.141),
                                        "QRotate_Ydag<" QCONST_T "(3.141),QConst1_t(0)>",  RYdag<>,       QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     Ry<>,          QConst(3.141),
                                        "QRotate_Ydag<" QCONST_T "(3.141),QConst1_t(0)>",  Rydag<>,       QConst(3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_y<>,    QConst(-3.141),
                                        "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_y<>,    QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Y<>,    QConst(-3.141),
                                        "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Y<>,    QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    ry<>,          QConst(-3.141),
                                        "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     ry<>,          QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    RY<>,          QConst(-3.141),
                                        "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     RY<>,          QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    Ry<>,          QConst(-3.141),
                                        "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",     Ry<>,          QConst(3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_y<>,    QConst(-3.141),
                                        "QRotate_Ydag<" QCONST_T "(-3.141),QConst1_t(0)>", rotate_ydag<>, QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Y<>,    QConst(-3.141),
                                        "QRotate_Ydag<" QCONST_T "(-3.141),QConst1_t(0)>", ROTATE_Ydag<>, QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    ry<>,          QConst(-3.141),
                                        "QRotate_Ydag<" QCONST_T "(-3.141),QConst1_t(0)>", rydag<>,       QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    RY<>,          QConst(-3.141),
                                        "QRotate_Ydag<" QCONST_T "(-3.141),QConst1_t(0)>", RYdag<>,       QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>",    Ry<>,          QConst(-3.141),
                                        "QRotate_Ydag<" QCONST_T "(-3.141),QConst1_t(0)>", Rydag<>,       QConst(-3.141) );

  //TEST_UNARY_GATE( "QRotate_Y<" QCONST_T "(3.141),QConst1_t(0)>",  QRotate_Y<QConst_t(3.141)>() );
  //TEST_UNARY_GATE( "QRotate_Y<" QCONST_T "(-3.141),QConst1_t(0)>", QRotate_Y<QConst_t(-3.141)>() );
}
