/** @file test_gate_pauli_z.hpp
 *
 *  @brief LibKet::gates::pauli_z() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_pauli_z)
{
  TEST_API_CONSISTENCY( pauli_z );
  TEST_API_CONSISTENCY( PAULI_Z );
  TEST_API_CONSISTENCY( z       );
  TEST_API_CONSISTENCY( Z       );

  TEST_API_CONSISTENCY( pauli_zdag );
  TEST_API_CONSISTENCY( PAULI_Zdag );
  TEST_API_CONSISTENCY( zdag       );
  TEST_API_CONSISTENCY( Zdag       );

  TEST_API_CONSISTENCY( QPauli_Z() );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", pauli_z );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", PAULI_Z );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", z       );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", Z       );
  
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", pauli_zdag );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", PAULI_Zdag );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", zdag       );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", Zdag       );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Z", QPauli_Z() );
}
