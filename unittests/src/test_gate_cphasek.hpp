/** @file test_gate_cphasek.hpp
 *
 *  @brief LibKet::gates::cphasek() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cphasek)
{
  TEST_API_CONSISTENCY( cphasek<1>()  );
  TEST_API_CONSISTENCY( QCPhaseK<1>() );
  
  try {
    // cphasek()
    auto expr = cphasek<1>();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(all(),all())
    auto expr = cphasek<1>(all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = cphasek<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(range<0,3>(), range<4,7>())
    auto expr = cphasek<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(qureg<0,4>(), qureg<4,4>())
    auto expr = cphasek<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(qubit<1>(), qubit<2>())
    auto expr = cphasek<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>()
    auto expr = CPHASEK<1>();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(all(), all())
    auto expr = CPHASEK<1>(all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = CPHASEK<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(range<0,3>(), range<4,7>())
    auto expr = CPHASEK<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(qureg<0,4>(), qureg<4,4>())
    auto expr = CPHASEK<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(qubit<1>(), qubit<2>())
    auto expr = CPHASEK<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>()
    auto expr = crk<1>();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(all(), all())
    auto expr = crk<1>(all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = crk<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(range<0,3>(), range<4,7>())
    auto expr = crk<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(qureg<0,4>(), qureg<4,4>())
    auto expr = crk<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(qubit<1>(), qubit<2>())
    auto expr = crk<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>()
    auto expr = CRK<1>();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(all(), all())
    auto expr = CRK<1>(all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = CRK<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(range<0,3>(), range<4,7>())
    auto expr = CRK<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(qureg<0,4>(), qureg<4,4>())
    auto expr = CRK<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(qubit<1>(), qubit<2>())
    auto expr = CRK<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()
    auto expr = QCPhaseK<1>();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QCPhaseK<1,QConst1_t(0)>\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(all(), all())
    auto expr = QCPhaseK<1>()(all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = QCPhaseK<1>()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(range<0,3>(), range<4,7>())
    auto expr = QCPhaseK<1>()(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(qureg<0,4>(), qureg<4,4>())
    auto expr = QCPhaseK<1>()(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(qubit<1>(), qubit<2>())
    auto expr = QCPhaseK<1>()(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK<1,QConst1_t(0)>\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(cphasek<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(init())))
    auto expr = cphasek<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(init()));
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
