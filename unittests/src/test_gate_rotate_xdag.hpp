/** @file test_gate_rotate_xdag.hpp
 *
 *  @brief LibKet::gates::rotate_xdag() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_xdag)
{
  TEST_API_CONSISTENCY( rotate_xdag<>, QConst(3.141) );
  TEST_API_CONSISTENCY( ROTATE_Xdag<>, QConst(3.141) );
  TEST_API_CONSISTENCY( rxdag<>,       QConst(3.141) );
  TEST_API_CONSISTENCY( RXdag<>,       QConst(3.141) );
  TEST_API_CONSISTENCY( Rxdag<>,       QConst(3.141) );

  TEST_API_CONSISTENCY( QRotate_Xdag<QConst_t(3.141)>() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_xdag<>,    QConst(3.141),
                                        "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_xdag<>,    QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Xdag<>,    QConst(3.141),
                                        "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Xdag<>,    QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     rxdag<>,          QConst(3.141),
                                        "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    rxdag<>,          QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     RXdag<>,          QConst(3.141),
                                        "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    RXdag<>,          QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     Rxdag<>,          QConst(3.141),
                                        "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    Rxdag<>,          QConst(-3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_xdag<>,    QConst(3.141),
                                        "QRotate_X<" QCONST_T "(3.141),QConst1_t(0)>",        rotate_x<>,       QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Xdag<>,    QConst(3.141),
                                        "QRotate_X<" QCONST_T "(3.141),QConst1_t(0)>",        ROTATE_X<>,       QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     rxdag<>,          QConst(3.141),
                                        "QRotate_X<" QCONST_T "(3.141),QConst1_t(0)>",        rx<>,             QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     RXdag<>,          QConst(3.141),
                                        "QRotate_X<" QCONST_T "(3.141),QConst1_t(0)>",        RX<>,             QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     Rxdag<>,          QConst(3.141),
                                        "QRotate_X<" QCONST_T "(3.141),QConst1_t(0)>",        Rx<>,             QConst(3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_xdag<>,    QConst(-3.141),
                                        "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     rotate_xdag<>,    QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Xdag<>,    QConst(-3.141),
                                        "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     ROTATE_Xdag<>,    QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    rxdag<>,          QConst(-3.141),
                                        "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     rxdag<>,          QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    RXdag<>,          QConst(-3.141),
                                        "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     RXdag<>,          QConst(3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    Rxdag<>,          QConst(-3.141),
                                        "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",     Rxdag<>,          QConst(3.141) );

  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    rotate_xdag<>,    QConst(-3.141),
                                        "QRotate_X<" QCONST_T "(-3.141),QConst1_t(0)>",       rotate_x<>,       QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    ROTATE_Xdag<>,    QConst(-3.141),
                                        "QRotate_X<" QCONST_T "(-3.141),QConst1_t(0)>",       ROTATE_X<>,       QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    rxdag<>,          QConst(-3.141),
                                        "QRotate_X<" QCONST_T "(-3.141),QConst1_t(0)>",       rx<>,             QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    RXdag<>,          QConst(-3.141),
                                        "QRotate_X<" QCONST_T "(-3.141),QConst1_t(0)>",       RX<>,             QConst(-3.141) );
  TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>",    Rxdag<>,          QConst(-3.141),
                                        "QRotate_X<" QCONST_T "(-3.141),QConst1_t(0)>",       Rx<>,             QConst(-3.141) );

  //TEST_UNARY_GATE( "QRotate_Xdag<" QCONST_T "(3.141),QConst1_t(0)>",  QRotate_Xdag<QConst_t(3.141)>() );
  //TEST_UNARY_GATE( "QRotate_Xdag<" QCONST_T "(-3.141),QConst1_t(0)>", QRotate_Xdag<QConst_t(-3.141)>() );
}
