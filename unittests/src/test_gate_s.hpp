/** @file test_gate_s.hpp
 *
 *  @brief LibKet::gates::s() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_s)
{
  TEST_API_CONSISTENCY( s );
  TEST_API_CONSISTENCY( S );

  TEST_API_CONSISTENCY( QS() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QS", s, "QSdag", sdag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QS", S, "QSdag", Sdag );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QS", QS(), "QSdag", QSdag() );
}
