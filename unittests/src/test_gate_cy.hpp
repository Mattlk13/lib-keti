/** @file test_gate_cy.hpp
 *
 *  @brief LibKet::gates::cy() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cy)
{
  TEST_API_CONSISTENCY( cy );
  TEST_API_CONSISTENCY( CY );  
  TEST_API_CONSISTENCY( cydag );
  TEST_API_CONSISTENCY( CYdag );
  
  TEST_API_CONSISTENCY( QCY() );

  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCY", cy );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCY", CY );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCY", cydag );
  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCY", CYdag );

  TEST_BINARY_GATE_OPTIMIZE_IDENTITY( "QCY", QCY() );
}
