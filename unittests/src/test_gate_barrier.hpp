/** @file test_gate_barrier.hpp
 *
 *  @brief LibKet::gates::barrier() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_barrier)
{
  TEST_API_CONSISTENCY( barrier  );
  TEST_API_CONSISTENCY( BARRIER  );

  TEST_API_CONSISTENCY( QBarrier() );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QBarrier", barrier );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QBarrier", BARRIER );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QBarrier", QBarrier() );
}
