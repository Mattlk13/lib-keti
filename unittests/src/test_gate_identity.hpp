/** @file test_gate_identity.hpp
 *
 *  @brief LibKet::gates::identity() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_identity)
{
  TEST_API_CONSISTENCY( identity );
  TEST_API_CONSISTENCY( IDENTITY );
  TEST_API_CONSISTENCY( i        );
  TEST_API_CONSISTENCY( id       );
  TEST_API_CONSISTENCY( I        );
  TEST_API_CONSISTENCY( ID       );

  TEST_API_CONSISTENCY( identitydag );
  TEST_API_CONSISTENCY( IDENTITYdag );
  TEST_API_CONSISTENCY( idag        );
  TEST_API_CONSISTENCY( iddag       );
  TEST_API_CONSISTENCY( Idag        );
  TEST_API_CONSISTENCY( IDdag       );

  TEST_API_CONSISTENCY( QIdentity() );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", identity );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", IDENTITY );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", i        );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", id       );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", I        );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", ID       );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", identitydag );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", IDENTITYdag );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", idag        );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", iddag       );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", Idag        );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", IDdag       );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QIdentity", QIdentity() );
}
